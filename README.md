# Smart Fish Monitoring System backend

![High level diagram](documentation/high-level-diagram.png "High Level Diagram")

## Important links

- [Heroku dashboard](https://dashboard.heroku.com/apps/smart-fish-monitoring-system)
- [Api Contract](backend-app/src/main/resources/openapi.yml)

## Development
 
Setup infrastructures

```
$ docker-compose up -d
$ PG_DATABASE_HOST=localhost:5432 \ 
    PG_DATABASE_NAME=db_smart_fist_monitoring_system \
    PG_DATABASE_USERNAME=postgres_user \
    PG_DATABASE_PASSWORD=postgres_password \
    ./gradlew flywayMigrate
```

### How to verify code quality (test and static analyzer)

```console!
$ ./gradlew clean test pmdTest pmdMain
```

### How to build

```console
$ ./gradlew clean build
```
