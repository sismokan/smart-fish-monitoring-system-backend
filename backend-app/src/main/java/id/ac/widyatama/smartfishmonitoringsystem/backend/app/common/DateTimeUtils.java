package id.ac.widyatama.smartfishmonitoringsystem.backend.app.common;

import id.ac.widyatama.smartfishmonitoringsystem.backend.app.Constant;
import org.apache.commons.lang3.StringUtils;

import java.time.Instant;
import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.Optional;

public class DateTimeUtils {

    public static ZoneOffset getJakartaZone() {
        return ZoneId.of(Constant.DateTime.ZONE_ASIA_JAKARTA).getRules().getOffset(Instant.now());
    }

    public static OffsetDateTime getNow() {
        return Instant.now().atOffset(getJakartaZone());
    }

    public static OffsetDateTime parse(String dateTime) {
        return OffsetDateTime.parse(dateTime, DateTimeFormatter.ISO_OFFSET_DATE_TIME);
    }

    public static OffsetDateTime from(long epochInSecond) {
        return OffsetDateTime.ofInstant(Instant.ofEpochSecond(epochInSecond), DateTimeUtils.getJakartaZone());
    }

    public static long parseEpochOrReplace(String timeStr, OffsetDateTime replacementDateTime) {
        return Optional.ofNullable(timeStr)
                .filter(StringUtils::isNotBlank)
                .map(Long::parseLong)
                .orElse(replacementDateTime.toEpochSecond());
    }
}
