package id.ac.widyatama.smartfishmonitoringsystem.backend.app.domain.dto;

import id.ac.widyatama.smartfishmonitoringsystem.backend.api_response.APIResponse;
import id.ac.widyatama.smartfishmonitoringsystem.backend.error.Error;
import lombok.Builder;
import lombok.Value;

import java.time.OffsetDateTime;
import java.util.Objects;

@Value
@Builder(setterPrefix = "set")
public class TimeSelector implements Validable<TimeSelector> {

    OffsetDateTime startTime;
    OffsetDateTime endTime;

    public APIResponse<TimeSelector> validate() {
        return APIResponse.createResponse(() -> startTime)
                .validate(Objects::nonNull, Error.Code.FIELD_CANNOT_BE_BLANK, "time_selector.start_time")
                .map(data -> endTime)
                .validate(Objects::nonNull, Error.Code.FIELD_CANNOT_BE_BLANK, "time_selector.end_time")
                .validate(data -> data.isAfter(startTime), Error.Code.MALFORMED_DATA, "end_time", "end_time should be before than start_time")
                .map(data -> this);
    }
}
