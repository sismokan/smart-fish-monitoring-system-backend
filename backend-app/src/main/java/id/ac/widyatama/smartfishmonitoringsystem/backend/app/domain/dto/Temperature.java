package id.ac.widyatama.smartfishmonitoringsystem.backend.app.domain.dto;

import id.ac.widyatama.smartfishmonitoringsystem.backend.api_response.APIResponse;
import id.ac.widyatama.smartfishmonitoringsystem.backend.error.Error;
import lombok.Builder;
import lombok.Value;
import org.apache.commons.lang3.StringUtils;

import java.time.OffsetDateTime;
import java.util.Objects;

@Value
@Builder(setterPrefix = "set")
public class Temperature implements Validable<Temperature> {

    Double value;
    String measurement;
    OffsetDateTime takenAt;

    public APIResponse<Temperature> validate() {
        return APIResponse.createResponse(() -> value)
                .validate(Objects::nonNull, Error.Code.FIELD_CANNOT_BE_BLANK, "temperature.value")
                .map(data -> measurement)
                .validate(StringUtils::isNotBlank, Error.Code.FIELD_CANNOT_BE_BLANK, "temperature.measurement")
                .map(data -> this);
    }
}
