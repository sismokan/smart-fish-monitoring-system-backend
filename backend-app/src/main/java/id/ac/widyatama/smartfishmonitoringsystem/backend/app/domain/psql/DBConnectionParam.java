package id.ac.widyatama.smartfishmonitoringsystem.backend.app.domain.psql;

import com.gojek.ApplicationConfiguration;
import com.zaxxer.hikari.HikariConfig;
import id.ac.widyatama.smartfishmonitoringsystem.backend.app.Constant;
import lombok.Value;

@Value
public class DBConnectionParam {

    private static final int DEFAULT_POOL_SIZE = 1;
    private static final int DEFAULT_TIMEOUT_IN_MS = 250;
    String host;
    String databaseName;
    String username;
    String password;
    String poolName;
    int poolSize;
    int timeoutInMs;

    public static DBConnectionParam createFromConfig(ApplicationConfiguration configuration) {
        return new DBConnectionParam(
                configuration.getValueAsString(Constant.Config.PG_DATABASE_HOST),
                configuration.getValueAsString(Constant.Config.PG_DATABASE_NAME),
                configuration.getValueAsString(Constant.Config.PG_DATABASE_USERNAME),
                configuration.getValueAsString(Constant.Config.PG_DATABASE_PASSWORD),
                configuration.getValueAsString(Constant.Config.PG_DATABASE_POOL_NAME),
                configuration.getValueAsInt(Constant.Config.PG_DATABASE_POOL_SIZE, DEFAULT_POOL_SIZE),
                configuration.getValueAsInt(Constant.Config.PG_DATABASE_TIMEOUT_IN_MS, DEFAULT_TIMEOUT_IN_MS)
        );
    }

    public String getJdbcURL() {
        return String.format("jdbc:postgresql://%s/%s", host, databaseName);
    }

    public HikariConfig toHikariConfig() {
        HikariConfig config = new HikariConfig();
        config.setJdbcUrl(getJdbcURL());
        config.setUsername(username);
        config.setPassword(password);
        config.setPoolName(poolName);
        config.setMaximumPoolSize(poolSize);
        config.setConnectionTimeout(timeoutInMs);
        return config;
    }
}
