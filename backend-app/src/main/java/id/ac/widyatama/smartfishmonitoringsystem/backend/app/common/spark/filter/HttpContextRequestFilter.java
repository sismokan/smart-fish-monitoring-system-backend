package id.ac.widyatama.smartfishmonitoringsystem.backend.app.common.spark.filter;

import id.ac.widyatama.smartfishmonitoringsystem.backend.app.Constant;
import id.ac.widyatama.smartfishmonitoringsystem.backend.app.common.context.RequestContext;
import id.ac.widyatama.smartfishmonitoringsystem.backend.app.common.spark.Request;
import id.ac.widyatama.smartfishmonitoringsystem.backend.app.common.spark.SparkRequest;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import spark.Filter;
import spark.Response;

import java.util.UUID;

import static net.logstash.logback.marker.Markers.append;

@Slf4j
public class HttpContextRequestFilter implements Filter {

    private static final String DEBUG_ID = "debug-id";

    @Override
    public void handle(spark.Request request, Response response) {
        Request requestWrapper = SparkRequest.wrap(request);

        RequestContext.putCorrelationId(getCorrelationId(requestWrapper));
        RequestContext.putRequestUri(requestWrapper.getUri());
        RequestContext.putRequestMethod(requestWrapper.getMethod());

        log.info(append("request_body", requestWrapper.getBody())
                        .and(append("request_headers", requestWrapper.getRequestHeaders().toString()))
                        .and(append("request_query_params", requestWrapper.getRequestQueryParams().toString())),
                "Received request");

        request.attribute(Constant.Attribute.WRAPPED_REQUEST, requestWrapper);
    }

    private String getCorrelationId(Request requestWrapper) {
        String debugId = requestWrapper.getHeaderValue(DEBUG_ID);
        return StringUtils.isBlank(debugId)
                ? UUID.randomUUID().toString()
                : debugId;
    }
}
