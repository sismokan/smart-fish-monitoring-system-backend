package id.ac.widyatama.smartfishmonitoringsystem.backend.app.common.spark;

import com.google.gson.Gson;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import spark.ResponseTransformer;

@AllArgsConstructor
@FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
public class JsonResponseTransformer implements ResponseTransformer {

    Gson gson;

    @Override
    public String render(Object model) {
        return gson.toJson(model);
    }
}
