package id.ac.widyatama.smartfishmonitoringsystem.backend.app.domain.influxdb;

import com.influxdb.query.FluxRecord;
import com.influxdb.query.FluxTable;
import id.ac.widyatama.smartfishmonitoringsystem.backend.app.common.LambdaUtils;
import id.ac.widyatama.smartfishmonitoringsystem.backend.app.common.LogUtils;
import id.ac.widyatama.smartfishmonitoringsystem.backend.api_response.APIResponse;
import id.ac.widyatama.smartfishmonitoringsystem.backend.error.Error;
import id.ac.widyatama.smartfishmonitoringsystem.backend.app.domain.influxdb.query.Query;
import org.slf4j.Logger;

import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;

public interface CommonRepository<M> {

    InfluxDBWrapper getInfluxDBWrapper();

    Logger getLogger();

    default APIResponse<Void> saveRecord(M metric) {
        Logger logger = getLogger();
        return APIResponse.createResponse(() -> metric)
                .validate(Objects::nonNull, Error.Code.GENERIC_ERROR, "", "Object cannot be null")
                .onSuccess(data -> logger.debug(String.format("saveRecord: %s", data.toString())))
                .flatMap(getInfluxDBWrapper()::write)
                .onError(errors -> logger.error(LogUtils.getErrorLogMarkers(errors), "Got failure when writing record"))
                .map(LambdaUtils::toVoid);
    }

    default <Q extends Query> APIResponse<List<M>> readRecords(Q query, Function<FluxRecord, M> mapperFn) {
        Logger logger = getLogger();
        return APIResponse.createResponse(query::toQuery)
                .onSuccess(data -> logger.debug(String.format("readRecords: %s", data)))
                .flatMap(data -> readRecords(data, mapperFn))
                .onError(errors -> logger.error(LogUtils.getErrorLogMarkers(errors), "Got failure when reading records"));
    }

    private APIResponse<List<M>> readRecords(String query, Function<FluxRecord, M> mapperFn) {
        return getInfluxDBWrapper().read(query)
                .map(Collection::stream)
                .map(stream -> stream.map(FluxTable::getRecords))
                .map(stream -> stream.flatMap(Collection::stream))
                .map(stream -> stream.map(mapperFn))
                .map(stream -> stream.collect(Collectors.toList()));
    }
}
