package id.ac.widyatama.smartfishmonitoringsystem.backend.app.domain.influxdb.pond_sensor;

import id.ac.widyatama.smartfishmonitoringsystem.backend.app.domain.influxdb.PondRepository;
import id.ac.widyatama.smartfishmonitoringsystem.backend.app.domain.influxdb.InfluxDBWrapper;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class PondSensorRepository extends PondRepository<PondSensorMetric> {

    public PondSensorRepository(InfluxDBWrapper influxDBWrapper) {
        super(influxDBWrapper);
    }
}
