package id.ac.widyatama.smartfishmonitoringsystem.backend.app.controller.app.v1.ponds;

import id.ac.widyatama.smartfishmonitoringsystem.backend.app.Constant;
import id.ac.widyatama.smartfishmonitoringsystem.backend.app.common.DateTimeUtils;
import id.ac.widyatama.smartfishmonitoringsystem.backend.app.common.LogUtils;
import id.ac.widyatama.smartfishmonitoringsystem.backend.app.common.context.FlowContext;
import id.ac.widyatama.smartfishmonitoringsystem.backend.app.common.context.RequestContext;
import id.ac.widyatama.smartfishmonitoringsystem.backend.app.common.spark.status_code.HttpErrorTranslator;
import id.ac.widyatama.smartfishmonitoringsystem.backend.app.controller.AbstractController;
import id.ac.widyatama.smartfishmonitoringsystem.backend.api_response.APIResponse;
import id.ac.widyatama.smartfishmonitoringsystem.backend.app.domain.contract.GetPondTemperatureMetricsRequest;
import id.ac.widyatama.smartfishmonitoringsystem.backend.app.domain.contract.GetPondTemperatureMetricsResponse;
import id.ac.widyatama.smartfishmonitoringsystem.backend.app.domain.dto.Temperature;
import id.ac.widyatama.smartfishmonitoringsystem.backend.app.domain.dto.TimeSelector;
import id.ac.widyatama.smartfishmonitoringsystem.backend.error.Error;
import id.ac.widyatama.smartfishmonitoringsystem.backend.app.domain.metric.GetMetricsContext;
import id.ac.widyatama.smartfishmonitoringsystem.backend.app.domain.metric.GetPondMetricsService;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import spark.Request;
import spark.Response;
import spark.Route;

import java.net.HttpURLConnection;
import java.time.OffsetDateTime;
import java.util.List;

import static net.logstash.logback.marker.Markers.append;

@Slf4j
@Getter
@AllArgsConstructor
public class GetPondTemperatureMetricsController extends AbstractController implements Route {

    private GetPondMetricsService getPondMetricsService;
    private HttpErrorTranslator httpErrorTranslator;

    @Override
    public APIResponse<GetPondTemperatureMetricsResponse> handle(Request request, Response response) {
        FlowContext context = GetMetricsContext.createForTemperature();
        RequestContext.putContext(context);

        log.info("Getting data from request");
        APIResponse<GetPondTemperatureMetricsRequest> mappedRequest = decodeRequest(request);
        if (!mappedRequest.isSuccess()) {
            return handleErrorResponse(mappedRequest, response);
        }

        log.info("Fetching data from database");
        APIResponse<List<Temperature>> result = getPondMetricsService.getTemperatureMetrics(mappedRequest.getData());
        if (!result.isSuccess()) {
            return handleErrorResponse(result, response);
        }

        return handleSuccessResponse(result, response);
    }

    private APIResponse<GetPondTemperatureMetricsResponse> handleSuccessResponse(APIResponse<List<Temperature>> result, Response response) {
        response.status(HttpURLConnection.HTTP_OK);
        log.info("Get metrics succeeded");
        return result.map(data -> GetPondTemperatureMetricsResponse.builder().setTemperatureMetrics(data).build());
    }

    private APIResponse<GetPondTemperatureMetricsRequest> decodeRequest(Request request) {
        return APIResponse.createResponse(() -> constructGetMetricRequest(request))
                .changeErrors(Error.Code.MALFORMED_DATA, "request")
                .flatMap(GetPondTemperatureMetricsRequest::validate)
                .peek(data -> log.info(append("pond_id", data.getPondId()), String.format("Successfully decoded request: %s", data)),
                        errors -> log.error(LogUtils.getErrorLogMarkers(errors), "Failed to decode request"));
    }

    private GetPondTemperatureMetricsRequest constructGetMetricRequest(Request request) {
        return GetPondTemperatureMetricsRequest.builder()
                .setPondId(request.params("pond_id"))
                .setSelectedTime(buildSelectedTime(request))
                .build();
    }

    private TimeSelector buildSelectedTime(Request request) {
        OffsetDateTime now = DateTimeUtils.getNow();
        long startTimeInEpochSecond = DateTimeUtils.parseEpochOrReplace(request.queryParams("start_time"), now.minusHours(Constant.DateTime.DEFAULT_MINUS_HOUR));
        long endTimeInEpochSecond = DateTimeUtils.parseEpochOrReplace(request.queryParams("end_time"), now);

        return TimeSelector.builder()
                .setStartTime(DateTimeUtils.from(startTimeInEpochSecond))
                .setEndTime(DateTimeUtils.from(endTimeInEpochSecond))
                .build();
    }
}
