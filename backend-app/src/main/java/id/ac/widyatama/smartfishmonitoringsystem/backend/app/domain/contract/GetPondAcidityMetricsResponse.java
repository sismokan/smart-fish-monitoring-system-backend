package id.ac.widyatama.smartfishmonitoringsystem.backend.app.domain.contract;

import id.ac.widyatama.smartfishmonitoringsystem.backend.app.domain.dto.Acidity;
import lombok.Builder;
import lombok.Value;

import java.util.List;

@Value
@Builder(setterPrefix = "set")
public class GetPondAcidityMetricsResponse {

    List<Acidity> acidityMetrics;
}
