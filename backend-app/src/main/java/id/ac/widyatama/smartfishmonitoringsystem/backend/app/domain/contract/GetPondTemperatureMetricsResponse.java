package id.ac.widyatama.smartfishmonitoringsystem.backend.app.domain.contract;

import id.ac.widyatama.smartfishmonitoringsystem.backend.app.domain.dto.Temperature;
import lombok.Builder;
import lombok.Value;

import java.util.List;

@Value
@Builder(setterPrefix = "set")
public class GetPondTemperatureMetricsResponse {

    List<Temperature> temperatureMetrics;
}
