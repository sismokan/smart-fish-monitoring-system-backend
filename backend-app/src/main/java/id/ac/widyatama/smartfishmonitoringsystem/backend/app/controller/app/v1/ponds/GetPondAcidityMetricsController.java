package id.ac.widyatama.smartfishmonitoringsystem.backend.app.controller.app.v1.ponds;

import id.ac.widyatama.smartfishmonitoringsystem.backend.app.Constant;
import id.ac.widyatama.smartfishmonitoringsystem.backend.app.common.DateTimeUtils;
import id.ac.widyatama.smartfishmonitoringsystem.backend.app.common.LogUtils;
import id.ac.widyatama.smartfishmonitoringsystem.backend.app.common.context.FlowContext;
import id.ac.widyatama.smartfishmonitoringsystem.backend.app.common.context.RequestContext;
import id.ac.widyatama.smartfishmonitoringsystem.backend.app.common.spark.status_code.HttpErrorTranslator;
import id.ac.widyatama.smartfishmonitoringsystem.backend.app.controller.AbstractController;
import id.ac.widyatama.smartfishmonitoringsystem.backend.api_response.APIResponse;
import id.ac.widyatama.smartfishmonitoringsystem.backend.app.domain.contract.GetPondAcidityMetricsRequest;
import id.ac.widyatama.smartfishmonitoringsystem.backend.app.domain.contract.GetPondAcidityMetricsResponse;
import id.ac.widyatama.smartfishmonitoringsystem.backend.app.domain.dto.Acidity;
import id.ac.widyatama.smartfishmonitoringsystem.backend.app.domain.dto.TimeSelector;
import id.ac.widyatama.smartfishmonitoringsystem.backend.error.Error;
import id.ac.widyatama.smartfishmonitoringsystem.backend.app.domain.metric.GetMetricsContext;
import id.ac.widyatama.smartfishmonitoringsystem.backend.app.domain.metric.GetPondMetricsService;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import spark.Request;
import spark.Response;
import spark.Route;

import java.net.HttpURLConnection;
import java.time.OffsetDateTime;
import java.util.List;

import static net.logstash.logback.marker.Markers.append;

@Slf4j
@Getter
@AllArgsConstructor
public class GetPondAcidityMetricsController extends AbstractController implements Route {

    private GetPondMetricsService getPondMetricsService;
    private HttpErrorTranslator httpErrorTranslator;

    @Override
    public APIResponse<GetPondAcidityMetricsResponse> handle(Request request, Response response) {
        FlowContext context = GetMetricsContext.createForAcidity();
        RequestContext.putContext(context);

        log.info("Getting data from request");
        APIResponse<GetPondAcidityMetricsRequest> mappedRequest = decodeRequest(request);
        if (!mappedRequest.isSuccess()) {
            return handleErrorResponse(mappedRequest, response);
        }

        log.info("Fetching data from database");
        APIResponse<List<Acidity>> result = getPondMetricsService.getAcidityMetrics(mappedRequest.getData());
        if (!result.isSuccess()) {
            return handleErrorResponse(result, response);
        }

        return handleSuccessResponse(result, response);
    }

    private APIResponse<GetPondAcidityMetricsResponse> handleSuccessResponse(APIResponse<List<Acidity>> result, Response response) {
        response.status(HttpURLConnection.HTTP_OK);
        log.info("Get metrics succeeded");
        return result.map(data -> GetPondAcidityMetricsResponse.builder().setAcidityMetrics(data).build());
    }

    private APIResponse<GetPondAcidityMetricsRequest> decodeRequest(Request request) {
        return APIResponse.createResponse(() -> constructGetMetricRequest(request))
                .changeErrors(Error.Code.MALFORMED_DATA, "request")
                .flatMap(GetPondAcidityMetricsRequest::validate)
                .peek(data -> log.info(append("pond_id", data.getPondId()), String.format("Successfully decoded request: %s", data)),
                        errors -> log.error(LogUtils.getErrorLogMarkers(errors), "Failed to decode request"));
    }

    private GetPondAcidityMetricsRequest constructGetMetricRequest(Request request) {
        return GetPondAcidityMetricsRequest.builder()
                .setPondId(request.params("pond_id"))
                .setSelectedTime(buildSelectedTime(request))
                .build();
    }

    private TimeSelector buildSelectedTime(Request request) {
        OffsetDateTime now = DateTimeUtils.getNow();
        long startTimeInEpochSecond = DateTimeUtils.parseEpochOrReplace(request.queryParams("start_time"), now.minusHours(Constant.DateTime.DEFAULT_MINUS_HOUR));
        long endTimeInEpochSecond = DateTimeUtils.parseEpochOrReplace(request.queryParams("end_time"), now);

        return TimeSelector.builder()
                .setStartTime(DateTimeUtils.from(startTimeInEpochSecond))
                .setEndTime(DateTimeUtils.from(endTimeInEpochSecond))
                .build();
    }
}
