package id.ac.widyatama.smartfishmonitoringsystem.backend.app.domain.influxdb.acidity;

import com.influxdb.query.FluxRecord;
import id.ac.widyatama.smartfishmonitoringsystem.backend.app.common.DateTimeUtils;
import id.ac.widyatama.smartfishmonitoringsystem.backend.app.domain.dto.Acidity;
import lombok.Value;

import java.time.Instant;

@Value
public class AcidityMetric {

    private static final String PH = "PH";
    String measurement;
    Double value;
    Instant time;

    public static AcidityMetric createFromFluxRecord(FluxRecord fluxRecord) {
        return new AcidityMetric(
                PH,
                (Double) fluxRecord.getValue(),
                fluxRecord.getTime()
        );
    }

    public Acidity toDTOTemperature() {
        return Acidity.builder()
                .setTakenAt(time.atOffset(DateTimeUtils.getJakartaZone()))
                .setMeasurement(measurement)
                .setValue(value)
                .build();
    }
}

