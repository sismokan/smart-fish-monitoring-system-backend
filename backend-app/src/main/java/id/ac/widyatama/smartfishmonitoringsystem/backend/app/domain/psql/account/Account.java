package id.ac.widyatama.smartfishmonitoringsystem.backend.app.domain.psql.account;

import lombok.Builder;
import lombok.Value;

@Value
@Builder(setterPrefix = "set")
public class Account {

    String guid;
    String username;
    String password;
}
