package id.ac.widyatama.smartfishmonitoringsystem.backend.app.domain.psql.pond;

import lombok.Builder;
import lombok.Value;

@Value
@Builder(setterPrefix = "set")
public class Pond {

    String guid;
    String name;
    String address;
    String userGuid;
}
