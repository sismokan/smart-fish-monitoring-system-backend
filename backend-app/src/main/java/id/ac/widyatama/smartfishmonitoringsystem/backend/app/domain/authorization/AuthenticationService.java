package id.ac.widyatama.smartfishmonitoringsystem.backend.app.domain.authorization;

import id.ac.widyatama.smartfishmonitoringsystem.backend.app.common.LogUtils;
import id.ac.widyatama.smartfishmonitoringsystem.backend.api_response.APIResponse;
import id.ac.widyatama.smartfishmonitoringsystem.backend.app.domain.decoder.DecodeBasicAuthResponse;
import id.ac.widyatama.smartfishmonitoringsystem.backend.app.domain.decoder.DecoderService;
import id.ac.widyatama.smartfishmonitoringsystem.backend.error.Error;
import id.ac.widyatama.smartfishmonitoringsystem.backend.app.domain.psql.account.AccountRepository;
import id.ac.widyatama.smartfishmonitoringsystem.backend.app.domain.psql.dto.GetAccountByUserCredential;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

import static net.logstash.logback.marker.Markers.append;

@Slf4j
public class AuthenticationService {

    private final DecoderService decoderService;
    private final AccountRepository accountRepository;

    public AuthenticationService(DecoderService decoderService,
                                 AccountRepository accountRepository) {
        this.decoderService = decoderService;
        this.accountRepository = accountRepository;
    }

    public APIResponse<AuthenticatedAccount> authorizeUsingBasic(String token) {
        return APIResponse.createResponse(() -> token)
                .validate(StringUtils::isNoneBlank, Error.Code.MALFORMED_DATA, "token")
                .flatMap(data -> decoderService.decodeBasicAuth(token))
                .map(this::buildCredentialRequest)
                .flatMap(accountRepository::getAccountBy)
                .map(this::mapAccountData)
                .peek(data -> log.info(append("account_guid", data.getGuid()), "Authentication succeed"),
                        errors -> log.error(LogUtils.getErrorLogMarkers(errors), "Authentication failed"));
    }

    private AuthenticatedAccount mapAccountData(id.ac.widyatama.smartfishmonitoringsystem.backend.app.domain.psql.account.Account data) {
        return AuthenticatedAccount.builder()
                .setGuid(data.getGuid())
                .setUsername(data.getUsername())
                .build();
    }

    private GetAccountByUserCredential buildCredentialRequest(DecodeBasicAuthResponse data) {
        return GetAccountByUserCredential.builder()
                .setUsername(data.getUsername())
                .setPassword(data.getPassword())
                .build();
    }
}
