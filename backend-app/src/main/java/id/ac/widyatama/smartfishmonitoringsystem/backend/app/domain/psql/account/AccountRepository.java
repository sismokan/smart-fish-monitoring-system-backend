package id.ac.widyatama.smartfishmonitoringsystem.backend.app.domain.psql.account;

import com.gojek.ApplicationConfiguration;
import id.ac.widyatama.smartfishmonitoringsystem.backend.api_response.APIResponse;
import id.ac.widyatama.smartfishmonitoringsystem.backend.error.Error;
import id.ac.widyatama.smartfishmonitoringsystem.backend.app.domain.psql.BaseRepository;
import id.ac.widyatama.smartfishmonitoringsystem.backend.app.domain.psql.dto.GetAccountByUserCredential;

import java.util.Objects;

public class AccountRepository extends BaseRepository<AccountDAO> {

    public AccountRepository(ApplicationConfiguration configuration) {
        super(configuration);
    }

    public APIResponse<Account> getAccountBy(GetAccountByUserCredential getAccountByUserCredential) {
        return execute(AccountDAO.class, dao -> dao.getAccountBy(getAccountByUserCredential))
                .validate(Objects::nonNull, Error.Code.RESOURCE_CANNOT_BE_FOUND, "account");
    }
}
