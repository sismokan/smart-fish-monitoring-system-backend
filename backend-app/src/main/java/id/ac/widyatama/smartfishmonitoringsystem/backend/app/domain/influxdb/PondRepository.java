package id.ac.widyatama.smartfishmonitoringsystem.backend.app.domain.influxdb;

import com.influxdb.query.FluxRecord;
import id.ac.widyatama.smartfishmonitoringsystem.backend.api_response.APIResponse;
import id.ac.widyatama.smartfishmonitoringsystem.backend.error.Error;
import id.ac.widyatama.smartfishmonitoringsystem.backend.app.domain.influxdb.query.GetRecordsByPondId;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;

import java.util.List;
import java.util.Objects;
import java.util.function.Function;

@Slf4j
public class PondRepository<M> implements CommonRepository<M> {

    protected final InfluxDBWrapper influxDBWrapper;

    protected PondRepository(InfluxDBWrapper influxDBWrapper) {
        this.influxDBWrapper = influxDBWrapper;
    }

    @Override
    public InfluxDBWrapper getInfluxDBWrapper() {
        return influxDBWrapper;
    }

    @Override
    public Logger getLogger() {
        return log;
    }

    public APIResponse<List<M>> getRecordsByPondId(GetRecordsByPondId request, Function<FluxRecord, M> mapperFn) {
        return APIResponse.createResponse(() -> request)
                .validate(Objects::nonNull, Error.Code.GENERIC_ERROR, "", "Object cannot be null")
                .flatMap(data -> readRecords(data, mapperFn));
    }
}
