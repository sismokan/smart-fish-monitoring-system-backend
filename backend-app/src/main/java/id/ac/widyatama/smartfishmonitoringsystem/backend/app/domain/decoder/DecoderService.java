package id.ac.widyatama.smartfishmonitoringsystem.backend.app.domain.decoder;

import id.ac.widyatama.smartfishmonitoringsystem.backend.app.common.LogUtils;
import id.ac.widyatama.smartfishmonitoringsystem.backend.api_response.APIResponse;
import id.ac.widyatama.smartfishmonitoringsystem.backend.error.Error;
import lombok.extern.slf4j.Slf4j;

import java.nio.charset.StandardCharsets;
import java.util.Base64;

@Slf4j
public class DecoderService {

    private static final String METHOD_BASIC = "Basic";

    public APIResponse<DecodeBasicAuthResponse> decodeBasicAuth(String token) {
        log.info("Decoding the token using basic method");
        return APIResponse.createResponse(() -> token.substring(METHOD_BASIC.length()).trim())
                .mapTry(this::decodeToken)
                .onError(errors -> log.error(LogUtils.getErrorLogMarkers(errors), "Failed to decode token"))
                .changeErrors(Error.Code.MALFORMED_DATA, "authorization.basic");
    }

    private DecodeBasicAuthResponse decodeToken(String base64Credentials) {
        byte[] credDecoded = Base64.getDecoder().decode(base64Credentials);
        String credentials = new String(credDecoded, StandardCharsets.UTF_8);
        final String[] values = credentials.split(":", 2);
        return DecodeBasicAuthResponse.builder()
                .setUsername(values[0])
                .setPassword(values[1])
                .build();
    }
}
