package id.ac.widyatama.smartfishmonitoringsystem.backend.app;

import lombok.experimental.UtilityClass;

@UtilityClass
public class Constant {

    @UtilityClass
    public static class Server {
        public static final int DEFAULT_PORT = 4000;
        public static final int DEFAULT_SHUTDOWN_TIMEOUT_MS = 20000;
    }

    @UtilityClass
    public static class Config {
        public static final String PORT = "PORT";
        public static final String SHUTDOWN_TIMEOUT_MS = "SHUTDOWN_TIMEOUT_MS";

        public static final String INFLUXDB_BUCKET = "STACKHERO_INFLUXDB_ADMIN_BUCKET";
        public static final String INFLUXDB_ORGANIZATION = "STACKHERO_INFLUXDB_ADMIN_ORGANIZATION";
        public static final String INFLUXDB_API_TOKEN = "STACKHERO_INFLUXDB_API_TOKEN";
        public static final String INFLUXDB_HOST = "STACKHERO_INFLUXDB_HOST";

        public static final String PG_DATABASE_HOST = "PG_DATABASE_HOST";
        public static final String PG_DATABASE_NAME = "PG_DATABASE_NAME";
        public static final String PG_DATABASE_USERNAME = "PG_DATABASE_USERNAME";
        public static final String PG_DATABASE_PASSWORD = "PG_DATABASE_PASSWORD";
        public static final String PG_DATABASE_POOL_NAME = "PG_DATABASE_POOL_NAME";
        public static final String PG_DATABASE_POOL_SIZE = "PG_DATABASE_POOL_SIZE";
        public static final String PG_DATABASE_TIMEOUT_IN_MS = "PG_DATABASE_TIMEOUT_IN_MS";
    }

    @UtilityClass
    public static class Measurement {
        public static final String POND_SENSOR_METRIC = "pond_sensor_metric";
        public static final String FIELD_ACIDITIY_PH = "acidityPh";
        public static final String FIELD_TEMPERATURE_CELSIUS = "temperatureCelsius";
    }

    @UtilityClass
    public static class DateTime {
        public static final String ZONE_ASIA_JAKARTA = "Asia/Jakarta";
        public static final int DEFAULT_MINUS_HOUR = 6;
    }

    @UtilityClass
    public static class Attribute {
        public static final String AUTHENTICATED_ACCOUNT = "authenticated_account";
        public static final String WRAPPED_REQUEST = "wrapped_request";
    }
}
