package id.ac.widyatama.smartfishmonitoringsystem.backend.app.controller;

import id.ac.widyatama.smartfishmonitoringsystem.backend.app.common.LambdaUtils;
import id.ac.widyatama.smartfishmonitoringsystem.backend.app.common.spark.status_code.HttpErrorTranslator;
import id.ac.widyatama.smartfishmonitoringsystem.backend.api_response.APIResponse;
import spark.Response;

import java.util.Collections;
import java.util.Optional;

public abstract class AbstractController {

    public abstract HttpErrorTranslator getHttpErrorTranslator();

    protected <T> APIResponse<T> handleErrorResponse(APIResponse<?> errorResult, Response response) {
        int httpStatusCode = getHttpStatusCode(errorResult);
        response.status(httpStatusCode);
        return errorResult.map(LambdaUtils::toValue);
    }

    private <T> Integer getHttpStatusCode(APIResponse<T> errorResult) {
        HttpErrorTranslator errorTranslator = getHttpErrorTranslator();
        return Optional.ofNullable(errorResult.getErrors())
                .orElse(Collections.emptyList())
                .stream()
                .findFirst()
                .map(errorTranslator::getStatusCode)
                .orElse(errorTranslator.getDefaultStatusCode());
    }
}
