package id.ac.widyatama.smartfishmonitoringsystem.backend.app.domain.dto;

import lombok.Builder;
import lombok.Value;

@Value
@Builder(setterPrefix = "set")
public class Pond {

    String guid;
    String name;
    String address;

    public static Pond createFrom(id.ac.widyatama.smartfishmonitoringsystem.backend.app.domain.psql.pond.Pond pond) {
        return Pond.builder()
                .setGuid(pond.getGuid())
                .setName(pond.getName())
                .setAddress(pond.getAddress())
                .build();
    }
}
