package id.ac.widyatama.smartfishmonitoringsystem.backend.app.domain.metric;

import id.ac.widyatama.smartfishmonitoringsystem.backend.app.common.context.FlowContext;

public class GetMetricsContext implements FlowContext {

    private static final String TEMPERATURE = "TEMPERATURE";
    private static final String ACIDITY = "ACIDITY";
    private final String name;

    private GetMetricsContext(String name) {
        this.name = name;
    }

    public static GetMetricsContext createForTemperature() {
        return new GetMetricsContext(TEMPERATURE);
    }

    public static GetMetricsContext createForAcidity() {
        return new GetMetricsContext(ACIDITY);
    }

    @Override
    public String getFlowContext() {
        return String.format("GET_%s_METRICS", name);
    }
}
