package id.ac.widyatama.smartfishmonitoringsystem.backend.app.controller.app.v1.ponds;

import id.ac.widyatama.smartfishmonitoringsystem.backend.api_response.APIResponse;
import id.ac.widyatama.smartfishmonitoringsystem.backend.app.Constant;
import id.ac.widyatama.smartfishmonitoringsystem.backend.app.common.context.RequestContext;
import id.ac.widyatama.smartfishmonitoringsystem.backend.app.common.spark.status_code.HttpErrorTranslator;
import id.ac.widyatama.smartfishmonitoringsystem.backend.app.controller.AbstractController;
import id.ac.widyatama.smartfishmonitoringsystem.backend.app.domain.authorization.AuthenticatedAccount;
import id.ac.widyatama.smartfishmonitoringsystem.backend.app.domain.contract.GetPondsResponse;
import id.ac.widyatama.smartfishmonitoringsystem.backend.app.domain.dto.Pond;
import id.ac.widyatama.smartfishmonitoringsystem.backend.app.domain.pond.GetPondsContext;
import id.ac.widyatama.smartfishmonitoringsystem.backend.app.domain.pond.PondService;
import id.ac.widyatama.smartfishmonitoringsystem.backend.error.Error;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import spark.Request;
import spark.Response;
import spark.Route;

import java.net.HttpURLConnection;
import java.util.List;
import java.util.Objects;

@Slf4j
@Getter
@AllArgsConstructor
public class GetPondsController extends AbstractController implements Route {

    private PondService pondService;
    private HttpErrorTranslator httpErrorTranslator;

    @Override
    public Object handle(Request request, Response response) throws Exception {
        RequestContext.putContext(new GetPondsContext());

        APIResponse<AuthenticatedAccount> storedAuthenticatedAccount = getStoredAuthenticatedAccount(request);
        if (!storedAuthenticatedAccount.isSuccess()) {
            return handleErrorResponse(storedAuthenticatedAccount, response);
        }

        log.info("Fetching data from database");
        AuthenticatedAccount authenticatedAccount = storedAuthenticatedAccount.getData();
        APIResponse<List<Pond>> result = pondService.getPondsByUser(authenticatedAccount.getGuid());
        if (!result.isSuccess()) {
            return handleErrorResponse(result, response);
        }

        return handleSuccessResponse(result, response);
    }

    private APIResponse<AuthenticatedAccount> getStoredAuthenticatedAccount(Request request) {
        return APIResponse.createResponse(() -> (AuthenticatedAccount) request.attribute(Constant.Attribute.AUTHENTICATED_ACCOUNT))
                .validate(Objects::nonNull, Error.Code.GENERIC_ERROR, "Must be with authenticated account");
    }

    private APIResponse<GetPondsResponse> handleSuccessResponse(APIResponse<List<Pond>> result, Response response) {
        response.status(HttpURLConnection.HTTP_OK);
        return result.map(data -> GetPondsResponse.builder().setPonds(data).build());
    }
}
