package id.ac.widyatama.smartfishmonitoringsystem.backend.app.domain.contract;

import id.ac.widyatama.smartfishmonitoringsystem.backend.api_response.APIResponse;
import id.ac.widyatama.smartfishmonitoringsystem.backend.app.domain.dto.TimeSelector;
import id.ac.widyatama.smartfishmonitoringsystem.backend.app.domain.dto.Validable;
import id.ac.widyatama.smartfishmonitoringsystem.backend.error.Error;
import lombok.Builder;
import lombok.Value;
import org.apache.commons.lang3.StringUtils;

import java.util.Objects;

@Value
@Builder(setterPrefix = "set")
public class GetPondTemperatureMetricsRequest implements Validable<GetPondTemperatureMetricsRequest> {

    TimeSelector selectedTime;
    String aggregateWindow;
    String pondId;

    public APIResponse<GetPondTemperatureMetricsRequest> validate() {
        return APIResponse.createResponse(() -> selectedTime)
                .validate(Objects::nonNull, Error.Code.FIELD_CANNOT_BE_BLANK, "request.selected_time")
                .flatMap(data -> selectedTime.validate())
                .map(data -> pondId)
                .validate(StringUtils::isNotBlank, Error.Code.FIELD_CANNOT_BE_BLANK, "request.pond_id")
                .map(data -> this);
    }
}
