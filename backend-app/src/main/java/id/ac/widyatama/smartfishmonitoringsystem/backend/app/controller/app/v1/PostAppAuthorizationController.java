package id.ac.widyatama.smartfishmonitoringsystem.backend.app.controller.app.v1;

import id.ac.widyatama.smartfishmonitoringsystem.backend.app.Constant;
import id.ac.widyatama.smartfishmonitoringsystem.backend.app.common.LogUtils;
import id.ac.widyatama.smartfishmonitoringsystem.backend.app.common.context.FlowContext;
import id.ac.widyatama.smartfishmonitoringsystem.backend.app.common.context.RequestContext;
import id.ac.widyatama.smartfishmonitoringsystem.backend.app.common.spark.status_code.HttpErrorTranslator;
import id.ac.widyatama.smartfishmonitoringsystem.backend.app.controller.AbstractController;
import id.ac.widyatama.smartfishmonitoringsystem.backend.api_response.APIResponse;
import id.ac.widyatama.smartfishmonitoringsystem.backend.app.domain.authorization.AuthenticatedAccount;
import id.ac.widyatama.smartfishmonitoringsystem.backend.error.Error;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import spark.Request;
import spark.Response;
import spark.Route;

import java.net.HttpURLConnection;
import java.util.Objects;

@Slf4j
@Getter
@AllArgsConstructor
public class PostAppAuthorizationController extends AbstractController implements Route {

    private HttpErrorTranslator httpErrorTranslator;

    @Override
    public APIResponse<AuthenticatedAccount> handle(Request request, Response response) {
        FlowContext context = new AuthorizationContext();
        RequestContext.putContext(context);

        log.info("Getting data from request");

        APIResponse<AuthenticatedAccount> authenticatedAccount = getAuthenticatedAccount(request);
        return handleAuthorizationResult(authenticatedAccount, response);
    }

    private APIResponse<AuthenticatedAccount> getAuthenticatedAccount(Request request) {
        return APIResponse.createResponse(() -> (AuthenticatedAccount) request.attribute(Constant.Attribute.AUTHENTICATED_ACCOUNT))
                .validate(Objects::nonNull, Error.Code.GENERIC_ERROR, "authenticated_account", "Cannot be null");
    }

    private APIResponse<AuthenticatedAccount> handleAuthorizationResult(APIResponse<AuthenticatedAccount> result,
                                                                        Response response) {
        return result.peek(data -> {
                    log.info(String.format("Successfully authorized: %s", data));
                    response.status(HttpURLConnection.HTTP_OK);
                },
                errors -> {
                    log.error(LogUtils.getErrorLogMarkers(errors), "Failed to authorize");
                    response.status(httpErrorTranslator.getStatusCode(errors.get(0)));
                });
    }

    private static class AuthorizationContext implements FlowContext {

        @Override
        public String getFlowContext() {
            return "APP_V1_AUTH";
        }
    }
}
