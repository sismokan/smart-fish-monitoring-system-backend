package id.ac.widyatama.smartfishmonitoringsystem.backend.app.common.spark.filter;

import id.ac.widyatama.smartfishmonitoringsystem.backend.app.Constant;
import id.ac.widyatama.smartfishmonitoringsystem.backend.app.common.spark.JsonResponseTransformer;
import id.ac.widyatama.smartfishmonitoringsystem.backend.app.common.spark.status_code.HttpErrorTranslator;
import id.ac.widyatama.smartfishmonitoringsystem.backend.api_response.APIResponse;
import id.ac.widyatama.smartfishmonitoringsystem.backend.app.domain.authorization.AuthenticationService;
import id.ac.widyatama.smartfishmonitoringsystem.backend.error.Error;
import id.ac.widyatama.smartfishmonitoringsystem.backend.error.ErrorUtils;
import lombok.AllArgsConstructor;
import org.apache.http.entity.ContentType;
import spark.Filter;
import spark.Request;
import spark.Response;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static spark.Spark.halt;

@AllArgsConstructor
public class BasicAuthFilter implements Filter {

    private AuthenticationService authenticationService;
    private JsonResponseTransformer jsonResponseTransformer;
    private HttpErrorTranslator httpErrorTranslator;

    @Override
    public void handle(Request request, Response response) {
        APIResponse.createResponse(() -> request.headers("Authorization"))
                .flatMap((data) -> authenticationService.authorizeUsingBasic(data))
                .changeErrors(this::changeErrorsToInvalidToken)
                .peek(data -> request.attribute(Constant.Attribute.AUTHENTICATED_ACCOUNT, data),
                        errors -> handleFailedResponse(errors, response));
    }

    private void handleFailedResponse(List<Error> errors, Response response) {
        int statusCode = httpErrorTranslator.getStatusCode(errors.get(0));
        response.header("Content-Type", ContentType.APPLICATION_JSON.toString());
        halt(statusCode, jsonResponseTransformer.render(APIResponse.createErrorResponse(errors)));
    }

    private List<Error> changeErrorsToInvalidToken(List<Error> errors) {
        return Optional.ofNullable(errors)
                .orElse(Collections.emptyList())
                .stream()
                .filter(ErrorUtils::isGenericError)
                .findFirst()
                .map(Collections::singletonList)
                .orElse(ErrorUtils.createErrors(Error.Code.INVALID_AUTHORIZATION, "token"));
    }
}
