package id.ac.widyatama.smartfishmonitoringsystem.backend.app.domain.psql;

import com.gojek.ApplicationConfiguration;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import id.ac.widyatama.smartfishmonitoringsystem.backend.api_response.APIResponse;
import org.jdbi.v3.core.Jdbi;
import org.jdbi.v3.sqlobject.SqlObjectPlugin;

import javax.sql.DataSource;
import java.util.function.Function;

public class BaseRepository<T> {

    private static final Object DATA_SOURCE_LOCK = new Object();
    volatile static Jdbi jdbi;

    public BaseRepository(ApplicationConfiguration configuration) {
        initializeConnectionPool(configuration);
    }

    private static void initializeConnectionPool(ApplicationConfiguration configuration) {
        if (jdbi != null) {
            return;
        }

        synchronized (DATA_SOURCE_LOCK) {
            if (jdbi == null) {
                DBConnectionParam param = DBConnectionParam.createFromConfig(configuration);
                DataSource source = getHikariDataSource(param);
                initializeJDBI(source);
            }
        }
    }

    private static void initializeJDBI(DataSource source) {
        jdbi = Jdbi.create(source);
        jdbi.installPlugin(new SqlObjectPlugin());
    }

    private static HikariDataSource getHikariDataSource(DBConnectionParam param) {
        HikariConfig config = param.toHikariConfig();
        return new HikariDataSource(config);
    }

    public static Jdbi getJdbi() {
        return jdbi;
    }

    protected <R> APIResponse<R> execute(Class<T> dBInterfaceClass, Function<T, R> sqlExecution) {
        return APIResponse.createResponse(() -> jdbi.withExtension(dBInterfaceClass, sqlExecution::apply));
    }
}
