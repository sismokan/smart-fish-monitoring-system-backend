package id.ac.widyatama.smartfishmonitoringsystem.backend.app.domain.contract;

import id.ac.widyatama.smartfishmonitoringsystem.backend.api_response.APIResponse;
import id.ac.widyatama.smartfishmonitoringsystem.backend.app.domain.dto.Acidity;
import id.ac.widyatama.smartfishmonitoringsystem.backend.app.domain.dto.Temperature;
import id.ac.widyatama.smartfishmonitoringsystem.backend.app.domain.dto.Validable;
import id.ac.widyatama.smartfishmonitoringsystem.backend.app.domain.influxdb.pond_sensor.PondSensorMetric;
import id.ac.widyatama.smartfishmonitoringsystem.backend.error.Error;
import lombok.Builder;
import lombok.Value;
import org.apache.commons.lang3.StringUtils;

import java.time.OffsetDateTime;
import java.util.Objects;

@Value
@Builder(setterPrefix = "set", toBuilder = true)
public class PublishPondMetricRequest implements Validable<PublishPondMetricRequest> {

    OffsetDateTime takenAt;
    Temperature temperature;
    Acidity acidity;
    String pondId;

    public APIResponse<PublishPondMetricRequest> validate() {
        return APIResponse.createResponse(() -> takenAt)
                .validate(Objects::nonNull, Error.Code.FIELD_CANNOT_BE_BLANK, "request.taken_at")
                .map(data -> temperature)
                .validate(Objects::nonNull, Error.Code.FIELD_CANNOT_BE_BLANK, "request.temperature")
                .flatMap(Temperature::validate)
                .map(data -> acidity)
                .validate(Objects::nonNull, Error.Code.FIELD_CANNOT_BE_BLANK, "request.acidity")
                .flatMap(Acidity::validate)
                .map(data -> pondId)
                .validate(StringUtils::isNotBlank, Error.Code.FIELD_CANNOT_BE_BLANK, "request.pond_id")
                .map(data -> this);
    }

    public PondSensorMetric toPondSensorMetric() {
        return PondSensorMetric.builder()
                .setTemperatureCelsius(temperature.getValue())
                .setAcidityPh(acidity.getValue())
                .setPondId(pondId)
                .setTime(takenAt.toInstant())
                .build();
    }
}
