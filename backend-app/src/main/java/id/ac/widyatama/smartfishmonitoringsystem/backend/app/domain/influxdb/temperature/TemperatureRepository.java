package id.ac.widyatama.smartfishmonitoringsystem.backend.app.domain.influxdb.temperature;

import id.ac.widyatama.smartfishmonitoringsystem.backend.api_response.APIResponse;
import id.ac.widyatama.smartfishmonitoringsystem.backend.app.domain.influxdb.PondRepository;
import id.ac.widyatama.smartfishmonitoringsystem.backend.app.domain.influxdb.InfluxDBWrapper;
import id.ac.widyatama.smartfishmonitoringsystem.backend.app.domain.influxdb.query.GetRecordsByPondId;
import lombok.extern.slf4j.Slf4j;

import java.util.List;

@Slf4j
public class TemperatureRepository extends PondRepository<TemperatureMetric> {

    public TemperatureRepository(InfluxDBWrapper influxDBWrapper) {
        super(influxDBWrapper);
    }

    public APIResponse<List<TemperatureMetric>> getRecordsByPondId(GetRecordsByPondId request) {
        return super.getRecordsByPondId(request, TemperatureMetric::createFromFluxRecord);
    }
}
