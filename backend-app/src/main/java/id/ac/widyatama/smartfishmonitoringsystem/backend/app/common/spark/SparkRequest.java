package id.ac.widyatama.smartfishmonitoringsystem.backend.app.common.spark;

import lombok.Value;

import java.util.Optional;
import java.util.Set;

@Value
public class SparkRequest implements Request {

    spark.Request originalRequest;

    public static Request wrap(spark.Request request) {
        return new SparkRequest(request);
    }

    @Override
    public String getMethod() {
        return originalRequest.requestMethod();
    }

    @Override
    public String getUri() {
        return originalRequest.uri();
    }

    @Override
    public Optional<String> getBody() {
        return Optional.ofNullable(originalRequest.body());
    }

    @Override
    public Set<String> getHeaders() {
        return originalRequest.headers();
    }

    @Override
    public Set<String> getQueryParams() {
        return originalRequest.queryParams();
    }

    public String getHeaderValue(String key) {
        return originalRequest.headers(key);
    }

    @Override
    public String getQueryParamValue(String key) {
        return originalRequest.queryParams(key);
    }
}
