package id.ac.widyatama.smartfishmonitoringsystem.backend.app.domain.authorization;

import lombok.Builder;
import lombok.Value;

@Value
@Builder(setterPrefix = "set")
public class AuthenticatedAccount {

    String guid;
    String username;
}
