package id.ac.widyatama.smartfishmonitoringsystem.backend.app.domain.influxdb;

import com.gojek.ApplicationConfiguration;
import com.influxdb.client.InfluxDBClient;
import com.influxdb.client.InfluxDBClientFactory;
import id.ac.widyatama.smartfishmonitoringsystem.backend.app.Constant;

public class ClientFactory {

    public static InfluxDBClient create(ApplicationConfiguration configuration) {
        String host = configuration.getValueAsString(Constant.Config.INFLUXDB_HOST);
        String token = configuration.getValueAsString(Constant.Config.INFLUXDB_API_TOKEN);
        String organization = configuration.getValueAsString(Constant.Config.INFLUXDB_ORGANIZATION);
        String bucket = configuration.getValueAsString(Constant.Config.INFLUXDB_BUCKET);

        return InfluxDBClientFactory.create(host, token.toCharArray(), organization, bucket);
    }
}
