package id.ac.widyatama.smartfishmonitoringsystem.backend.app.domain.psql.account;

import id.ac.widyatama.smartfishmonitoringsystem.backend.app.domain.psql.dto.GetAccountByUserCredential;
import org.jdbi.v3.sqlobject.config.RegisterRowMapper;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.GetGeneratedKeys;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;

public interface AccountDAO {

    @SqlUpdate("INSERT INTO \"accounts\" " +
            "(guid, username, password, created_at) " +
            "VALUES " +
            "(:guid, :username, MD5(:password), NOW())")
    @GetGeneratedKeys
    Long createAccount(@BindBean Account account);

    @SqlQuery("SELECT a.guid, a.username FROM \"accounts\" AS a " +
            "WHERE " +
            "username = :username AND " +
            "password = MD5(:password) " +
            "LIMIT 1")
    @RegisterRowMapper(AccountMapper.class)
    Account getAccountBy(@BindBean GetAccountByUserCredential getAccountByUserCredential);
}

