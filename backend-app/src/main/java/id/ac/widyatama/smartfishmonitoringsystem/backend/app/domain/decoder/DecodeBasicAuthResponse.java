package id.ac.widyatama.smartfishmonitoringsystem.backend.app.domain.decoder;

import lombok.Builder;
import lombok.Value;

@Value
@Builder(setterPrefix = "set")
public class DecodeBasicAuthResponse {

    String username;
    String password;
}
