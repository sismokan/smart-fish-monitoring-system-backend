package id.ac.widyatama.smartfishmonitoringsystem.backend.app.controller.hardware.v1.ponds;

import com.google.gson.Gson;
import id.ac.widyatama.smartfishmonitoringsystem.backend.api_response.APIResponse;
import id.ac.widyatama.smartfishmonitoringsystem.backend.app.common.DateTimeUtils;
import id.ac.widyatama.smartfishmonitoringsystem.backend.app.common.LogUtils;
import id.ac.widyatama.smartfishmonitoringsystem.backend.app.common.context.FlowContext;
import id.ac.widyatama.smartfishmonitoringsystem.backend.app.common.context.RequestContext;
import id.ac.widyatama.smartfishmonitoringsystem.backend.app.common.spark.status_code.HttpErrorTranslator;
import id.ac.widyatama.smartfishmonitoringsystem.backend.app.controller.AbstractController;
import id.ac.widyatama.smartfishmonitoringsystem.backend.app.domain.contract.PublishPondMetricRequest;
import id.ac.widyatama.smartfishmonitoringsystem.backend.app.domain.contract.PublishPondMetricResponse;
import id.ac.widyatama.smartfishmonitoringsystem.backend.app.domain.metric.PublishMetricContext;
import id.ac.widyatama.smartfishmonitoringsystem.backend.app.domain.metric.PublishPondMetricService;
import id.ac.widyatama.smartfishmonitoringsystem.backend.error.Error;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import spark.Request;
import spark.Response;
import spark.Route;

import java.net.HttpURLConnection;
import java.util.Objects;

import static net.logstash.logback.marker.Markers.append;

@Slf4j
@AllArgsConstructor
@Getter
public class PublishPondMetricController extends AbstractController implements Route {

    private PublishPondMetricService publishPondMetricService;
    private HttpErrorTranslator httpErrorTranslator;
    private Gson gson;

    @Override
    public APIResponse<PublishPondMetricResponse> handle(Request request, Response response) {
        FlowContext context = new PublishMetricContext();
        RequestContext.putContext(context);

        log.info("Getting decoded data from request");
        APIResponse<PublishPondMetricRequest> decodedRequest = decodeRequest(request);
        if (!decodedRequest.isSuccess()) {
            return handleErrorResponse(decodedRequest, response);
        }

        log.info("Publishing metric");
        APIResponse<Void> result = publishMetric(decodedRequest.getData());
        if (!result.isSuccess()) {
            return handleErrorResponse(result, response);
        }

        return handleSuccessResponse(decodedRequest, response);
    }

    private APIResponse<PublishPondMetricResponse> handleSuccessResponse(APIResponse<PublishPondMetricRequest> decodedRequest, Response response) {
        response.status(HttpURLConnection.HTTP_OK);
        log.info("Publish metric succeeded");
        return APIResponse.createSuccessResponse(decodedRequest.getData())
                .map(data -> PublishPondMetricResponse.builder()
                        .setPondId(data.getPondId())
                        .setPublishedAt(data.getTakenAt())
                        .build());
    }

    private APIResponse<PublishPondMetricRequest> decodeRequest(Request request) {
        return APIResponse.createResponse(() -> gson.fromJson(request.body(), PublishPondMetricRequest.class))
                .changeErrors(Error.Code.MALFORMED_JSON, "request.body")
                .validate(Objects::nonNull, Error.Code.MALFORMED_JSON, "request.body", "Request body cannot be empty")
                .flatMap(data -> enrichWithPondId(request, data))
                .flatMap(this::enrichWithTakenAt)
                .flatMap(PublishPondMetricRequest::validate)
                .peek(
                        data -> log.info(append("pond_id", data.getPondId()),
                                String.format("Successfully decoded request: %s", data)),
                        errors -> log.error(LogUtils.getErrorLogMarkers(errors),
                                "Failed to decode request")
                );
    }

    private APIResponse<PublishPondMetricRequest> enrichWithPondId(Request request, PublishPondMetricRequest data) {
        PublishPondMetricRequest enrichedRequest = data.toBuilder().setPondId(request.params("pond_id")).build();
        return APIResponse.createSuccessResponse(enrichedRequest);
    }

    private APIResponse<PublishPondMetricRequest> enrichWithTakenAt(PublishPondMetricRequest data) {
        if (Objects.nonNull(data.getTakenAt())) {
            return APIResponse.createSuccessResponse(data);
        }

        PublishPondMetricRequest enrichedRequest = data.toBuilder().setTakenAt(DateTimeUtils.getNow()).build();
        return APIResponse.createSuccessResponse(enrichedRequest);
    }

    private APIResponse<Void> publishMetric(PublishPondMetricRequest decodedRequest) {
        return publishPondMetricService.publish(decodedRequest)
                .onError(errors -> log.error(LogUtils.getErrorLogMarkers(errors), "Failed to publish metric"));
    }
}
