package id.ac.widyatama.smartfishmonitoringsystem.backend.app.domain.pond;

import id.ac.widyatama.smartfishmonitoringsystem.backend.app.common.context.FlowContext;

public class GetPondsContext implements FlowContext {

    @Override
    public String getFlowContext() {
        return "GET_PONDS";
    }
}
