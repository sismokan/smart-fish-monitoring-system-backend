package id.ac.widyatama.smartfishmonitoringsystem.backend.app.common;

import id.ac.widyatama.smartfishmonitoringsystem.backend.error.Error;
import lombok.experimental.UtilityClass;
import org.slf4j.Marker;

import java.util.List;
import java.util.Objects;

import static net.logstash.logback.marker.Markers.append;

@UtilityClass
public class LogUtils {

    public static Marker getErrorLogMarkers(List<Error> errors) {
        if (Objects.isNull(errors)) {
            return null;
        }

        Marker marker = append("errors", errors.toString());
        errors.forEach(error -> marker.add(append("error_code", error.getCode())
                .and(append("error_entity", error.getEntity()))
                .and(append("error_cause", error.getCause()))));
        return marker;
    }
}
