package id.ac.widyatama.smartfishmonitoringsystem.backend.app.domain.influxdb.pond_sensor;

import com.influxdb.annotations.Column;
import com.influxdb.annotations.Measurement;
import id.ac.widyatama.smartfishmonitoringsystem.backend.app.Constant;
import lombok.Builder;
import lombok.Value;

import java.time.Instant;

@Value
@Builder(setterPrefix = "set")
@Measurement(name = Constant.Measurement.POND_SENSOR_METRIC)
public class PondSensorMetric {

    @Column(tag = true)
    String pondId;

    @Column
    Double acidityPh;

    @Column
    Double temperatureCelsius;

    @Column(timestamp = true)
    Instant time;
}
