package id.ac.widyatama.smartfishmonitoringsystem.backend.app.domain.influxdb.acidity;

import id.ac.widyatama.smartfishmonitoringsystem.backend.api_response.APIResponse;
import id.ac.widyatama.smartfishmonitoringsystem.backend.app.domain.influxdb.InfluxDBWrapper;
import id.ac.widyatama.smartfishmonitoringsystem.backend.app.domain.influxdb.PondRepository;
import id.ac.widyatama.smartfishmonitoringsystem.backend.app.domain.influxdb.query.GetRecordsByPondId;
import lombok.extern.slf4j.Slf4j;

import java.util.List;

@Slf4j
public class AcidityRepository extends PondRepository<AcidityMetric> {

    public AcidityRepository(InfluxDBWrapper influxDBWrapper) {
        super(influxDBWrapper);
    }

    public APIResponse<List<AcidityMetric>> getRecordsByPondId(GetRecordsByPondId request) {
        return super.getRecordsByPondId(request, AcidityMetric::createFromFluxRecord);
    }
}
