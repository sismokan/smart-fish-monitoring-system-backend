package id.ac.widyatama.smartfishmonitoringsystem.backend.app.domain.pond;

import id.ac.widyatama.smartfishmonitoringsystem.backend.api_response.APIResponse;
import id.ac.widyatama.smartfishmonitoringsystem.backend.app.domain.dto.Pond;
import id.ac.widyatama.smartfishmonitoringsystem.backend.app.domain.psql.pond.PondRepository;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public class PondService {

    private final PondRepository repository;

    public PondService(PondRepository repository) {
        this.repository = repository;
    }

    public APIResponse<List<Pond>> getPondsByUser(String userGuid) {
        return repository.getPondByUser(userGuid)
                .map(Collection::stream)
                .map(stream -> stream.map(Pond::createFrom))
                .map(stream -> stream.collect(Collectors.toList()));
    }
}
