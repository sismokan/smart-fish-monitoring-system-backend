package id.ac.widyatama.smartfishmonitoringsystem.backend.app.domain.influxdb.query;

import com.gojek.ApplicationConfiguration;
import id.ac.widyatama.smartfishmonitoringsystem.backend.app.Constant;
import id.ac.widyatama.smartfishmonitoringsystem.backend.app.domain.dto.TimeSelector;
import lombok.EqualsAndHashCode;
import org.apache.commons.lang3.StringUtils;

@EqualsAndHashCode
public class GetRecordsByPondId implements Query {

    private static final String DEFAULT_AGGREGATE_WINDOW = "10m";
    private final String bucket;
    private final TimeSelector timeSelector;
    private final String pondId;
    private final String measurement;
    private final String aggregateWindow;
    private final String field;

    public GetRecordsByPondId(String bucket,
                              TimeSelector timeSelector,
                              String pondId,
                              String measurement,
                              String aggregateWindow,
                              String field) {
        this.bucket = bucket;
        this.timeSelector = timeSelector;
        this.pondId = pondId;
        this.measurement = measurement;
        this.aggregateWindow = aggregateWindow;
        this.field = field;
    }

    public static GetRecordsByPondId withContextPondAcidity(ApplicationConfiguration configuration,
                                                            TimeSelector timeSelector,
                                                            String pondId,
                                                            String aggregateWindow) {
        return new GetRecordsByPondId(
                configuration.getValueAsString(Constant.Config.INFLUXDB_BUCKET),
                timeSelector,
                pondId,
                Constant.Measurement.POND_SENSOR_METRIC,
                StringUtils.defaultString(aggregateWindow, DEFAULT_AGGREGATE_WINDOW),
                Constant.Measurement.FIELD_ACIDITIY_PH
        );
    }

    public static GetRecordsByPondId withContextPondTemperature(ApplicationConfiguration configuration,
                                                         TimeSelector timeSelector,
                                                         String pondId,
                                                         String aggregateWindow) {
        return new GetRecordsByPondId(
                configuration.getValueAsString(Constant.Config.INFLUXDB_BUCKET),
                timeSelector,
                pondId,
                Constant.Measurement.POND_SENSOR_METRIC,
                StringUtils.defaultString(aggregateWindow, DEFAULT_AGGREGATE_WINDOW),
                Constant.Measurement.FIELD_TEMPERATURE_CELSIUS
        );
    }

    @Override
    public String toQuery() {
        return new StringBuilder()
                .append(String.format("from(bucket: \"%s\")", bucket))
                .append("\n")
                .append("|> ").append(String.format("range(start: %s, stop: %s)",
                        timeSelector.getStartTime().toString(), timeSelector.getEndTime().toString()))
                .append("\n")
                .append("|> ").append(String.format("filter(fn: (r) => r[\"_measurement\"] == \"%s\")", measurement))
                .append("\n")
                .append("|> ").append(String.format("filter(fn: (r) => r[\"pondId\"] == \"%s\")", pondId))
                .append("\n")
                .append("|> ").append(String.format("filter(fn: (r) => r[\"_field\"] == \"%s\")", field))
                .append("\n")
                .append("|> ").append(String.format("aggregateWindow(every: %s, fn: mean, createEmpty: true)", aggregateWindow))
                .append("\n")
                .append("|> ").append("yield(name: \"_result\")")
                .toString();
    }
}
