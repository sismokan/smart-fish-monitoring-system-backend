package id.ac.widyatama.smartfishmonitoringsystem.backend.app.common.context;

public interface FlowContext {

    String getFlowContext();
}
