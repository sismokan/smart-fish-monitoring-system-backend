package id.ac.widyatama.smartfishmonitoringsystem.backend.app.common;

import lombok.experimental.UtilityClass;

@UtilityClass
public class LambdaUtils {

    public static <U> Void toVoid(U x) {
        return null;
    }

    public static <T, V> T toValue(V x) {
        return null;
    }

}
