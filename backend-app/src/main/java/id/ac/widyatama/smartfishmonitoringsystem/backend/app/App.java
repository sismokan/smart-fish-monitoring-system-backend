package id.ac.widyatama.smartfishmonitoringsystem.backend.app;

public class App {

    public static void main(String[] args) {
        SparkServer server = ServerFactory.createServer();
        server.startServer();
    }

}
