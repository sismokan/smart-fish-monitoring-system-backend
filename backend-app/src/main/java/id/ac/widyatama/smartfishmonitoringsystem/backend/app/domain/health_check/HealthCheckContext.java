package id.ac.widyatama.smartfishmonitoringsystem.backend.app.domain.health_check;

import id.ac.widyatama.smartfishmonitoringsystem.backend.app.common.context.FlowContext;

public class HealthCheckContext implements FlowContext {
    @Override
    public String getFlowContext() {
        return "HEALTH_CHECK";
    }
}
