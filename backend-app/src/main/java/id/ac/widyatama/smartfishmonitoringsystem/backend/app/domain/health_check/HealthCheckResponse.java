package id.ac.widyatama.smartfishmonitoringsystem.backend.app.domain.health_check;

import lombok.Value;

@Value
public class HealthCheckResponse {

    private static final String MESSAGE_OK = "pong";
    private static final String MESSAGE_NOT_AVAILABLE = "unavailable";
    String message;

    public static HealthCheckResponse ok() {
        return new HealthCheckResponse(MESSAGE_OK);
    }

    public static HealthCheckResponse unavailable() {
        return new HealthCheckResponse(MESSAGE_NOT_AVAILABLE);
    }
}
