package id.ac.widyatama.smartfishmonitoringsystem.backend.app.controller;

import id.ac.widyatama.smartfishmonitoringsystem.backend.app.common.context.FlowContext;
import id.ac.widyatama.smartfishmonitoringsystem.backend.app.common.context.RequestContext;
import id.ac.widyatama.smartfishmonitoringsystem.backend.app.domain.health_check.HealthCheckContext;
import id.ac.widyatama.smartfishmonitoringsystem.backend.app.domain.health_check.HealthCheckResponse;
import spark.Request;
import spark.Response;
import spark.Route;

import java.net.HttpURLConnection;

public class HealthCheckController implements Route {

    private volatile boolean serving = false;

    @Override
    public HealthCheckResponse handle(Request request, Response res) {
        FlowContext context = new HealthCheckContext();
        RequestContext.putContext(context);

        if (serving) {
            res.status(HttpURLConnection.HTTP_OK);
            return HealthCheckResponse.ok();
        }

        res.status(HttpURLConnection.HTTP_UNAVAILABLE);
        return HealthCheckResponse.unavailable();
    }

    public void beginServing() {
        this.serving = true;
    }

    public void stopServing() {
        this.serving = false;
    }
}
