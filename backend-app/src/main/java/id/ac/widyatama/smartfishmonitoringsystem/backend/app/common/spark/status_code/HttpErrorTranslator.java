package id.ac.widyatama.smartfishmonitoringsystem.backend.app.common.spark.status_code;

import id.ac.widyatama.smartfishmonitoringsystem.backend.error.Error;

public interface HttpErrorTranslator {

    int getStatusCode(Error error);

    int getDefaultStatusCode();
}
