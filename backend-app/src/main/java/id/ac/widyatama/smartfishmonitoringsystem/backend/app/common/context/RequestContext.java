package id.ac.widyatama.smartfishmonitoringsystem.backend.app.common.context;

import org.slf4j.MDC;

public class RequestContext {

    private static final String CORRELATION_ID = "correlation_id";
    private static final String CONTEXT = "context";
    private static final String REQUEST_METHOD = "request_method";
    private static final String REQUEST_URI = "request_uri";

    public static void clear() {
        MDC.clear();
    }

    public static void putCorrelationId(String correlationId) {
        MDC.put(CORRELATION_ID, correlationId);
    }

    public static String getCorrelationId() {
        return MDC.get(CORRELATION_ID);
    }

    public static void putRequestMethod(String requestMethod) {
        MDC.put(REQUEST_METHOD, requestMethod);
    }

    public static void putRequestUri(String requestUri) {
        MDC.put(REQUEST_URI, requestUri);
    }

    public static void putContext(FlowContext flowContext) {
        MDC.put(CONTEXT, flowContext.getFlowContext());
    }

}
