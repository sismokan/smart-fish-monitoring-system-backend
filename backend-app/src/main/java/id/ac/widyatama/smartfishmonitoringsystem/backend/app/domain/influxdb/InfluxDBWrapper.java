package id.ac.widyatama.smartfishmonitoringsystem.backend.app.domain.influxdb;

import com.gojek.ApplicationConfiguration;
import com.influxdb.client.InfluxDBClient;
import com.influxdb.client.QueryApi;
import com.influxdb.client.WriteApiBlocking;
import com.influxdb.client.domain.WritePrecision;
import com.influxdb.query.FluxTable;
import id.ac.widyatama.smartfishmonitoringsystem.backend.api_response.APIResponse;

import java.util.List;

public class InfluxDBWrapper {

    private final WriteApiBlocking writeApi;
    private final QueryApi queryApi;

    private InfluxDBWrapper(WriteApiBlocking writeApi, QueryApi queryApi) {
        this.writeApi = writeApi;
        this.queryApi = queryApi;
    }

    public static InfluxDBWrapper create(InfluxDBClient client) {
        WriteApiBlocking writeApi = client.getWriteApiBlocking();
        QueryApi queryApi = client.getQueryApi();
        return new InfluxDBWrapper(writeApi, queryApi);
    }

    public static InfluxDBWrapper create(ApplicationConfiguration configuration) {
        InfluxDBClient influxDBClient = ClientFactory.create(configuration);
        return InfluxDBWrapper.create(influxDBClient);
    }

    public <T> APIResponse<T> write(T object) {
        return APIResponse.createResponse(() -> {
            writeApi.writeMeasurement(WritePrecision.NS, object);
            return object;
        });
    }

    public APIResponse<List<FluxTable>> read(String query) {
        return APIResponse.createResponse(() -> queryApi.query(query));
    }
}
