package id.ac.widyatama.smartfishmonitoringsystem.backend.app;

import com.gojek.ApplicationConfiguration;
import com.gojek.Figaro;
import com.google.gson.Gson;
import id.ac.widyatama.smartfishmonitoringsystem.backend.app.common.json.GsonBuilder;
import id.ac.widyatama.smartfishmonitoringsystem.backend.app.common.spark.ExceptionHandler;
import id.ac.widyatama.smartfishmonitoringsystem.backend.app.common.spark.JsonResponseTransformer;
import id.ac.widyatama.smartfishmonitoringsystem.backend.app.common.spark.filter.BasicAuthFilter;
import id.ac.widyatama.smartfishmonitoringsystem.backend.app.common.spark.filter.HttpContextRequestFilter;
import id.ac.widyatama.smartfishmonitoringsystem.backend.app.common.spark.status_code.BaseHttpErrorTranslator;
import id.ac.widyatama.smartfishmonitoringsystem.backend.app.common.spark.status_code.HttpErrorTranslator;
import id.ac.widyatama.smartfishmonitoringsystem.backend.app.controller.HealthCheckController;
import id.ac.widyatama.smartfishmonitoringsystem.backend.app.controller.app.v1.PostAppAuthorizationController;
import id.ac.widyatama.smartfishmonitoringsystem.backend.app.controller.app.v1.ponds.GetPondAcidityMetricsController;
import id.ac.widyatama.smartfishmonitoringsystem.backend.app.controller.app.v1.ponds.GetPondTemperatureMetricsController;
import id.ac.widyatama.smartfishmonitoringsystem.backend.app.controller.app.v1.ponds.GetPondsController;
import id.ac.widyatama.smartfishmonitoringsystem.backend.app.controller.hardware.v1.ponds.PublishPondMetricController;
import id.ac.widyatama.smartfishmonitoringsystem.backend.app.domain.authorization.AuthenticationService;
import id.ac.widyatama.smartfishmonitoringsystem.backend.app.domain.decoder.DecoderService;
import id.ac.widyatama.smartfishmonitoringsystem.backend.app.domain.influxdb.InfluxDBWrapper;
import id.ac.widyatama.smartfishmonitoringsystem.backend.app.domain.influxdb.acidity.AcidityRepository;
import id.ac.widyatama.smartfishmonitoringsystem.backend.app.domain.influxdb.pond_sensor.PondSensorRepository;
import id.ac.widyatama.smartfishmonitoringsystem.backend.app.domain.influxdb.temperature.TemperatureRepository;
import id.ac.widyatama.smartfishmonitoringsystem.backend.app.domain.metric.GetPondMetricsService;
import id.ac.widyatama.smartfishmonitoringsystem.backend.app.domain.metric.PublishPondMetricService;
import id.ac.widyatama.smartfishmonitoringsystem.backend.app.domain.pond.PondService;
import id.ac.widyatama.smartfishmonitoringsystem.backend.app.domain.psql.account.AccountRepository;
import id.ac.widyatama.smartfishmonitoringsystem.backend.app.domain.psql.pond.PondRepository;
import lombok.experimental.UtilityClass;

import java.util.Collections;

@UtilityClass
public class ServerFactory {

    public static SparkServer createServer() {
        ApplicationConfiguration configuration = Figaro.configure(Collections.emptySet());
        Gson gson = GsonBuilder.getDefaultGsonBuilder().create();
        InfluxDBWrapper influxDBWrapper = InfluxDBWrapper.create(configuration);

        AccountRepository accountRepository = new AccountRepository(configuration);
        DecoderService decoderService = new DecoderService();
        AuthenticationService authenticationService = new AuthenticationService(decoderService, accountRepository);

        HttpErrorTranslator baseHttpErrorTranslator = new BaseHttpErrorTranslator();
        JsonResponseTransformer jsonResponseTransformer = new JsonResponseTransformer(gson);
        HttpContextRequestFilter contextRequestFilter = new HttpContextRequestFilter();
        ExceptionHandler<Exception> exceptionHandler = new ExceptionHandler<>(jsonResponseTransformer);
        BasicAuthFilter basicAuthFilter = new BasicAuthFilter(
                authenticationService, jsonResponseTransformer, baseHttpErrorTranslator
        );

        HealthCheckController healthCheckController = new HealthCheckController();

        GetPondMetricsService getPondMetricsService = createGetPondMetricsService(influxDBWrapper, configuration);

        PublishPondMetricController publishPondMetricController = createPublishPondMetricController(
                influxDBWrapper, baseHttpErrorTranslator, gson
        );
        GetPondTemperatureMetricsController getPondTemperatureMetricsController = createGetTemperatureMetricsController(
                getPondMetricsService, baseHttpErrorTranslator
        );
        GetPondAcidityMetricsController getPondAcidityMetricsController = createGetAcidityMetricsController(
                getPondMetricsService, baseHttpErrorTranslator
        );
        PostAppAuthorizationController postAppAuthorizationController = new PostAppAuthorizationController(
                baseHttpErrorTranslator
        );
        GetPondsController getPondsController = new GetPondsController(
                createPondService(configuration),
                baseHttpErrorTranslator
        );

        return new SparkServer(configuration, jsonResponseTransformer, healthCheckController,
                contextRequestFilter, publishPondMetricController, postAppAuthorizationController,
                getPondTemperatureMetricsController, getPondAcidityMetricsController, getPondsController,
                exceptionHandler, basicAuthFilter);
    }

    private PublishPondMetricController createPublishPondMetricController(InfluxDBWrapper influxDBWrapper,
                                                                          HttpErrorTranslator httpErrorTranslator,
                                                                          Gson gson) {
        PondSensorRepository pondSensorRepository = new PondSensorRepository(influxDBWrapper);
        PublishPondMetricService publishPondMetricService = new PublishPondMetricService(pondSensorRepository);
        return new PublishPondMetricController(
                publishPondMetricService,
                httpErrorTranslator,
                gson
        );
    }

    private GetPondTemperatureMetricsController createGetTemperatureMetricsController(GetPondMetricsService getPondMetricsService,
                                                                                      HttpErrorTranslator httpErrorTranslator) {
        return new GetPondTemperatureMetricsController(
                getPondMetricsService,
                httpErrorTranslator
        );
    }

    private GetPondAcidityMetricsController createGetAcidityMetricsController(GetPondMetricsService getPondMetricsService,
                                                                              HttpErrorTranslator httpErrorTranslator) {
        return new GetPondAcidityMetricsController(
                getPondMetricsService,
                httpErrorTranslator
        );
    }

    private GetPondMetricsService createGetPondMetricsService(InfluxDBWrapper influxDBWrapper, ApplicationConfiguration configuration) {
        TemperatureRepository pondTemperatureRepository = new TemperatureRepository(influxDBWrapper);
        AcidityRepository pondAcidityRepository = new AcidityRepository(influxDBWrapper);
        return new GetPondMetricsService(
                pondAcidityRepository,
                pondTemperatureRepository,
                configuration
        );
    }

    private PondService createPondService(ApplicationConfiguration configuration) {
        PondRepository pondRepository = new PondRepository(configuration);
        return new PondService(
                pondRepository
        );
    }
}
