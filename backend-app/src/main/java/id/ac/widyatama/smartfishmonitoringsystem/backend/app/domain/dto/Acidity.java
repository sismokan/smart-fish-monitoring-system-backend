package id.ac.widyatama.smartfishmonitoringsystem.backend.app.domain.dto;

import id.ac.widyatama.smartfishmonitoringsystem.backend.api_response.APIResponse;
import id.ac.widyatama.smartfishmonitoringsystem.backend.error.Error;
import lombok.Builder;
import lombok.Value;
import org.apache.commons.lang3.StringUtils;

import java.time.OffsetDateTime;
import java.util.Objects;

@Value
@Builder(setterPrefix = "set")
public class Acidity implements Validable<Acidity> {

    Double value;
    String measurement;
    OffsetDateTime takenAt;

    public APIResponse<Acidity> validate() {
        return APIResponse.createResponse(() -> value)
                .validate(Objects::nonNull, Error.Code.FIELD_CANNOT_BE_BLANK, "acidity.value")
                .map(data -> measurement)
                .validate(StringUtils::isNotBlank, Error.Code.FIELD_CANNOT_BE_BLANK, "acidity.measurement")
                .map(data -> this);
    }
}
