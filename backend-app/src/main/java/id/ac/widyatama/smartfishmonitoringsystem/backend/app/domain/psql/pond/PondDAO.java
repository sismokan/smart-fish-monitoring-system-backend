package id.ac.widyatama.smartfishmonitoringsystem.backend.app.domain.psql.pond;

import org.jdbi.v3.sqlobject.config.RegisterRowMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.GetGeneratedKeys;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;

import java.util.List;

public interface PondDAO {

    @SqlUpdate("INSERT INTO \"ponds\" " +
            "(guid, user_guid, name, address, created_at) " +
            "VALUES " +
            "(:guid, :userGuid, :name, :address, NOW())")
    @GetGeneratedKeys
    Long createPond(@BindBean Pond pond);

    @SqlQuery("SELECT guid, name, address FROM \"ponds\" " +
            "WHERE " +
            "user_guid = :user_guid")
    @RegisterRowMapper(PondMapper.class)
    List<Pond> getPondsByUser(@Bind("user_guid") String userGuid);
}
