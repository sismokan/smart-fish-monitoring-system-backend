package id.ac.widyatama.smartfishmonitoringsystem.backend.app.common.json;

import com.google.gson.FieldNamingPolicy;
import lombok.experimental.UtilityClass;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.time.ZonedDateTime;

@UtilityClass
public class GsonBuilder {

    public static com.google.gson.GsonBuilder getDefaultGsonBuilder() {
        return new com.google.gson.GsonBuilder()
                .serializeNulls()
                .setDateFormat("yyyy-MM-dd'T'HH:mm:ssXXX")
                .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
                .registerTypeAdapter(LocalDateTime.class, new LocalDateTimeSerializer())
                .registerTypeAdapter(OffsetDateTime.class, new OffsetDateTimeSerializer())
                .registerTypeAdapter(ZonedDateTime.class, new ZonedDateTimeSerializer())
                .registerTypeAdapter(LocalDate.class, new LocalDateSerializer());
    }

}
