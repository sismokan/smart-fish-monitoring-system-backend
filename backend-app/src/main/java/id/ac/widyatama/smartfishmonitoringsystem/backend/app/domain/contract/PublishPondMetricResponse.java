package id.ac.widyatama.smartfishmonitoringsystem.backend.app.domain.contract;

import lombok.Builder;
import lombok.Value;

import java.time.OffsetDateTime;

@Value
@Builder(setterPrefix = "set")
public class PublishPondMetricResponse {

    String pondId;
    OffsetDateTime publishedAt;
}
