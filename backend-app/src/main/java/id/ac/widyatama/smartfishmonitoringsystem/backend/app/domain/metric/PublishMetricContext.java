package id.ac.widyatama.smartfishmonitoringsystem.backend.app.domain.metric;

import id.ac.widyatama.smartfishmonitoringsystem.backend.app.common.context.FlowContext;

public class PublishMetricContext implements FlowContext {
    @Override
    public String getFlowContext() {
        return "PUBLISH_METRIC";
    }
}
