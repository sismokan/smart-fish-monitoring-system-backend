package id.ac.widyatama.smartfishmonitoringsystem.backend.app.domain.influxdb.query;

public interface Query {

    String toQuery();
}
