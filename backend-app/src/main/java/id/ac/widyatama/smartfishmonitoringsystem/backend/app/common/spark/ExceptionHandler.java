package id.ac.widyatama.smartfishmonitoringsystem.backend.app.common.spark;

import id.ac.widyatama.smartfishmonitoringsystem.backend.app.common.LogUtils;
import id.ac.widyatama.smartfishmonitoringsystem.backend.api_response.APIResponse;
import id.ac.widyatama.smartfishmonitoringsystem.backend.error.Error;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Marker;
import spark.Request;
import spark.Response;

import javax.net.ssl.HttpsURLConnection;
import java.util.List;

import static net.logstash.logback.marker.Markers.append;

@Slf4j
public class ExceptionHandler<T extends Exception> implements spark.ExceptionHandler<T> {

    private final JsonResponseTransformer jsonResponseTransformer;

    public ExceptionHandler(JsonResponseTransformer jsonResponseTransformer) {
        this.jsonResponseTransformer = jsonResponseTransformer;
    }

    @Override
    public void handle(Exception exception, Request request, Response response) {
        APIResponse<Object> errorResponse = APIResponse.createErrorResponse(Error.Code.GENERIC_ERROR, "", "");
        Marker errorLogMarker = createErrorLogMarker(exception, errorResponse.getErrors());
        log.error(errorLogMarker, "Caught exception");
        response.status(HttpsURLConnection.HTTP_INTERNAL_ERROR);
        response.body(jsonResponseTransformer.render(errorResponse));
    }

    public Marker createErrorLogMarker(Exception exception, List<Error> errors) {
        Marker errorMarker = LogUtils.getErrorLogMarkers(errors);
        errorMarker.add(append("exception_stack_trace", ExceptionUtils.getStackTrace(exception)));
        errorMarker.add(append("exception_message", ExceptionUtils.getMessage(exception)));
        return errorMarker;
    }
}
