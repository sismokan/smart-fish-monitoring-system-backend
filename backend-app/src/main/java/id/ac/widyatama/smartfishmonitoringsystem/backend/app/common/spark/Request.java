package id.ac.widyatama.smartfishmonitoringsystem.backend.app.common.spark;

import org.apache.commons.lang3.tuple.Pair;

import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

public interface Request {

    String getMethod();

    String getUri();

    Optional<String> getBody();

    Set<String> getHeaders();

    Set<String> getQueryParams();

    String getHeaderValue(String key);

    String getQueryParamValue(String key);

    default Map<String, String> getRequestHeaders() {
        return getHeaders()
                .stream()
                .map(key -> Pair.of(key, getHeaderValue(key)))
                .collect(Collectors.toMap(Pair::getKey, Pair::getValue));
    }

    default Map<String, String> getRequestQueryParams() {
        return getQueryParams()
                .stream()
                .map(key -> Pair.of(key, getQueryParamValue(key)))
                .collect(Collectors.toMap(Pair::getKey, Pair::getValue));
    }
}
