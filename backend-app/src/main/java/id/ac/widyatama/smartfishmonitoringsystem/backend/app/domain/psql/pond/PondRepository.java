package id.ac.widyatama.smartfishmonitoringsystem.backend.app.domain.psql.pond;

import com.gojek.ApplicationConfiguration;
import id.ac.widyatama.smartfishmonitoringsystem.backend.api_response.APIResponse;
import id.ac.widyatama.smartfishmonitoringsystem.backend.app.domain.psql.BaseRepository;

import java.util.List;

public class PondRepository extends BaseRepository<PondDAO> {

    public PondRepository(ApplicationConfiguration configuration) {
        super(configuration);
    }

    public APIResponse<List<Pond>> getPondByUser(String userGuid) {
        return execute(PondDAO.class, dao -> dao.getPondsByUser(userGuid));
    }
}
