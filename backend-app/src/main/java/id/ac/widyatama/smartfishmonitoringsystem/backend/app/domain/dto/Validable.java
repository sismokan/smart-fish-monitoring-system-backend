package id.ac.widyatama.smartfishmonitoringsystem.backend.app.domain.dto;

import id.ac.widyatama.smartfishmonitoringsystem.backend.api_response.APIResponse;

public interface Validable<T> {

    APIResponse<T> validate();
}
