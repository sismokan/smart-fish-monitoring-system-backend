package id.ac.widyatama.smartfishmonitoringsystem.backend.app.domain.metric;

import id.ac.widyatama.smartfishmonitoringsystem.backend.app.common.LambdaUtils;
import id.ac.widyatama.smartfishmonitoringsystem.backend.api_response.APIResponse;
import id.ac.widyatama.smartfishmonitoringsystem.backend.app.domain.contract.PublishPondMetricRequest;
import id.ac.widyatama.smartfishmonitoringsystem.backend.app.domain.influxdb.pond_sensor.PondSensorRepository;

public class PublishPondMetricService {

    private final PondSensorRepository pondSensorRepository;

    public PublishPondMetricService(PondSensorRepository pondSensorRepository) {
        this.pondSensorRepository = pondSensorRepository;
    }

    public APIResponse<Void> publish(PublishPondMetricRequest request) {
        return APIResponse.createResponse(request::toPondSensorMetric)
                .map(pondSensorRepository::saveRecord)
                .map(LambdaUtils::toVoid);
    }
}
