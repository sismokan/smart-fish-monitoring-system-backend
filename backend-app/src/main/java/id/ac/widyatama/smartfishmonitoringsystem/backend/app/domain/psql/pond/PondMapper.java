package id.ac.widyatama.smartfishmonitoringsystem.backend.app.domain.psql.pond;

import org.jdbi.v3.core.mapper.RowMapper;
import org.jdbi.v3.core.statement.StatementContext;

import java.sql.ResultSet;
import java.sql.SQLException;

public class PondMapper implements RowMapper<Pond> {

    @Override
    public Pond map(ResultSet rs, StatementContext ctx) throws SQLException {
        return Pond.builder()
                .setGuid(rs.getString("guid"))
                .setName(rs.getString("name"))
                .setAddress(rs.getString("address"))
                .build();
    }
}
