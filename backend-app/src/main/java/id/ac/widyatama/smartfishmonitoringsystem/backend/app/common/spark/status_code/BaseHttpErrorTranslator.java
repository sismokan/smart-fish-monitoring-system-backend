package id.ac.widyatama.smartfishmonitoringsystem.backend.app.common.spark.status_code;

import id.ac.widyatama.smartfishmonitoringsystem.backend.error.Error;

import java.net.HttpURLConnection;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class BaseHttpErrorTranslator implements HttpErrorTranslator {

    private static final Map<Error.Code, Integer> statusMap;

    static {
        statusMap = new HashMap<>() {
            {
                put(Error.Code.GENERIC_ERROR, HttpURLConnection.HTTP_INTERNAL_ERROR);
                put(Error.Code.UNKNOWN, HttpURLConnection.HTTP_INTERNAL_ERROR);
                put(Error.Code.MALFORMED_JSON, HttpURLConnection.HTTP_BAD_REQUEST);
                put(Error.Code.MALFORMED_DATA, HttpURLConnection.HTTP_BAD_REQUEST);
                put(Error.Code.FIELD_CANNOT_BE_BLANK, HttpURLConnection.HTTP_BAD_REQUEST);
                put(Error.Code.RESOURCE_CANNOT_BE_FOUND, HttpURLConnection.HTTP_NOT_FOUND);
                put(Error.Code.INVALID_AUTHORIZATION, HttpURLConnection.HTTP_UNAUTHORIZED);
            }
        };
    }

    @Override
    public int getStatusCode(Error error) {
        return Optional.ofNullable(statusMap.get(error.getCode()))
                .orElse(getDefaultStatusCode());
    }

    @Override
    public int getDefaultStatusCode() {
        return HttpURLConnection.HTTP_INTERNAL_ERROR;
    }
}
