package id.ac.widyatama.smartfishmonitoringsystem.backend.app.domain.influxdb.temperature;

import com.influxdb.query.FluxRecord;
import id.ac.widyatama.smartfishmonitoringsystem.backend.app.common.DateTimeUtils;
import id.ac.widyatama.smartfishmonitoringsystem.backend.app.domain.dto.Temperature;
import lombok.Value;

import java.time.Instant;

@Value
public class TemperatureMetric {

    private static final String CELSIUS = "CELSIUS";
    String measurement;
    Double value;
    Instant time;

    public static TemperatureMetric createFromFluxRecord(FluxRecord fluxRecord) {
        return new TemperatureMetric(
                CELSIUS,
                (Double) fluxRecord.getValue(),
                fluxRecord.getTime()
        );
    }

    public Temperature toDTOTemperature() {
        return Temperature.builder()
                .setTakenAt(time.atOffset(DateTimeUtils.getJakartaZone()))
                .setMeasurement(measurement)
                .setValue(value)
                .build();
    }
}

