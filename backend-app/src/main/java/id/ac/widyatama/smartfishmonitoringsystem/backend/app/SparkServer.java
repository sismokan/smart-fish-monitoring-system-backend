package id.ac.widyatama.smartfishmonitoringsystem.backend.app;

import com.gojek.ApplicationConfiguration;
import id.ac.widyatama.smartfishmonitoringsystem.backend.app.common.context.RequestContext;
import id.ac.widyatama.smartfishmonitoringsystem.backend.app.common.spark.ExceptionHandler;
import id.ac.widyatama.smartfishmonitoringsystem.backend.app.common.spark.JsonResponseTransformer;
import id.ac.widyatama.smartfishmonitoringsystem.backend.app.common.spark.filter.BasicAuthFilter;
import id.ac.widyatama.smartfishmonitoringsystem.backend.app.common.spark.filter.HttpContextRequestFilter;
import id.ac.widyatama.smartfishmonitoringsystem.backend.app.controller.HealthCheckController;
import id.ac.widyatama.smartfishmonitoringsystem.backend.app.controller.app.v1.PostAppAuthorizationController;
import id.ac.widyatama.smartfishmonitoringsystem.backend.app.controller.app.v1.ponds.GetPondAcidityMetricsController;
import id.ac.widyatama.smartfishmonitoringsystem.backend.app.controller.app.v1.ponds.GetPondTemperatureMetricsController;
import id.ac.widyatama.smartfishmonitoringsystem.backend.app.controller.app.v1.ponds.GetPondsController;
import id.ac.widyatama.smartfishmonitoringsystem.backend.app.controller.hardware.v1.ponds.PublishPondMetricController;
import lombok.Value;
import lombok.extern.slf4j.Slf4j;

import static spark.Spark.*;

@Slf4j
@Value
public class SparkServer {

    ApplicationConfiguration configuration;
    JsonResponseTransformer jsonResponseTransformer;
    HealthCheckController healthCheckController;
    HttpContextRequestFilter contextRequestFilter;
    PublishPondMetricController publishPondMetricController;
    PostAppAuthorizationController postAppAuthorizationController;
    GetPondTemperatureMetricsController getPondTemperatureMetricsController;
    GetPondAcidityMetricsController getPondAcidityMetricsController;
    GetPondsController getPondsController;
    ExceptionHandler<Exception> exceptionHandler;
    BasicAuthFilter basicAuthFilter;

    public void startServer() {
        port(configuration.getValueAsInt(Constant.Config.PORT, Constant.Server.DEFAULT_PORT));
        defaultResponseTransformer(jsonResponseTransformer);
        exception(Exception.class, exceptionHandler);

        before(contextRequestFilter);

        get("/ping", healthCheckController);

        before("/hardware/*", basicAuthFilter);
        path("/hardware", () -> {
            path("/v1", () -> {
                path("/ponds/:pond_id", () -> {
                    post("/metric", publishPondMetricController);
                });
            });
        });

        before("/app/*", basicAuthFilter);
        path("/app/v1", () -> {
            post("/auth", postAppAuthorizationController);
            path("/ponds", () -> {
                get("", getPondsController);
                path("/:pond_id/metric", () -> {
                    get("/temperature", getPondTemperatureMetricsController);
                    get("/acidity", getPondAcidityMetricsController);
                });
            });
        });

        afterAfter((request, response) -> RequestContext.clear());
        after("/*", (request, response) -> response.type("application/json"));

        awaitInitialization();
        healthCheckController.beginServing();
        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            log.info("Shutting down app...");
            stopServer();
            log.info("Closing Thread...");
        }));
    }

    public void stopServer() {
        long shutdownTimeout = configuration.getValueAsLong(Constant.Config.SHUTDOWN_TIMEOUT_MS, Constant.Server.DEFAULT_SHUTDOWN_TIMEOUT_MS);
        stopServer(shutdownTimeout);
    }

    public void stopServer(long timeout) {
        healthCheckController.stopServing();
        try {
            Thread.sleep(timeout);
            stop();
            awaitStop();
        } catch (InterruptedException e) {
            log.error("Error when stopping server!");
        }
    }
}
