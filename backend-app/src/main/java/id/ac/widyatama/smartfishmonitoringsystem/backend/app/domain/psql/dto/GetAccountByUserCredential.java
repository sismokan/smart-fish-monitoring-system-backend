package id.ac.widyatama.smartfishmonitoringsystem.backend.app.domain.psql.dto;

import lombok.Builder;
import lombok.Value;

@Value
@Builder(setterPrefix = "set")
public class GetAccountByUserCredential {

    String username;
    String password;
}
