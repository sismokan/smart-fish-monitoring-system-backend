package id.ac.widyatama.smartfishmonitoringsystem.backend.app.domain.metric;

import com.gojek.ApplicationConfiguration;
import id.ac.widyatama.smartfishmonitoringsystem.backend.api_response.APIResponse;
import id.ac.widyatama.smartfishmonitoringsystem.backend.app.domain.contract.GetPondAcidityMetricsRequest;
import id.ac.widyatama.smartfishmonitoringsystem.backend.app.domain.contract.GetPondTemperatureMetricsRequest;
import id.ac.widyatama.smartfishmonitoringsystem.backend.app.domain.dto.Acidity;
import id.ac.widyatama.smartfishmonitoringsystem.backend.app.domain.dto.Temperature;
import id.ac.widyatama.smartfishmonitoringsystem.backend.app.domain.influxdb.acidity.AcidityMetric;
import id.ac.widyatama.smartfishmonitoringsystem.backend.app.domain.influxdb.acidity.AcidityRepository;
import id.ac.widyatama.smartfishmonitoringsystem.backend.app.domain.influxdb.query.GetRecordsByPondId;
import id.ac.widyatama.smartfishmonitoringsystem.backend.app.domain.influxdb.temperature.TemperatureMetric;
import id.ac.widyatama.smartfishmonitoringsystem.backend.app.domain.influxdb.temperature.TemperatureRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@AllArgsConstructor
public class GetPondMetricsService {

    private final AcidityRepository pondAcidityRepository;
    private final TemperatureRepository pondTemperatureRepository;
    private final ApplicationConfiguration configuration;

    public APIResponse<List<Temperature>> getTemperatureMetrics(GetPondTemperatureMetricsRequest request) {
        return APIResponse.createResponse(() -> GetRecordsByPondId.withContextPondTemperature(
                        configuration,
                        request.getSelectedTime(),
                        request.getPondId(),
                        request.getAggregateWindow()
                )).flatMap(pondTemperatureRepository::getRecordsByPondId)
                .map(Collection::stream)
                .map(stream -> stream.map(TemperatureMetric::toDTOTemperature))
                .map(stream -> stream.collect(Collectors.toList()));
    }

    public APIResponse<List<Acidity>> getAcidityMetrics(GetPondAcidityMetricsRequest request) {
        return APIResponse.createResponse(() -> GetRecordsByPondId.withContextPondAcidity(
                        configuration,
                        request.getSelectedTime(),
                        request.getPondId(),
                        request.getAggregateWindow()
                )).flatMap(pondAcidityRepository::getRecordsByPondId)
                .map(Collection::stream)
                .map(stream -> stream.map(AcidityMetric::toDTOTemperature))
                .map(stream -> stream.collect(Collectors.toList()));
    }
}
