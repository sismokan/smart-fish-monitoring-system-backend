CREATE TABLE IF NOT EXISTS ponds (
    id serial,
    guid VARCHAR (255) UNIQUE NOT NULL,
    user_guid VARCHAR (255) NOT NULL,
    created_at TIMESTAMP NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (user_guid)
        REFERENCES accounts (guid)
);