CREATE TABLE IF NOT EXISTS accounts (
    id serial,
    guid VARCHAR (255) UNIQUE NOT NULL,
    username VARCHAR (255) UNIQUE NOT NULL,
    password VARCHAR (255) NOT NULL,
    created_at TIMESTAMP NOT NULL,
    PRIMARY KEY (id)
);
