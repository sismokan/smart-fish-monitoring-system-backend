package id.ac.widyatama.smartfishmonitoringsystem.backend.app.common.json;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import lombok.Value;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ZonedDateTimeSerializerTest {
    private static final Gson gson = new GsonBuilder()
            .registerTypeAdapter(ZonedDateTime.class, new ZonedDateTimeSerializer())
            .create();

    @Test
    void shouldSerializeLocalDate() {
        String expectedDate = "{\"dateTime\":\"2011-04-19T23:59:59+07:00[Asia/Jakarta]\"}";
        ZonedDateTime dateTime = ZonedDateTime.of(LocalDateTime.of(2011, 4, 19, 23, 59, 59), ZoneId.of("Asia/Jakarta"));
        TestZonedDateTime actualZonedDateTime = new TestZonedDateTime(dateTime);

        assertEquals(expectedDate, gson.toJson(actualZonedDateTime));
    }

    @Test
    void shouldDeserializeLocalDate() {
        ZonedDateTime datetime = ZonedDateTime.of(LocalDateTime.of(2011, 4, 19, 23, 59, 59), ZoneId.of("Asia/Jakarta"));
        String expectedDate = "{\"dateTime\":\"2011-04-19T23:59:59+07:00[Asia/Jakarta]\"}";

        TestZonedDateTime expected = new TestZonedDateTime(datetime);

        TestZonedDateTime actual = gson.fromJson(expectedDate, TestZonedDateTime.class);
        assertEquals(expected, actual);
    }
}

@Value
class TestZonedDateTime {
    private ZonedDateTime dateTime;
}