package id.ac.widyatama.smartfishmonitoringsystem.backend.integration;

import id.ac.widyatama.smartfishmonitoringsystem.backend.app.App;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpHost;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import spark.Spark;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static spark.Spark.awaitInitialization;
import static spark.Spark.awaitStop;

public class HealthCheckIntegrationTest {

    @Nested
    class TestPing {

        private CloseableHttpClient httpClient;

        @BeforeEach
        public void beforeEach() {
            httpClient = HttpClientBuilder.create().build();
            App.main(new String[]{});
            awaitInitialization();
        }

        @AfterEach
        void tearDown() {
            Spark.stop();
            awaitStop();
        }

        @Test
        public void shouldReturnOk() throws Exception {
            Map<String, Object> expectedResponseMap = new HashMap<>() {
                {
                    put("response_code", 200);
                    put("response_body", "{\"message\":\"pong\"}");
                }
            };

            Map<String, Object> responseMap = callPing();

            assertEquals(expectedResponseMap, responseMap);
        }

        private Map<String, Object> callPing() throws IOException {
            HttpGet request = new HttpGet("/ping");
            CloseableHttpResponse response = httpClient.execute(HttpHost.create("http://127.0.0.1:4000"), request);
            Map<String, Object> responseMap = new HashMap<>();
            responseMap.put("response_code", response.getStatusLine().getStatusCode());
            responseMap.put("response_body", IOUtils.toString(response.getEntity().getContent(), Charset.defaultCharset()));
            return responseMap;
        }
    }

}
