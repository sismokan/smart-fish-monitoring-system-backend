package id.ac.widyatama.smartfishmonitoringsystem.backend.integration.hardware.v1.pond;

import com.gojek.Figaro;
import com.influxdb.query.FluxRecord;
import com.influxdb.query.FluxTable;
import id.ac.widyatama.smartfishmonitoringsystem.backend.app.App;
import id.ac.widyatama.smartfishmonitoringsystem.backend.app.domain.psql.account.Account;
import id.ac.widyatama.smartfishmonitoringsystem.backend.helper.FileUtils;
import id.ac.widyatama.smartfishmonitoringsystem.backend.helper.InfluxDBUtils;
import id.ac.widyatama.smartfishmonitoringsystem.backend.helper.PSQLUtils;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpHost;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.junit.jupiter.api.*;
import org.skyscreamer.jsonassert.JSONAssert;
import spark.Spark;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.Charset;
import java.time.Instant;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static spark.Spark.awaitInitialization;
import static spark.Spark.awaitStop;

public class PublishPondMetricIntegrationTest {

    private static CloseableHttpClient httpClient;
    private static PSQLUtils.DBHelper dbHelper;

    @BeforeAll
    public static void setUpAll() {
        dbHelper = new PSQLUtils.DBHelper(Figaro.configure(Collections.emptySet()));
        httpClient = HttpClientBuilder.create().build();
        App.main(new String[]{});
        awaitInitialization();
    }

    @AfterAll
    public static void tearDownAll() {
        InfluxDBUtils.deleteAllRecords();
        dbHelper.truncateAll();
        Spark.stop();
        awaitStop();
    }

    @BeforeEach
    public void setUpEach() {
        InfluxDBUtils.deleteAllRecords();
        dbHelper.truncateAll();
        dbHelper.createAccount(Account.builder()
                .setGuid(UUID.randomUUID().toString())
                .setUsername("qornanali")
                .setPassword("ali123")
                .build());
    }

    private Map<String, Object> callAPI(String requestBody, String authToken) throws IOException {
        return callAPI("http://127.0.0.1:4000", requestBody, "3264b0ab-710a-4ad4-871c-fa742d5cbb96", authToken);
    }

    private Map<String, Object> callAPI(String host, String requestBody, String pondId, String authToken) throws IOException {
        HttpPost request = new HttpPost(String.format("/hardware/v1/ponds/%s/metric", pondId));
        request.addHeader("Authorization", String.format("Basic %s", authToken));
        request.addHeader("Content-Type", "application/json");
        request.addHeader("debug-id", "some-correlation-id");
        request.setEntity(new StringEntity(requestBody));
        CloseableHttpResponse response = httpClient.execute(HttpHost.create(host), request);
        Map<String, Object> responseMap = new HashMap<>();
        responseMap.put("response_code", response.getStatusLine().getStatusCode());
        responseMap.put("response_body", IOUtils.toString(response.getEntity().getContent(), Charset.defaultCharset()));
        return responseMap;
    }

    private String getAuthToken() {
        return getAuthToken("qornanali", "ali123");
    }

    private String getAuthToken(String username, String password) {
        return new String(Base64.getEncoder().encode(String.format("%s:%s", username, password).getBytes()));
    }

    @Nested
    class GivenSuccessScenario {

        @Test
        public void shouldReturnOk() throws Exception {
            URL responseBodyFileURL = getClass().getResource("/response/hardware/v1/ponds/publish-metric/success.json");
            String expectedResponseBody = FileUtils.getFileContentAsString(responseBodyFileURL);

            URL requestBodyFileURL = getClass().getResource("/request/hardware/v1/ponds/publish_metric/basic.json");
            String responseBody = FileUtils.getFileContentAsString(requestBodyFileURL);
            Map<String, Object> actualResponseMap = callAPI(responseBody, getAuthToken());

            JSONAssert.assertEquals(expectedResponseBody, (String) actualResponseMap.get("response_body"), true);
            assertEquals(HttpURLConnection.HTTP_OK, actualResponseMap.get("response_code"));

            Instant expectedPublishAt = OffsetDateTime.of(2022, 5, 28, 10, 25, 26, 439486000, ZoneOffset.UTC).toInstant();

            String query = "from(bucket: \"bucket_smart_fist_monitoring_system\")\n" +
                    "  |> range(start: 2022-05-28T00:00:00.439486Z, stop: 2022-05-30T00:00:00.439486Z)\n" +
                    "  |> filter(fn: (r) => r[\"_measurement\"] == \"pond_sensor_metric\")\n" +
                    "  |> yield(name: \"_result\")";
            List<FluxTable> tables = InfluxDBUtils.getRecords(query);

            FluxTable acidityTable = tables.get(0);
            FluxRecord acidityRecord = acidityTable.getRecords().get(0);
            assertEquals("3264b0ab-710a-4ad4-871c-fa742d5cbb96", acidityRecord.getValueByKey("pondId"));
            assertEquals(10.0, acidityRecord.getValueByKey("_value"));
            assertEquals(expectedPublishAt, acidityRecord.getTime());

            FluxTable temperatureTable = tables.get(1);
            FluxRecord temperatureRecord = temperatureTable.getRecords().get(0);
            assertEquals("3264b0ab-710a-4ad4-871c-fa742d5cbb96", temperatureRecord.getValueByKey("pondId"));
            assertEquals(22.5, temperatureRecord.getValueByKey("_value"));
            assertEquals(expectedPublishAt, temperatureRecord.getTime());
        }
    }

    @Nested
    class GivenFailureScenario {

        @Test
        public void withInvalidJSONShouldReturnBadRequest() throws Exception {
            URL responseBodyFileURL = getClass().getResource("/response/hardware/v1/ponds/publish-metric/invalid_json.json");
            String expectedResponseBody = FileUtils.getFileContentAsString(responseBodyFileURL);

            URL requestBodyFileURL = getClass().getResource("/request/hardware/v1/ponds/publish_metric/invalid_json.json");
            Map<String, Object> actualResponseMap = callAPI(FileUtils.getFileContentAsString(requestBodyFileURL), getAuthToken());

            JSONAssert.assertEquals(expectedResponseBody, (String) actualResponseMap.get("response_body"), true);
            assertEquals(HttpURLConnection.HTTP_BAD_REQUEST, actualResponseMap.get("response_code"));
        }

        @Test
        public void withEmptyBodyShouldReturnBadRequest() throws Exception {
            URL responseBodyFileURL = getClass().getResource("/response/hardware/v1/ponds/publish-metric/empty_body.json");
            String expectedResponseBody = FileUtils.getFileContentAsString(responseBodyFileURL);

            URL requestBodyFileURL = getClass().getResource("/request/hardware/v1/ponds/publish_metric/empty_body.json");
            Map<String, Object> actualResponseMap = callAPI(FileUtils.getFileContentAsString(requestBodyFileURL), getAuthToken());

            JSONAssert.assertEquals(expectedResponseBody, (String) actualResponseMap.get("response_body"), true);
            assertEquals(HttpURLConnection.HTTP_BAD_REQUEST, actualResponseMap.get("response_code"));
        }

        @Test
        public void withInvalidAuthenticationCredentialShouldReturnUnauthorized() throws Exception {
            URL responseBodyFileURL = getClass().getResource("/response/hardware/v1/ponds/publish-metric/invalid_credential.json");
            String expectedResponseBody = FileUtils.getFileContentAsString(responseBodyFileURL);

            URL requestBodyFileURL = getClass().getResource("/request/hardware/v1/ponds/publish_metric/basic.json");
            Map<String, Object> actualResponseMap = callAPI(FileUtils.getFileContentAsString(requestBodyFileURL), getAuthToken("foo", "bar"));

            JSONAssert.assertEquals(expectedResponseBody, (String) actualResponseMap.get("response_body"), true);
            assertEquals(HttpURLConnection.HTTP_UNAUTHORIZED, actualResponseMap.get("response_code"));
        }

        @Test
        public void withInvalidAuthenticationTokenShouldReturnUnauthorized() throws Exception {
            URL responseBodyFileURL = getClass().getResource("/response/hardware/v1/ponds/publish-metric/invalid_auth_token.json");
            String expectedResponseBody = FileUtils.getFileContentAsString(responseBodyFileURL);

            URL requestBodyFileURL = getClass().getResource("/request/hardware/v1/ponds/publish_metric/basic.json");
            Map<String, Object> actualResponseMap = callAPI(FileUtils.getFileContentAsString(requestBodyFileURL), "foo123isinvalid");

            JSONAssert.assertEquals(expectedResponseBody, (String) actualResponseMap.get("response_body"), true);
            assertEquals(HttpURLConnection.HTTP_UNAUTHORIZED, actualResponseMap.get("response_code"));
        }
    }
}
