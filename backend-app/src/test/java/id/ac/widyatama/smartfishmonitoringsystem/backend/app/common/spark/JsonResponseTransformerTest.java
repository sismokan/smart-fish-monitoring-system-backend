package id.ac.widyatama.smartfishmonitoringsystem.backend.app.common.spark;

import com.google.gson.GsonBuilder;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class JsonResponseTransformerTest {

    @Nested
    class TestRender {

        @Test
        public void shouldTranslateObjectToJsonString() {
            JsonResponseTransformer jsonResponseTransformer = new JsonResponseTransformer(new GsonBuilder().create());
            Object obj = new Object();
            assertEquals("{}", jsonResponseTransformer.render(obj));
        }
    }
}