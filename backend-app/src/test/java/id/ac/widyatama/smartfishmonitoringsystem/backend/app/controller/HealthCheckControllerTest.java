package id.ac.widyatama.smartfishmonitoringsystem.backend.app.controller;

import id.ac.widyatama.smartfishmonitoringsystem.backend.app.domain.health_check.HealthCheckResponse;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import spark.Request;
import spark.Response;

import java.net.HttpURLConnection;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

class HealthCheckControllerTest {

    @Nested
    class TestHandle {

        private HealthCheckController healthCheckController;
        private Request request;
        private Response response;

        @BeforeEach
        public void setUp() {
            healthCheckController = new HealthCheckController();
            request = mock(Request.class);
            response = mock(Response.class);
        }

        @Test
        public void givenServingIsActiveShouldReturnOk() {
            healthCheckController.beginServing();

            HealthCheckResponse actualResponse = healthCheckController.handle(request, response);

            assertEquals(HealthCheckResponse.ok(), actualResponse);
            verify(response, times(1)).status(HttpURLConnection.HTTP_OK);
        }

        @Test
        public void givenServingIsInactiveShouldReturnUnavailable() {
            healthCheckController.stopServing();

            HealthCheckResponse actualResponse = healthCheckController.handle(request, response);

            assertEquals(HealthCheckResponse.unavailable(), actualResponse);
            verify(response, times(1)).status(HttpURLConnection.HTTP_UNAVAILABLE);
        }
    }
}