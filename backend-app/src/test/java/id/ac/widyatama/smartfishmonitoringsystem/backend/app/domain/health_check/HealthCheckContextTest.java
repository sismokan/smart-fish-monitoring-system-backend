package id.ac.widyatama.smartfishmonitoringsystem.backend.app.domain.health_check;

import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class HealthCheckContextTest {

    @Nested
    class TestGetFlowContext {

        @Test
        public void shouldReturnContext() {
            HealthCheckContext healthCheckContext = new HealthCheckContext();

            assertEquals("HEALTH_CHECK", healthCheckContext.getFlowContext());
        }
    }
}