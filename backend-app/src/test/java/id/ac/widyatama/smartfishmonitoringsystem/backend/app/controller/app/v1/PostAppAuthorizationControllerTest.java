package id.ac.widyatama.smartfishmonitoringsystem.backend.app.controller.app.v1;

import id.ac.widyatama.smartfishmonitoringsystem.backend.app.Constant;
import id.ac.widyatama.smartfishmonitoringsystem.backend.app.common.spark.status_code.HttpErrorTranslator;
import id.ac.widyatama.smartfishmonitoringsystem.backend.api_response.APIResponse;
import id.ac.widyatama.smartfishmonitoringsystem.backend.app.domain.authorization.AuthenticatedAccount;
import id.ac.widyatama.smartfishmonitoringsystem.backend.error.Error;
import id.ac.widyatama.smartfishmonitoringsystem.backend.error.ErrorUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import spark.Request;
import spark.Response;

import java.net.HttpURLConnection;
import java.util.Collections;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

class PostAppAuthorizationControllerTest {

    @Nested
    class TestHandle {

        private Request request;
        private Response response;
        private HttpErrorTranslator httpErrorTranslator;
        private PostAppAuthorizationController controller;

        @BeforeEach
        public void setUp() {
            request = mock(Request.class);
            response = mock(Response.class);
            httpErrorTranslator = mock(HttpErrorTranslator.class);
            controller = new PostAppAuthorizationController(httpErrorTranslator);
        }

        @Test
        public void givenThereIsNoAuthenticatedAccountStoredShouldReturnGenericError() {
            Error genericError = ErrorUtils.createError(Error.Code.GENERIC_ERROR, "authenticated_account", "Cannot be null");
            APIResponse<AuthenticatedAccount> expectedResponse = APIResponse.createErrorResponse(Collections.singletonList(genericError));
            when(httpErrorTranslator.getStatusCode(eq(genericError))).thenReturn(HttpURLConnection.HTTP_INTERNAL_ERROR);

            APIResponse<AuthenticatedAccount> actualResponse = controller.handle(request, response);

            assertEquals(expectedResponse, actualResponse);
            verify(response, times(1)).status(HttpURLConnection.HTTP_INTERNAL_ERROR);
        }

        @Test
        public void givenThereAuthenticatedAccountStoredShouldReturnData() {
            AuthenticatedAccount authenticatedAccount = AuthenticatedAccount.builder()
                    .setUsername("foobar")
                    .setGuid("123")
                    .build();
            when(request.attribute(Constant.Attribute.AUTHENTICATED_ACCOUNT)).thenReturn(authenticatedAccount);
            APIResponse<AuthenticatedAccount> expectedResponse = APIResponse.createSuccessResponse(authenticatedAccount);

            APIResponse<AuthenticatedAccount> actualResponse = controller.handle(request, response);

            assertEquals(expectedResponse, actualResponse);
            verify(response, times(1)).status(HttpURLConnection.HTTP_OK);
        }
    }
}