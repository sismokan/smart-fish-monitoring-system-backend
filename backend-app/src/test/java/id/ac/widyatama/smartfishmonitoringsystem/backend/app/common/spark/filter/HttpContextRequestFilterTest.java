package id.ac.widyatama.smartfishmonitoringsystem.backend.app.common.spark.filter;

import id.ac.widyatama.smartfishmonitoringsystem.backend.app.common.spark.SparkRequest;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.slf4j.MDC;
import spark.Request;
import spark.Response;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

class HttpContextRequestFilterTest {

    @Nested
    class TestHandle {

        @BeforeEach
        public void setUp() {
            MDC.clear();
        }

        @AfterEach
        public void tearDown() {
            MDC.clear();
        }

        @Test
        public void shouldPutRequestInformationToMDC() {
            Request request = mock(Request.class);
            when(request.headers("debug-id")).thenReturn("some-correlation-id");
            when(request.requestMethod()).thenReturn("GET");
            when(request.uri()).thenReturn("/foobar");

            HttpContextRequestFilter requestFilter = new HttpContextRequestFilter();
            requestFilter.handle(request, mock(Response.class));

            assertEquals("some-correlation-id", MDC.get("correlation_id"));
            assertEquals("GET", MDC.get("request_method"));
            assertEquals("/foobar", MDC.get("request_uri"));
            verify(request, times(1)).attribute("wrapped_request", SparkRequest.wrap(request));
        }
    }
}