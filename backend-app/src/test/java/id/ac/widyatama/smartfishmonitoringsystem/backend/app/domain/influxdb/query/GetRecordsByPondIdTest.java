package id.ac.widyatama.smartfishmonitoringsystem.backend.app.domain.influxdb.query;

import id.ac.widyatama.smartfishmonitoringsystem.backend.app.domain.dto.TimeSelector;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.time.Instant;
import java.time.ZoneOffset;
import java.time.temporal.ChronoUnit;

import static org.junit.jupiter.api.Assertions.assertEquals;

class GetRecordsByPondIdTest {

    @Nested
    class TestToQuery {

        @Test
        public void shouldReturnString() {
            Instant now = Instant.parse("2021-12-03T10:15:30.00Z");
            GetRecordsByPondId getRecordsByPondId = new GetRecordsByPondId(
                    "some_bucket",
                    TimeSelector.builder()
                            .setStartTime(now.minus(1, ChronoUnit.DAYS).atOffset(ZoneOffset.UTC))
                            .setEndTime(now.plus(1, ChronoUnit.DAYS).atOffset(ZoneOffset.UTC))
                            .build(),
                    "foobar",
                    "some_measurement",
                    "1m",
                    "loremipsum"
            );
            assertEquals("from(bucket: \"some_bucket\")\n" +
                            "|> range(start: 2021-12-02T10:15:30Z, stop: 2021-12-04T10:15:30Z)\n" +
                            "|> filter(fn: (r) => r[\"_measurement\"] == \"some_measurement\")\n" +
                            "|> filter(fn: (r) => r[\"pondId\"] == \"foobar\")\n" +
                            "|> filter(fn: (r) => r[\"_field\"] == \"loremipsum\")\n" +
                            "|> aggregateWindow(every: 1m, fn: mean, createEmpty: true)\n" +
                            "|> yield(name: \"_result\")",
                    getRecordsByPondId.toQuery());
        }
    }
}