package id.ac.widyatama.smartfishmonitoringsystem.backend.app.domain.decoder;

import id.ac.widyatama.smartfishmonitoringsystem.backend.api_response.APIResponse;
import id.ac.widyatama.smartfishmonitoringsystem.backend.error.Error;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.Base64;

import static org.junit.jupiter.api.Assertions.assertEquals;

class DecoderServiceTest {

    @Nested
    class TestDecodeBasicAuth {

        private DecoderService decoderService;

        @BeforeEach
        public void setUp() {
            decoderService = new DecoderService();
        }

        @Test
        public void givenInvalidTokenShouldReturnError() {
            APIResponse expectedResponse = APIResponse.createErrorResponse(Error.Code.MALFORMED_DATA, "authorization.basic",
                    "Input byte[] should at least have 2 bytes for base64 bytes");
            assertEquals(expectedResponse, decoderService.decodeBasicAuth("foobar"));
        }

        @Test
        public void givenValidTokenShouldReturnSucceed() {
            DecodeBasicAuthResponse decodedBasicAuth = DecodeBasicAuthResponse.builder()
                    .setUsername("foo")
                    .setPassword("bar")
                    .build();
            APIResponse expectedResponse = APIResponse.createSuccessResponse(decodedBasicAuth);

            String authToken = String.format("Basic %s", new String(Base64.getEncoder().encode("foo:bar".getBytes())));
            assertEquals(expectedResponse, decoderService.decodeBasicAuth(authToken));
        }
    }
}