package id.ac.widyatama.smartfishmonitoringsystem.backend.app.domain.metric;

import id.ac.widyatama.smartfishmonitoringsystem.backend.app.domain.influxdb.pond_sensor.PondSensorRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.mockito.Mockito.mock;

class PublishPondMetricServiceTest {

    private PondSensorRepository repository;
    private PublishPondMetricService publishPondMetricService;

    @BeforeEach
    public void setUp() {
        repository = mock(PondSensorRepository.class);
        publishPondMetricService = new PublishPondMetricService(repository);
    }

    @Nested
    class TestPublish {

        @Test
        public void givenPublishAcidityMetricFailedShouldReturnError() {
//            OffsetDateTime now = DateTimeUtils.getNow();
//            PublishPondMetricRequest request = PublishPondMetricRequest.builder()
//                    .setAcidity(Acidity.builder()
//                            .setValue(10.0)
//                            .setMeasurement("POWER_OF_HIDROGEN")
//                            .build())
//                    .setTemperature(Temperature.builder()
//                            .setValue(10.0)
//                            .setMeasurement("CELCIUS")
//                            .build())
//                    .setTakenAt(now)
//                    .setPondId("foobar")
//                    .build();
//            PondAcidityMetric pondAcidityMetric = PondAcidityMetric.builder()
//                    .setTime(now.toInstant())
//                    .setMeasurement("POWER_OF_HIDROGEN")
//                    .setValue(10.0)
//                    .setPondId("foobar")
//                    .build();
//            PondTemperatureMetric pondTemperatureMetric = PondTemperatureMetric.builder()
//                    .setTime(now.toInstant())
//                    .setMeasurement("CELCIUS")
//                    .setValue(10.0)
//                    .setPondId("foobar")
//                    .build();
//
//            APIResponse<Void> actualResponse = publishPondMetricService.publish(request);
//
//            assertEquals(APIResponse.createSuccessResponse(null).map(LambdaUtils::toValue), actualResponse);
//            verify(publishAcidityMetricJob, times(1)).withData(eq(pondAcidityMetric));
//            verify(publishTemperatureMetricJob, times(1)).withData(eq(pondTemperatureMetric));
        }
    }
}