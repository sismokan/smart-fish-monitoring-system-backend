package id.ac.widyatama.smartfishmonitoringsystem.backend.integration.app.v1.auth;

import com.gojek.Figaro;
import id.ac.widyatama.smartfishmonitoringsystem.backend.app.App;
import id.ac.widyatama.smartfishmonitoringsystem.backend.app.domain.psql.account.Account;
import id.ac.widyatama.smartfishmonitoringsystem.backend.helper.FileUtils;
import id.ac.widyatama.smartfishmonitoringsystem.backend.helper.InfluxDBUtils;
import id.ac.widyatama.smartfishmonitoringsystem.backend.helper.PSQLUtils;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpHost;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.junit.jupiter.api.*;
import org.skyscreamer.jsonassert.JSONAssert;
import spark.Spark;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static spark.Spark.awaitInitialization;
import static spark.Spark.awaitStop;

public class PostAppAuthorizationIntegrationTest {

    private static CloseableHttpClient httpClient;
    private static PSQLUtils.DBHelper dbHelper;

    @BeforeAll
    public static void setUpAll() {
        dbHelper = new PSQLUtils.DBHelper(Figaro.configure(Collections.emptySet()));
        httpClient = HttpClientBuilder.create().build();
        App.main(new String[]{});
        awaitInitialization();
    }

    @AfterAll
    public static void tearDownAll() {
        InfluxDBUtils.deleteAllRecords();
        dbHelper.truncateAll();
        Spark.stop();
        awaitStop();
    }

    @BeforeEach
    public void setUpEach() {
        InfluxDBUtils.deleteAllRecords();
        dbHelper.truncateAll();
        dbHelper.createAccount(Account.builder()
                .setGuid("d259cb31-33ef-45a8-a807-010c4442521b")
                .setUsername("qornanali")
                .setPassword("ali123")
                .build());
    }

    private String getAuthToken() {
        return getAuthToken("qornanali", "ali123");
    }

    private String getAuthToken(String username, String password) {
        return new String(Base64.getEncoder().encode(String.format("%s:%s", username, password).getBytes()));
    }

    private Map<String, Object> callAPI(String authToken) throws IOException {
        HttpPost request = new HttpPost("/app/v1/auth");
        request.addHeader("Authorization", String.format("Basic %s", authToken));
        request.addHeader("Content-Type", "application/json");
        request.addHeader("debug-id", "some-correlation-id");
        CloseableHttpResponse response = httpClient.execute(HttpHost.create("http://127.0.0.1:4000"), request);
        Map<String, Object> responseMap = new HashMap<>();
        responseMap.put("response_code", response.getStatusLine().getStatusCode());
        responseMap.put("response_body", IOUtils.toString(response.getEntity().getContent(), Charset.defaultCharset()));
        return responseMap;
    }

    @Nested
    class GivenSuccessScenario {

        @Test
        public void shouldReturnOk() throws Exception {
            URL responseBodyFileURL = getClass().getResource("/response/app/v1/auth/success.json");
            String expectedResponseBody = FileUtils.getFileContentAsString(responseBodyFileURL);

            Map<String, Object> actualResponseMap = callAPI(getAuthToken());

            JSONAssert.assertEquals(expectedResponseBody, (String) actualResponseMap.get("response_body"), true);
            assertEquals(HttpURLConnection.HTTP_OK, actualResponseMap.get("response_code"));
        }
    }

    @Nested
    class GivenFailureScenario {

        @Test
        public void withInvalidAuthenticationCredentialShouldReturnUnauthorized() throws Exception {
            URL responseBodyFileURL = getClass().getResource("/response/app/v1/invalid_credential.json");
            String expectedResponseBody = FileUtils.getFileContentAsString(responseBodyFileURL);

            Map<String, Object> actualResponseMap = callAPI(getAuthToken("foo", "bar"));

            JSONAssert.assertEquals(expectedResponseBody, (String) actualResponseMap.get("response_body"), true);
            assertEquals(HttpURLConnection.HTTP_UNAUTHORIZED, actualResponseMap.get("response_code"));
        }

        @Test
        public void withInvalidAuthenticationTokenShouldReturnUnauthorized() throws Exception {
            URL responseBodyFileURL = getClass().getResource("/response/app/v1/invalid_auth_token.json");
            String expectedResponseBody = FileUtils.getFileContentAsString(responseBodyFileURL);

            Map<String, Object> actualResponseMap = callAPI("foo123isinvalid");

            JSONAssert.assertEquals(expectedResponseBody, (String) actualResponseMap.get("response_body"), true);
            assertEquals(HttpURLConnection.HTTP_UNAUTHORIZED, actualResponseMap.get("response_code"));
        }
    }
}
