package id.ac.widyatama.smartfishmonitoringsystem.backend.app.common.json;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import lombok.Value;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import static org.junit.jupiter.api.Assertions.assertEquals;

class LocalDateTimeSerializerTest {
    private static final Gson gson = new GsonBuilder()
            .registerTypeAdapter(LocalDateTime.class, new LocalDateTimeSerializer())
            .create();

    @Test
    void shouldSerializeLocalDateTime() {
        String expectedDate = "{\"localDateTime\":\"2019-11-14T23:59:00\"}";
        LocalDateTime localDateTime = LocalDateTime.parse("2019-11-14T23:59:00", DateTimeFormatter.ISO_LOCAL_DATE_TIME);
        TestLocalDateTime actualLocalDateTime = new TestLocalDateTime(localDateTime);

        assertEquals(expectedDate, gson.toJson(actualLocalDateTime));
    }

    @Test
    void shouldDeserializeLocalDateTime() {
        String expectedDate = "{\"localDateTime\":\"2019-11-14T23:59:00\"}";

        TestLocalDateTime expected = new TestLocalDateTime(LocalDateTime.parse("2019-11-14T23:59:00",
                DateTimeFormatter.ISO_LOCAL_DATE_TIME));

        TestLocalDateTime actual = gson.fromJson(expectedDate, TestLocalDateTime.class);
        assertEquals(expected, actual);
    }

}

@Value
class TestLocalDateTime {
    private LocalDateTime localDateTime;
}