package id.ac.widyatama.smartfishmonitoringsystem.backend.app.domain.psql;

import com.gojek.ApplicationConfiguration;
import id.ac.widyatama.smartfishmonitoringsystem.backend.app.Constant;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class DBConnectionParamTest {

    @Nested
    class TestCreateFromConfig {

        private ApplicationConfiguration configuration;

        @BeforeEach
        public void setUp() {
            configuration = mock(ApplicationConfiguration.class);
            when(configuration.getValueAsString(Constant.Config.PG_DATABASE_HOST)).thenReturn("some-host");
            when(configuration.getValueAsString(Constant.Config.PG_DATABASE_NAME)).thenReturn("some-database-name");
            when(configuration.getValueAsString(Constant.Config.PG_DATABASE_USERNAME)).thenReturn("some-username");
            when(configuration.getValueAsString(Constant.Config.PG_DATABASE_PASSWORD)).thenReturn("some-password");
            when(configuration.getValueAsString(Constant.Config.PG_DATABASE_POOL_NAME)).thenReturn("some-pool-name");
            when(configuration.getValueAsInt(Constant.Config.PG_DATABASE_POOL_SIZE, 1)).thenReturn(5);
            when(configuration.getValueAsInt(Constant.Config.PG_DATABASE_TIMEOUT_IN_MS, 250)).thenReturn(500);
        }

        @Test
        public void shouldCreateDBConnectionParam() {
            DBConnectionParam expected = new DBConnectionParam(
                    "some-host",
                    "some-database-name",
                    "some-username",
                    "some-password",
                    "some-pool-name",
                    5,
                    500
            );

            assertEquals(expected, DBConnectionParam.createFromConfig(configuration));
        }
    }

    @Nested
    class TestGetJdbcURL {


        @Test
        public void shouldConstructJDBCConnectionString() {
            DBConnectionParam connectionParam = new DBConnectionParam(
                    "some-host",
                    "some-database-name",
                    "some-username",
                    "some-password",
                    "some-pool-name",
                    5,
                    500
            );

            assertEquals("jdbc:postgresql://some-host/some-database-name", connectionParam.getJdbcURL());
        }
    }
}