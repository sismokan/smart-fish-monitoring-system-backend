package id.ac.widyatama.smartfishmonitoringsystem.backend.app.common.spark;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.Collections;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.function.Supplier;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class SparkRequestTest {

    private static Request request;

    private static Stream<Arguments> getAssertionMapping() {
        return Stream.of(
                Arguments.of("GetMethod",
                        "GET", (Supplier<String>) () -> request.getMethod()),
                Arguments.of("GetUri",
                        "/foobar", (Supplier<String>) () -> request.getUri()),
                Arguments.of("GetBody",
                        Optional.of("{}"), (Supplier<Optional<String>>) () -> request.getBody()),
                Arguments.of("GetHeaders",
                        Collections.singleton("dolor"), (Supplier<Set<String>>) () -> request.getHeaders()),
                Arguments.of("GetQueryParams",
                        Collections.singleton("lorem"), (Supplier<Set<String>>) () -> request.getQueryParams()),
                Arguments.of("GetHeaderValue",
                        "somet", (Supplier<String>) () -> request.getHeaderValue("dolor")),
                Arguments.of("GetQueryParamValue",
                        "ipsum", (Supplier<String>) () -> request.getQueryParamValue("lorem")),
                Arguments.of("GetRequestHeaders",
                        Collections.singletonMap("dolor", "somet"), (Supplier<Map<String, String>>) () -> request.getRequestHeaders()),
                Arguments.of("GetRequestQueryParams",
                        Collections.singletonMap("lorem", "ipsum"), (Supplier<Map<String, String>>) () -> request.getRequestQueryParams())
        );
    }

    @BeforeEach
    public void setUp() {
        spark.Request originalRequest = mock(spark.Request.class);
        request = SparkRequest.wrap(originalRequest);
        when(originalRequest.body()).thenReturn("{}");
        when(originalRequest.requestMethod()).thenReturn("GET");
        when(originalRequest.uri()).thenReturn("/foobar");
        when(originalRequest.headers()).thenReturn(Collections.singleton("dolor"));
        when(originalRequest.queryParams()).thenReturn(Collections.singleton("lorem"));
        when(originalRequest.headers(eq("dolor"))).thenReturn("somet");
        when(originalRequest.queryParams(eq("lorem"))).thenReturn("ipsum");
    }

    @ParameterizedTest(name = "test{0}ShouldReturn{1}FromOriginalRequest")
    @MethodSource("getAssertionMapping")
    public void testMethods(String methodName, Object expectedResult, Supplier<Object> actualMethodSupplier) {
        assertEquals(expectedResult, actualMethodSupplier.get());
    }
}