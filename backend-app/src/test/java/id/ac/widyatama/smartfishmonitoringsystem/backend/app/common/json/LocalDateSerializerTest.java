package id.ac.widyatama.smartfishmonitoringsystem.backend.app.common.json;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import lombok.Value;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import static org.junit.jupiter.api.Assertions.assertEquals;

class LocalDateSerializerTest {
    private static final Gson gson = new GsonBuilder()
            .registerTypeAdapter(LocalDate.class, new LocalDateSerializer())
            .create();

    @Test
    void shouldSerializeLocalDate() {
        String expectedDate = "{\"localDate\":\"2019-11-14\"}";
        LocalDate localDate = LocalDate.parse("2019-11-14", DateTimeFormatter.ISO_LOCAL_DATE);
        TestLocalDate actualLocalDate = new TestLocalDate(localDate);

        assertEquals(expectedDate, gson.toJson(actualLocalDate));
    }

    @Test
    void shouldDeserializeLocalDate() {
        String expectedDate = "{\"localDate\":\"2019-11-14\"}";

        TestLocalDate expected = new TestLocalDate(LocalDate.parse("2019-11-14",
                DateTimeFormatter.ISO_LOCAL_DATE));

        TestLocalDate actual = gson.fromJson(expectedDate, TestLocalDate.class);
        assertEquals(expected, actual);
    }
}

@Value
class TestLocalDate {
    private LocalDate localDate;
}