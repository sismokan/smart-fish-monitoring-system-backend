package id.ac.widyatama.smartfishmonitoringsystem.backend.app.domain.influxdb;

import id.ac.widyatama.smartfishmonitoringsystem.backend.app.common.LambdaUtils;
import id.ac.widyatama.smartfishmonitoringsystem.backend.api_response.APIResponse;
import id.ac.widyatama.smartfishmonitoringsystem.backend.error.Error;
import id.ac.widyatama.smartfishmonitoringsystem.backend.app.domain.influxdb.query.GetRecordsByPondId;
import id.ac.widyatama.smartfishmonitoringsystem.backend.app.domain.influxdb.query.Query;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

class PondRepositoryTest {

    private InfluxDBWrapper influxDBWrapper;
    private PondRepository<Object> repository;

    @BeforeEach
    public void setUp() {
        influxDBWrapper = mock(InfluxDBWrapper.class);
        repository = new PondRepository<>(influxDBWrapper);
    }

    static class DummyQuery implements Query {

        @Override
        public String toQuery() {
            return "foo";
        }
    }

    @Nested
    class TestSaveRecord {

        @Test
        public void shouldInsertRecordIfInfluxDBDoesNotReturnError() {
            Object dummyObject = new Object();
            when(influxDBWrapper.write(eq(dummyObject))).thenReturn(APIResponse.createSuccessResponse(null));

            APIResponse<Void> actualResult = repository.saveRecord(dummyObject);

            assertEquals(APIResponse.createSuccessResponse(null).map(LambdaUtils::toVoid), actualResult);
            verify(influxDBWrapper, times(1)).write(eq(dummyObject));
        }

        @Test
        public void shouldNotInsertRecordIfInfluxDBReturnError() {
            Object dummyObject = new Object();
            APIResponse genericErrorResponse = APIResponse.createErrorResponse(Error.Code.GENERIC_ERROR, "");
            when(influxDBWrapper.write(eq(dummyObject))).thenReturn(genericErrorResponse);

            APIResponse<Void> actualResult = repository.saveRecord(dummyObject);

            assertEquals(genericErrorResponse, actualResult);
            verify(influxDBWrapper, times(1)).write(eq(dummyObject));
        }
    }

    @Nested
    class TestReadRecords {

        @Test
        public void shouldReturnFechedRecordsIfInfluxDBDoesNotReturnError() {
            DummyQuery query = new DummyQuery();
            Object dummyObject = new Object();
            when(influxDBWrapper.read(eq("foo"))).thenReturn(APIResponse.createSuccessResponse(Collections.emptyList()));

            APIResponse<List<Object>> actualResult = repository.readRecords(query, record -> dummyObject);

            assertEquals(APIResponse.createSuccessResponse(Collections.emptyList()), actualResult);
            verify(influxDBWrapper, times(1)).read(eq("foo"));
        }

        @Test
        public void shouldReturnErrorIfInfluxDBCaughtError() {
            DummyQuery query = new DummyQuery();
            Object dummyObject = new Object();
            APIResponse genericErrorResponse = APIResponse.createErrorResponse(Error.Code.GENERIC_ERROR, "");
            when(influxDBWrapper.read(eq("foo"))).thenReturn(genericErrorResponse);

            APIResponse<List<Object>> actualResult = repository.readRecords(query, record -> dummyObject);

            assertEquals(genericErrorResponse, actualResult);
            verify(influxDBWrapper, times(1)).read(eq("foo"));
        }
    }

    @Nested
    class TestGetRecordsByPondId {

        @Test
        public void shouldReturnFechedRecordsIfInfluxDBDoesNotReturnError() {
            Object dummyObject = new Object();
            GetRecordsByPondId getRecordsByPondId = mock(GetRecordsByPondId.class);
            when(getRecordsByPondId.toQuery()).thenReturn("foo");
            when(influxDBWrapper.read(eq("foo"))).thenReturn(APIResponse.createSuccessResponse(Collections.emptyList()));

            APIResponse<List<Object>> actualResult = repository.getRecordsByPondId(getRecordsByPondId, record -> dummyObject);

            assertEquals(APIResponse.createSuccessResponse(Collections.emptyList()), actualResult);
            verify(influxDBWrapper, times(1)).read(eq("foo"));
        }

        @Test
        public void shouldReturnErrorIfInfluxDBCaughtError() {
            Object dummyObject = new Object();
            GetRecordsByPondId getRecordsByPondId = mock(GetRecordsByPondId.class);
            when(getRecordsByPondId.toQuery()).thenReturn("foo");

            APIResponse genericErrorResponse = APIResponse.createErrorResponse(Error.Code.GENERIC_ERROR, "");
            when(influxDBWrapper.read(eq("foo"))).thenReturn(genericErrorResponse);

            APIResponse<List<Object>> actualResult = repository.getRecordsByPondId(getRecordsByPondId, record -> dummyObject);

            assertEquals(genericErrorResponse, actualResult);
            verify(influxDBWrapper, times(1)).read(eq("foo"));
        }
    }
}