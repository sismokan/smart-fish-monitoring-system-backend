package id.ac.widyatama.smartfishmonitoringsystem.backend.app.common.spark;

import id.ac.widyatama.smartfishmonitoringsystem.backend.api_response.APIResponse;
import id.ac.widyatama.smartfishmonitoringsystem.backend.error.Error;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import spark.Response;

import java.net.HttpURLConnection;

import static org.mockito.Mockito.*;

class ExceptionHandlerTest {

    @Nested
    class TestHandle {

        private JsonResponseTransformer jsonResponseTransformer;
        private ExceptionHandler<Exception> exceptionHandler;

        @BeforeEach
        public void setUp() {
            jsonResponseTransformer = mock(JsonResponseTransformer.class);
            exceptionHandler = new ExceptionHandler<>(jsonResponseTransformer);
        }

        @Test
        public void shouldHandleExceptionAsResponse() {
            String stubSerializedBody = "{}";
            APIResponse genericError = APIResponse.createErrorResponse(Error.Code.GENERIC_ERROR, "", "");
            when(jsonResponseTransformer.render(genericError)).thenReturn(stubSerializedBody);
            spark.Request request = mock(spark.Request.class);
            Response response = mock(Response.class);

            exceptionHandler.handle(new RuntimeException("foobar"), request, response);

            verify(response, times(1)).status(HttpURLConnection.HTTP_INTERNAL_ERROR);
            verify(response, times(1)).body(stubSerializedBody);
        }
    }
}