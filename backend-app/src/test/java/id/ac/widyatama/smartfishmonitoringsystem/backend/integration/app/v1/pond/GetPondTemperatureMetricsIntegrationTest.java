package id.ac.widyatama.smartfishmonitoringsystem.backend.integration.app.v1.pond;

import com.gojek.Figaro;
import com.influxdb.client.domain.WritePrecision;
import com.influxdb.client.write.Point;
import id.ac.widyatama.smartfishmonitoringsystem.backend.app.App;
import id.ac.widyatama.smartfishmonitoringsystem.backend.app.Constant;
import id.ac.widyatama.smartfishmonitoringsystem.backend.app.common.DateTimeUtils;
import id.ac.widyatama.smartfishmonitoringsystem.backend.app.domain.psql.account.Account;
import id.ac.widyatama.smartfishmonitoringsystem.backend.helper.FileUtils;
import id.ac.widyatama.smartfishmonitoringsystem.backend.helper.InfluxDBUtils;
import id.ac.widyatama.smartfishmonitoringsystem.backend.helper.PSQLUtils;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpHost;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.junit.jupiter.api.*;
import org.skyscreamer.jsonassert.JSONAssert;
import spark.Spark;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.Charset;
import java.time.OffsetDateTime;
import java.util.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static spark.Spark.awaitInitialization;
import static spark.Spark.awaitStop;

public class GetPondTemperatureMetricsIntegrationTest {

    private static CloseableHttpClient httpClient;
    private static PSQLUtils.DBHelper dbHelper;
    private OffsetDateTime now;

    @BeforeAll
    public static void setUpAll() {
        dbHelper = new PSQLUtils.DBHelper(Figaro.configure(Collections.emptySet()));
        httpClient = HttpClientBuilder.create().build();
        App.main(new String[]{});
        awaitInitialization();
    }

    @AfterAll
    public static void tearDownAll() {
        InfluxDBUtils.deleteAllRecords();
        dbHelper.truncateAll();
        Spark.stop();
        awaitStop();
    }

    @BeforeEach
    public void setUpEach() {
        now = DateTimeUtils.parse("2022-05-28T10:25:26.439486Z");
        InfluxDBUtils.deleteAllRecords();
        dbHelper.truncateAll();
        dbHelper.createAccount(Account.builder()
                .setGuid(UUID.randomUUID().toString())
                .setUsername("qornanali")
                .setPassword("ali123")
                .build());
        Point point1 = Point.measurement(Constant.Measurement.POND_SENSOR_METRIC)
                .addTag("pondId", "3264b0ab-710a-4ad4-871c-fa742d5cbb96")
                .addField(Constant.Measurement.FIELD_ACIDITIY_PH, 10.5)
                .addField(Constant.Measurement.FIELD_TEMPERATURE_CELSIUS, 22.5)
                .time(now.minusMinutes(30).toInstant().toEpochMilli(), WritePrecision.MS);
        InfluxDBUtils.saveRecord(point1);
        Point point2 = Point.measurement(Constant.Measurement.POND_SENSOR_METRIC)
                .addTag("pondId", "3264b0ab-710a-4ad4-871c-fa742d5cbb96")
                .addField(Constant.Measurement.FIELD_ACIDITIY_PH, 11.0)
                .addField(Constant.Measurement.FIELD_TEMPERATURE_CELSIUS, 38.1)
                .time(now.minusMinutes(20).toInstant().toEpochMilli(), WritePrecision.MS);
        InfluxDBUtils.saveRecord(point2);
        Point point3 = Point.measurement(Constant.Measurement.POND_SENSOR_METRIC)
                .addTag("pondId", "3264b0ab-710a-4ad4-871c-fa742d5cbb96")
                .addField(Constant.Measurement.FIELD_ACIDITIY_PH, 12.3)
                .addField(Constant.Measurement.FIELD_TEMPERATURE_CELSIUS, 20.4)
                .time(now.minusMinutes(10).toInstant().toEpochMilli(), WritePrecision.MS);
        InfluxDBUtils.saveRecord(point3);
    }

    private String getAuthToken() {
        return getAuthToken("qornanali", "ali123");
    }

    private String getAuthToken(String username, String password) {
        return new String(Base64.getEncoder().encode(String.format("%s:%s", username, password).getBytes()));
    }

    private Map<String, Object> callAPI(String authToken) throws IOException {
        HttpGet request = new HttpGet(String.format("/app/v1/ponds/%s/metric/temperature?start_time=%s&end_time=%s",
                "3264b0ab-710a-4ad4-871c-fa742d5cbb96",
                now.minusHours(1).toEpochSecond(),
                now.toEpochSecond()));
        request.addHeader("Authorization", String.format("Basic %s", authToken));
        request.addHeader("Content-Type", "application/json");
        request.addHeader("debug-id", "some-correlation-id");
        CloseableHttpResponse response = httpClient.execute(HttpHost.create("http://127.0.0.1:4000"), request);
        Map<String, Object> responseMap = new HashMap<>();
        responseMap.put("response_code", response.getStatusLine().getStatusCode());
        responseMap.put("response_body", IOUtils.toString(response.getEntity().getContent(), Charset.defaultCharset()));
        return responseMap;
    }

    @Nested
    class GivenSuccessScenario {

        @Test
        public void shouldReturnOk() throws Exception {
            URL responseBodyFileURL = getClass().getResource("/response/app/v1/ponds/get_metrics/temperature/success.json");
            String expectedResponseBody = FileUtils.getFileContentAsString(responseBodyFileURL);

            Map<String, Object> actualResponseMap = callAPI(getAuthToken());

            JSONAssert.assertEquals(expectedResponseBody, (String) actualResponseMap.get("response_body"), true);
            assertEquals(HttpURLConnection.HTTP_OK, actualResponseMap.get("response_code"));
        }
    }

    @Nested
    class GivenFailureScenario {

        @Test
        public void withInvalidAuthenticationCredentialShouldReturnUnauthorized() throws Exception {
            URL responseBodyFileURL = getClass().getResource("/response/app/v1/invalid_credential.json");
            String expectedResponseBody = FileUtils.getFileContentAsString(responseBodyFileURL);

            Map<String, Object> actualResponseMap = callAPI(getAuthToken("foo", "bar"));

            JSONAssert.assertEquals(expectedResponseBody, (String) actualResponseMap.get("response_body"), true);
            assertEquals(HttpURLConnection.HTTP_UNAUTHORIZED, actualResponseMap.get("response_code"));
        }

        @Test
        public void withInvalidAuthenticationTokenShouldReturnUnauthorized() throws Exception {
            URL responseBodyFileURL = getClass().getResource("/response/app/v1/invalid_auth_token.json");
            String expectedResponseBody = FileUtils.getFileContentAsString(responseBodyFileURL);

            Map<String, Object> actualResponseMap = callAPI("foo123isinvalid");

            JSONAssert.assertEquals(expectedResponseBody, (String) actualResponseMap.get("response_body"), true);
            assertEquals(HttpURLConnection.HTTP_UNAUTHORIZED, actualResponseMap.get("response_code"));
        }
    }
}
