package id.ac.widyatama.smartfishmonitoringsystem.backend.app.domain.metric;

import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class PublishMetricContextTest {

    @Nested
    class TestGetFlowContext {

        @Test
        public void shouldReturnContext() {
            PublishMetricContext context = new PublishMetricContext();

            assertEquals("PUBLISH_METRIC", context.getFlowContext());
        }
    }
}
