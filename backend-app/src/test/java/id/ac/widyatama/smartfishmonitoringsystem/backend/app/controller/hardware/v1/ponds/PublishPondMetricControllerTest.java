package id.ac.widyatama.smartfishmonitoringsystem.backend.app.controller.hardware.v1.ponds;

import com.google.gson.Gson;
import id.ac.widyatama.smartfishmonitoringsystem.backend.api_response.APIResponse;
import id.ac.widyatama.smartfishmonitoringsystem.backend.app.common.LambdaUtils;
import id.ac.widyatama.smartfishmonitoringsystem.backend.app.common.json.GsonBuilder;
import id.ac.widyatama.smartfishmonitoringsystem.backend.app.common.spark.status_code.HttpErrorTranslator;
import id.ac.widyatama.smartfishmonitoringsystem.backend.app.domain.contract.PublishPondMetricRequest;
import id.ac.widyatama.smartfishmonitoringsystem.backend.app.domain.contract.PublishPondMetricResponse;
import id.ac.widyatama.smartfishmonitoringsystem.backend.app.domain.dto.Acidity;
import id.ac.widyatama.smartfishmonitoringsystem.backend.app.domain.dto.Temperature;
import id.ac.widyatama.smartfishmonitoringsystem.backend.app.domain.metric.PublishPondMetricService;
import id.ac.widyatama.smartfishmonitoringsystem.backend.error.Error;
import id.ac.widyatama.smartfishmonitoringsystem.backend.error.ErrorUtils;
import org.jetbrains.annotations.NotNull;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import spark.Request;
import spark.Response;

import java.net.HttpURLConnection;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.Collections;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;

class PublishPondMetricControllerTest {

    @Nested
    class TestHandle {

        private final Gson gson = GsonBuilder.getDefaultGsonBuilder().create();
        private PublishPondMetricService service;
        private PublishPondMetricController controller;
        private HttpErrorTranslator httpErrorTranslator;
        private Request request;
        private Response response;

        @BeforeEach
        public void setUp() {
            service = mock(PublishPondMetricService.class);
            request = mock(Request.class);
            response = mock(Response.class);
            httpErrorTranslator = mock(HttpErrorTranslator.class);
            controller = new PublishPondMetricController(service, httpErrorTranslator, gson);
        }

        @Test
        public void givenRequestBodyIsInvalidJsonShouldReturnError() {
            Error malformedJsonError = ErrorUtils.createError(Error.Code.MALFORMED_JSON, "request.body",
                    "java.io.EOFException: End of input at line 1 column 2 path $.");
            when(httpErrorTranslator.getStatusCode(eq(malformedJsonError))).thenReturn(HttpURLConnection.HTTP_BAD_REQUEST);

            when(request.body()).thenReturn("{");

            APIResponse<PublishPondMetricResponse> expected = APIResponse.createErrorResponse(Collections.singletonList(malformedJsonError));

            APIResponse<PublishPondMetricResponse> actual = controller.handle(request, response);

            assertEquals(expected, actual);
            verify(response, times(1)).status(HttpURLConnection.HTTP_BAD_REQUEST);
        }

        @Test
        public void givenTakenAtIsEmptyShouldEnrichItAndSucceed() {
            when(request.body()).thenReturn(getDummyRequestBodyWithoutTakenAt());
            String pondId = "foobar";
            when(request.params("pond_id")).thenReturn(pondId);
            when(service.publish(any()))
                    .thenReturn(APIResponse.createSuccessResponse(null).map(LambdaUtils::toVoid));

            APIResponse<PublishPondMetricResponse> actual = controller.handle(request, response);

            assertTrue(actual.isSuccess());
            verify(response, times(1)).status(HttpURLConnection.HTTP_OK);
        }

        @Test
        public void givenServiceSucceedShouldReturnSucceed() {
            when(request.body()).thenReturn(getDummyRequestBody());
            String pondId = "foobar";
            when(request.params("pond_id")).thenReturn(pondId);
            OffsetDateTime publishedAt = OffsetDateTime.of(2022, 5, 28, 10, 25, 26, 439486000, ZoneOffset.UTC);
            PublishPondMetricRequest publishPondMetricRequest = getDummyDecodedRequest(pondId, publishedAt);
            when(service.publish(eq(publishPondMetricRequest)))
                    .thenReturn(APIResponse.createSuccessResponse(null).map(LambdaUtils::toVoid));

            APIResponse<PublishPondMetricResponse> expected = APIResponse.createSuccessResponse(
                    PublishPondMetricResponse.builder()
                            .setPublishedAt(publishedAt)
                            .setPondId(pondId)
                            .build()
            );

            APIResponse<PublishPondMetricResponse> actual = controller.handle(request, response);

            assertEquals(expected, actual);
            verify(response, times(1)).status(HttpURLConnection.HTTP_OK);
        }

        @Test
        public void givenServiceFailedShouldReturnError() {
            Error genericError = ErrorUtils.createError(Error.Code.GENERIC_ERROR, "", "");
            when(httpErrorTranslator.getStatusCode(eq(genericError))).thenReturn(HttpURLConnection.HTTP_INTERNAL_ERROR);
            APIResponse expectedResponse = APIResponse.createErrorResponse(Collections.singletonList(genericError));

            when(request.body()).thenReturn(getDummyRequestBody());

            String pondId = "foobar";
            when(request.params("pond_id")).thenReturn(pondId);

            OffsetDateTime publishedAt = OffsetDateTime.of(2022, 5, 28, 10, 25, 26, 439486000, ZoneOffset.UTC);
            PublishPondMetricRequest publishPondMetricRequest = getDummyDecodedRequest(pondId, publishedAt);
            when(service.publish(eq(publishPondMetricRequest))).thenReturn(expectedResponse);

            APIResponse<PublishPondMetricResponse> actual = controller.handle(request, response);

            assertEquals(expectedResponse, actual);
            verify(response, times(1)).status(HttpURLConnection.HTTP_INTERNAL_ERROR);
        }

        private PublishPondMetricRequest getDummyDecodedRequest(String pondId, OffsetDateTime publishedAt) {
            return PublishPondMetricRequest.builder()
                    .setTakenAt(publishedAt)
                    .setPondId(pondId)
                    .setAcidity(Acidity.builder()
                            .setMeasurement("POWER_OF_HIDROGEN")
                            .setValue(10.0)
                            .build())
                    .setTemperature(Temperature.builder()
                            .setMeasurement("CELCIUS")
                            .setValue(22.5)
                            .build())
                    .build();
        }

        @NotNull
        private String getDummyRequestBody() {
            return "{\n" +
                    "  \"taken_at\": \"2022-05-28T10:25:26.439486Z\",\n" +
                    "  \"temperature\": {\n" +
                    "    \"value\": 22.5,\n" +
                    "    \"measurement\": \"CELCIUS\"\n" +
                    "  },\n" +
                    "  \"acidity\": {\n" +
                    "    \"value\": 10.0,\n" +
                    "    \"measurement\": \"POWER_OF_HIDROGEN\"\n" +
                    "  }\n" +
                    "}";
        }

        @NotNull
        private String getDummyRequestBodyWithoutTakenAt() {
            return "{\n" +
                    "  \"temperature\": {\n" +
                    "    \"value\": 22.5,\n" +
                    "    \"measurement\": \"CELCIUS\"\n" +
                    "  },\n" +
                    "  \"acidity\": {\n" +
                    "    \"value\": 10.0,\n" +
                    "    \"measurement\": \"POWER_OF_HIDROGEN\"\n" +
                    "  }\n" +
                    "}";
        }
    }
}