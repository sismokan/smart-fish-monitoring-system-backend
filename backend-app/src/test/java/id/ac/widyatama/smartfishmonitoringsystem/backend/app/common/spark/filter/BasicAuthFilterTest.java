package id.ac.widyatama.smartfishmonitoringsystem.backend.app.common.spark.filter;

import id.ac.widyatama.smartfishmonitoringsystem.backend.app.Constant;
import id.ac.widyatama.smartfishmonitoringsystem.backend.app.common.spark.JsonResponseTransformer;
import id.ac.widyatama.smartfishmonitoringsystem.backend.app.common.spark.status_code.HttpErrorTranslator;
import id.ac.widyatama.smartfishmonitoringsystem.backend.api_response.APIResponse;
import id.ac.widyatama.smartfishmonitoringsystem.backend.app.domain.authorization.AuthenticatedAccount;
import id.ac.widyatama.smartfishmonitoringsystem.backend.app.domain.authorization.AuthenticationService;
import id.ac.widyatama.smartfishmonitoringsystem.backend.error.Error;
import id.ac.widyatama.smartfishmonitoringsystem.backend.error.ErrorUtils;
import org.apache.http.entity.ContentType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import spark.HaltException;
import spark.Request;
import spark.Response;

import java.net.HttpURLConnection;
import java.util.Collections;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

class BasicAuthFilterTest {

    @Nested
    class TestHandle {

        private final String AUTHORIZATION_HEADER_VALUE = "Basic foobar";
        private AuthenticationService authenticationService;
        private JsonResponseTransformer jsonResponseTransformer;
        private HttpErrorTranslator httpErrorTranslator;
        private Request mockRequest;
        private Response mockResponse;
        private BasicAuthFilter basicAuthFilter;

        @BeforeEach
        public void setUp() {
            authenticationService = mock(AuthenticationService.class);
            jsonResponseTransformer = mock(JsonResponseTransformer.class);
            httpErrorTranslator = mock(HttpErrorTranslator.class);
            mockRequest = mock(Request.class);
            mockResponse = mock(Response.class);
            basicAuthFilter = new BasicAuthFilter(authenticationService, jsonResponseTransformer, httpErrorTranslator);
            when(mockRequest.headers("Authorization")).thenReturn(AUTHORIZATION_HEADER_VALUE);
        }

        @Test
        public void givenAuthServiceReturnGenericErrorShouldHaltAndResponseWithInternalServerError() {
            Error genericError = ErrorUtils.createError(Error.Code.GENERIC_ERROR, "some-entity");
            when(httpErrorTranslator.getStatusCode(eq(genericError))).thenReturn(HttpURLConnection.HTTP_INTERNAL_ERROR);
            APIResponse genericErrorResponse = APIResponse.createErrorResponse(Collections.singletonList(genericError));
            when(authenticationService.authorizeUsingBasic(eq(AUTHORIZATION_HEADER_VALUE)))
                    .thenReturn(genericErrorResponse);
            when(jsonResponseTransformer.render(eq(genericErrorResponse))).thenReturn("{}");

            assertThrows(HaltException.class, () -> basicAuthFilter.handle(mockRequest, mockResponse));

            verify(mockResponse, times(1)).header("Content-Type", ContentType.APPLICATION_JSON.toString());
            verify(httpErrorTranslator, times(1)).getStatusCode(genericError);
            verify(jsonResponseTransformer, times(1)).render(genericErrorResponse);
        }

        @Test
        public void givenAuthServiceReturnNonGenericErrorShouldHaltAndResponseWithUnauthorized() {
            Error notFoundError = ErrorUtils.createError(Error.Code.RESOURCE_CANNOT_BE_FOUND, "some-entity");
            Error invalidAuthError = ErrorUtils.createError(Error.Code.INVALID_AUTHORIZATION, "token");
            when(httpErrorTranslator.getStatusCode(eq(invalidAuthError))).thenReturn(HttpURLConnection.HTTP_UNAUTHORIZED);
            APIResponse notFoundErrorResponse = APIResponse.createErrorResponse(Collections.singletonList(notFoundError));
            APIResponse changedResponse = APIResponse.createErrorResponse(Collections.singletonList(invalidAuthError));
            when(authenticationService.authorizeUsingBasic(eq(AUTHORIZATION_HEADER_VALUE)))
                    .thenReturn(notFoundErrorResponse);
            when(jsonResponseTransformer.render(eq(notFoundErrorResponse))).thenReturn("{}");

            assertThrows(HaltException.class, () -> basicAuthFilter.handle(mockRequest, mockResponse));

            verify(mockResponse, times(1)).header("Content-Type", ContentType.APPLICATION_JSON.toString());
            verify(httpErrorTranslator, times(1)).getStatusCode(invalidAuthError);
            verify(jsonResponseTransformer, times(1)).render(changedResponse);
        }

        @Test
        public void givenAuthServiceReturnAccountDataShouldSaveItToAttribute() {
            AuthenticatedAccount stubAuthenticatedAccount = AuthenticatedAccount.builder().build();
            when(authenticationService.authorizeUsingBasic(eq(AUTHORIZATION_HEADER_VALUE)))
                    .thenReturn(APIResponse.createSuccessResponse(stubAuthenticatedAccount));

            assertDoesNotThrow(() -> basicAuthFilter.handle(mockRequest, mockResponse));

            verify(mockRequest, times(1)).attribute(Constant.Attribute.AUTHENTICATED_ACCOUNT, stubAuthenticatedAccount);
        }
    }
}