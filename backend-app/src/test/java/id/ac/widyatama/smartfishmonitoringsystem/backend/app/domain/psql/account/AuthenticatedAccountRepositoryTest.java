package id.ac.widyatama.smartfishmonitoringsystem.backend.app.domain.psql.account;

import com.gojek.ApplicationConfiguration;
import com.gojek.Figaro;
import id.ac.widyatama.smartfishmonitoringsystem.backend.api_response.APIResponse;
import id.ac.widyatama.smartfishmonitoringsystem.backend.error.Error;
import id.ac.widyatama.smartfishmonitoringsystem.backend.app.domain.psql.dto.GetAccountByUserCredential;
import id.ac.widyatama.smartfishmonitoringsystem.backend.helper.InfluxDBUtils;
import id.ac.widyatama.smartfishmonitoringsystem.backend.helper.PSQLUtils;
import org.junit.jupiter.api.*;

import java.util.Collections;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;

class AuthenticatedAccountRepositoryTest {

    private static PSQLUtils.DBHelper dbHelper;
    private static AccountRepository accountRepository;

    @BeforeAll
    public static void setUpAll() {
        ApplicationConfiguration configuration = Figaro.configure(Collections.emptySet());
        dbHelper = new PSQLUtils.DBHelper(configuration);
        accountRepository = new AccountRepository(configuration);
    }

    @AfterAll
    public static void tearDownAll() {
        dbHelper.truncateAll();
    }

    @BeforeEach
    public void setUpEach() {
        InfluxDBUtils.deleteAllRecords();
        dbHelper.truncateAll();
    }

    @Nested
    class TestGetAuthenticatedAccountBy {

        private final String uuid = UUID.randomUUID().toString();

        @BeforeEach
        public void setUp() {
            dbHelper.createAccount(Account.builder()
                    .setGuid(uuid)
                    .setUsername("qornanali")
                    .setPassword("ali123")
                    .build());
        }

        @Nested
        class GivenGetAuthenticatedAccountByUserCredential {

            @Test
            public void whenRecordExistShouldReturnResult() {
                assertEquals(
                        APIResponse.createSuccessResponse(Account.builder()
                                .setUsername("qornanali")
                                .setGuid(uuid)
                                .build()),
                        accountRepository.getAccountBy(
                                GetAccountByUserCredential.builder()
                                        .setUsername("qornanali")
                                        .setPassword("ali123")
                                        .build()
                        )
                );
            }

            @Test
            public void whenRecordNotExistShouldReturnError() {
                assertEquals(
                        APIResponse.createErrorResponse(Error.Code.RESOURCE_CANNOT_BE_FOUND, "account"),
                        accountRepository.getAccountBy(
                                GetAccountByUserCredential.builder()
                                        .setUsername("foobar")
                                        .setPassword("testtest")
                                        .build()
                        )
                );
            }
        }
    }
}