package id.ac.widyatama.smartfishmonitoringsystem.backend.helper;

import com.gojek.ApplicationConfiguration;
import com.gojek.Figaro;
import com.influxdb.client.DeleteApi;
import com.influxdb.client.InfluxDBClient;
import com.influxdb.client.QueryApi;
import com.influxdb.client.WriteApiBlocking;
import com.influxdb.client.domain.DeletePredicateRequest;
import com.influxdb.client.write.Point;
import com.influxdb.query.FluxTable;
import id.ac.widyatama.smartfishmonitoringsystem.backend.app.Constant;
import id.ac.widyatama.smartfishmonitoringsystem.backend.app.common.DateTimeUtils;
import id.ac.widyatama.smartfishmonitoringsystem.backend.app.domain.influxdb.ClientFactory;

import java.time.OffsetDateTime;
import java.util.Collections;
import java.util.List;

public class InfluxDBUtils {

    public static List<FluxTable> getRecords(String fluxQuery) {
        ApplicationConfiguration configuration = Figaro.configure(Collections.emptySet());
        InfluxDBClient client = ClientFactory.create(configuration);
        if (!client.ping()) {
            throw new RuntimeException("InfluxDBClient Failed to be initialized");
        }

        QueryApi queryApi = client.getQueryApi();
        List<FluxTable> tables = queryApi.query(fluxQuery);
        client.close();
        return tables;
    }

    public static void saveRecord(Point point) {
        ApplicationConfiguration configuration = Figaro.configure(Collections.emptySet());
        InfluxDBClient client = ClientFactory.create(configuration);
        if (!client.ping()) {
            throw new RuntimeException("InfluxDBClient Failed to be initialized");
        }

        WriteApiBlocking writeApiBlocking = client.getWriteApiBlocking();
        writeApiBlocking.writePoint(point);

        client.close();
    }

    public static void deleteAllRecords() {
        ApplicationConfiguration configuration = Figaro.configure(Collections.emptySet());
        InfluxDBClient client = ClientFactory.create(configuration);
        if (!client.ping()) {
            throw new RuntimeException("InfluxDBClient Failed to be initialized");
        }

        String organization = configuration.getValueAsString(Constant.Config.INFLUXDB_ORGANIZATION);
        String bucket = configuration.getValueAsString(Constant.Config.INFLUXDB_BUCKET);

        DeleteApi deleteApi = client.getDeleteApi();
        OffsetDateTime now = DateTimeUtils.getNow();
        DeletePredicateRequest deletePredicateRequest = new DeletePredicateRequest()
                .predicate("")
                .start(DateTimeUtils.parse("2000-06-09T21:23:17.603137Z"))
                .stop(now);
        deleteApi.delete(deletePredicateRequest, bucket, organization);
        client.close();
    }
}
