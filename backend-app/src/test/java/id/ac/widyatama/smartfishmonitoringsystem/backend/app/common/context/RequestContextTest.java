package id.ac.widyatama.smartfishmonitoringsystem.backend.app.common.context;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.slf4j.MDC;

import static org.junit.jupiter.api.Assertions.assertEquals;

class RequestContextTest {

    @AfterAll
    public static void tearDownAll() {
        MDC.clear();
    }

    @BeforeEach
    public void setUp() {
        MDC.clear();
    }

    @Test
    void shouldPutCorrelationId() {
        RequestContext.putCorrelationId("correlation-id");
        String expected = "correlation-id";
        String actual = RequestContext.getCorrelationId();
        assertEquals(expected, actual);
    }

    @Test
    void shouldPutFlowContext() {
        RequestContext.putContext(new DummyContext());
        String expected = "foobar";
        assertEquals(expected, MDC.get("context"));
    }

    @Test
    void shouldPutRequestUri() {
        RequestContext.putRequestUri("/foobar");
        String expected = "/foobar";
        assertEquals(expected, MDC.get("request_uri"));
    }

    @Test
    void shouldPutRequestMethod() {
        RequestContext.putRequestMethod("GET");
        String expected = "GET";
        assertEquals(expected, MDC.get("request_method"));
    }

    private static class DummyContext implements FlowContext {

        @Override
        public String getFlowContext() {
            return "foobar";
        }
    }
}