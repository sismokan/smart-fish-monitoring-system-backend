package id.ac.widyatama.smartfishmonitoringsystem.backend.helper;

import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.Objects;

public class FileUtils {
    public static String getFileContentAsString(URL url) {
        try {
            return IOUtils.toString(Objects.requireNonNull(url), StandardCharsets.UTF_8);
        } catch (IOException e) {
            e.printStackTrace();
            return "";
        }
    }
}
