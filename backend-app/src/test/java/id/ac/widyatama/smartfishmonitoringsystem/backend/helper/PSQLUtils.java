package id.ac.widyatama.smartfishmonitoringsystem.backend.helper;

import com.gojek.ApplicationConfiguration;
import id.ac.widyatama.smartfishmonitoringsystem.backend.app.domain.psql.BaseRepository;
import id.ac.widyatama.smartfishmonitoringsystem.backend.app.domain.psql.account.Account;
import id.ac.widyatama.smartfishmonitoringsystem.backend.app.domain.psql.account.AccountDAO;
import id.ac.widyatama.smartfishmonitoringsystem.backend.app.domain.psql.pond.Pond;
import id.ac.widyatama.smartfishmonitoringsystem.backend.app.domain.psql.pond.PondDAO;
import org.jdbi.v3.core.Handle;

public class PSQLUtils {

    public static class DBHelper extends BaseRepository<Object> {

        public DBHelper(ApplicationConfiguration configuration) {
            super(configuration);
        }

        public void truncateAll() {
            Handle handle = getJdbi().open();
            handle.execute("TRUNCATE ponds RESTART IDENTITY CASCADE");
            handle.execute("TRUNCATE accounts RESTART IDENTITY CASCADE");
            handle.close();
        }

        public void createAccount(Account account) {
            getJdbi().withExtension(AccountDAO.class, dao -> dao.createAccount(account));
        }

        public void createPond(Pond pond) {
            getJdbi().withExtension(PondDAO.class, dao -> dao.createPond(pond));
        }
    }

}
