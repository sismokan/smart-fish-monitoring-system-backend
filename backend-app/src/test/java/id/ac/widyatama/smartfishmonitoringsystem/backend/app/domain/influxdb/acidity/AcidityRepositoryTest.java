package id.ac.widyatama.smartfishmonitoringsystem.backend.app.domain.influxdb.acidity;

import id.ac.widyatama.smartfishmonitoringsystem.backend.api_response.APIResponse;
import id.ac.widyatama.smartfishmonitoringsystem.backend.error.Error;
import id.ac.widyatama.smartfishmonitoringsystem.backend.app.domain.influxdb.InfluxDBWrapper;
import id.ac.widyatama.smartfishmonitoringsystem.backend.app.domain.influxdb.query.GetRecordsByPondId;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

class AcidityRepositoryTest {

    private InfluxDBWrapper influxDBWrapper;
    private AcidityRepository repository;

    @BeforeEach
    public void setUp() {
        influxDBWrapper = mock(InfluxDBWrapper.class);
        repository = new AcidityRepository(influxDBWrapper);
    }

    @Nested
    class TestGetRecordsByPondId {

        @Test
        public void shouldReturnFechedRecordsIfInfluxDBDoesNotReturnError() {
            GetRecordsByPondId getRecordsByPondId = mock(GetRecordsByPondId.class);
            when(getRecordsByPondId.toQuery()).thenReturn("foo");
            when(influxDBWrapper.read(eq("foo"))).thenReturn(APIResponse.createSuccessResponse(Collections.emptyList()));

            APIResponse<List<AcidityMetric>> actualResult = repository.getRecordsByPondId(getRecordsByPondId);

            assertEquals(APIResponse.createSuccessResponse(Collections.emptyList()), actualResult);
            verify(influxDBWrapper, times(1)).read(eq("foo"));
        }

        @Test
        public void shouldReturnErrorIfInfluxDBCaughtError() {
            GetRecordsByPondId getRecordsByPondId = mock(GetRecordsByPondId.class);
            when(getRecordsByPondId.toQuery()).thenReturn("foo");

            APIResponse genericErrorResponse = APIResponse.createErrorResponse(Error.Code.GENERIC_ERROR, "");
            when(influxDBWrapper.read(eq("foo"))).thenReturn(genericErrorResponse);

            APIResponse<List<AcidityMetric>> actualResult = repository.getRecordsByPondId(getRecordsByPondId);

            assertEquals(genericErrorResponse, actualResult);
            verify(influxDBWrapper, times(1)).read(eq("foo"));
        }
    }
}