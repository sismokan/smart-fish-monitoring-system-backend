package id.ac.widyatama.smartfishmonitoringsystem.backend.app.domain.psql.account;

import org.jdbi.v3.core.statement.StatementContext;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.sql.ResultSet;
import java.sql.SQLException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class AuthenticatedAccountMapperTest {

    @Nested
    class TestMap {

        @Test
        public void shouldSetFieldsCorrectly() throws SQLException {
            StatementContext ctx = mock(StatementContext.class);
            ResultSet rs = mock(ResultSet.class);
            when(rs.getString("guid")).thenReturn("foobar");
            when(rs.getString("username")).thenReturn("lorem ipsum");

            AccountMapper mapper = new AccountMapper();

            assertEquals(
                    Account.builder()
                            .setGuid("foobar")
                            .setUsername("lorem ipsum")
                            .build(),
                    mapper.map(rs, ctx));
        }
    }
}