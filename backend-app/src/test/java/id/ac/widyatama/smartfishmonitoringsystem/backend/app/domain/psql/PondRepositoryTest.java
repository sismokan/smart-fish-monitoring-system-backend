package id.ac.widyatama.smartfishmonitoringsystem.backend.app.domain.psql;

import com.gojek.Figaro;
import id.ac.widyatama.smartfishmonitoringsystem.backend.api_response.APIResponse;
import id.ac.widyatama.smartfishmonitoringsystem.backend.error.Error;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.Collections;

import static org.junit.jupiter.api.Assertions.assertEquals;

class PondRepositoryTest {

    @Nested
    class TestExecute {

        private BaseRepository baseRepository;

        @BeforeEach
        public void setUp() {
            baseRepository = new BaseRepository(Figaro.configure(Collections.emptySet()));
        }

        @Test
        public void givenExceptionShouldReturnError() {
            APIResponse errorResponse = APIResponse.createErrorResponse(Error.Code.GENERIC_ERROR, "", "");

            APIResponse<Object> actualResponse = baseRepository.execute(null, (dao) -> new Object());

            assertEquals(errorResponse, actualResponse);
        }
    }
}