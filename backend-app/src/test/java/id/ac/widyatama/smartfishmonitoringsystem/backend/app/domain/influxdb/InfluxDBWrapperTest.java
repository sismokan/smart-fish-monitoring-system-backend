package id.ac.widyatama.smartfishmonitoringsystem.backend.app.domain.influxdb;

import com.influxdb.client.InfluxDBClient;
import com.influxdb.client.QueryApi;
import com.influxdb.client.WriteApiBlocking;
import com.influxdb.client.domain.WritePrecision;
import com.influxdb.query.FluxTable;
import id.ac.widyatama.smartfishmonitoringsystem.backend.api_response.APIResponse;
import id.ac.widyatama.smartfishmonitoringsystem.backend.error.Error;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

class InfluxDBWrapperTest {

    private InfluxDBWrapper influxDBWrapper;
    private WriteApiBlocking writeApi;
    private QueryApi queryApi;

    @BeforeEach
    public void setUp() {
        writeApi = mock(WriteApiBlocking.class);
        queryApi = mock(QueryApi.class);
        InfluxDBClient client = mock(InfluxDBClient.class);
        when(client.getWriteApiBlocking()).thenReturn(writeApi);
        when(client.getQueryApi()).thenReturn(queryApi);
        influxDBWrapper = InfluxDBWrapper.create(client);
    }

    @Nested
    class TestWrite {

        @Test
        public void givenNoExceptionShouldReturnSucceed() {
            Object dummy = new Object();
            doNothing().when(writeApi).writeMeasurement(eq(WritePrecision.NS), eq(dummy));
            APIResponse<Object> actual = influxDBWrapper.write(dummy);
            assertEquals(APIResponse.createSuccessResponse(dummy), actual);
        }

        @Test
        public void givenExceptionShouldReturnFailed() {
            Object dummy = new Object();
            doThrow(new RuntimeException("some exception")).when(writeApi).writeMeasurement(eq(WritePrecision.NS), eq(dummy));
            APIResponse<Object> actual = influxDBWrapper.write(dummy);
            assertEquals(APIResponse.createErrorResponse(Error.Code.GENERIC_ERROR, "", "some exception"), actual);
        }
    }

    @Nested
    class TestRead {

        @Test
        public void givenNoExceptionShouldReturnSucceed() {
            when(queryApi.query(eq("foobar"))).thenReturn(null);
            APIResponse<List<FluxTable>> actual = influxDBWrapper.read("foobar");
            assertEquals(APIResponse.createSuccessResponse(null), actual);
        }

        @Test
        public void givenExceptionShouldReturnFailed() {
            when(queryApi.query(eq("foobar"))).thenThrow(new RuntimeException("some exception"));
            APIResponse<List<FluxTable>> actual = influxDBWrapper.read("foobar");
            assertEquals(APIResponse.createErrorResponse(Error.Code.GENERIC_ERROR, "", "some exception"), actual);
        }
    }
}