package id.ac.widyatama.smartfishmonitoringsystem.backend.app.domain.health_check;

import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class HealthCheckResponseTest {

    @Nested
    class TestOk {

        @Test
        public void shouldReturnOk() {
            assertEquals(new HealthCheckResponse("pong"), HealthCheckResponse.ok());
        }
    }

    @Nested
    class TestUnavailable {

        @Test
        public void shouldReturnOk() {
            assertEquals(new HealthCheckResponse("unavailable"), HealthCheckResponse.unavailable());
        }
    }
}