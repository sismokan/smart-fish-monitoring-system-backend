package id.ac.widyatama.smartfishmonitoringsystem.backend.app.common.spark.status_code;

import id.ac.widyatama.smartfishmonitoringsystem.backend.error.Error;
import id.ac.widyatama.smartfishmonitoringsystem.backend.error.ErrorUtils;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.net.HttpURLConnection;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

class BaseHttpErrorTranslatorTest {
    private static Stream<Arguments> getErrorMappings() {
        return Stream.of(
                Arguments.of(Error.Code.GENERIC_ERROR, HttpURLConnection.HTTP_INTERNAL_ERROR),
                Arguments.of(Error.Code.UNKNOWN, HttpURLConnection.HTTP_INTERNAL_ERROR),
                Arguments.of(Error.Code.MALFORMED_JSON, HttpURLConnection.HTTP_BAD_REQUEST),
                Arguments.of(Error.Code.MALFORMED_DATA, HttpURLConnection.HTTP_BAD_REQUEST),
                Arguments.of(Error.Code.FIELD_CANNOT_BE_BLANK, HttpURLConnection.HTTP_BAD_REQUEST),
                Arguments.of(Error.Code.RESOURCE_CANNOT_BE_FOUND, HttpURLConnection.HTTP_NOT_FOUND),
                Arguments.of(Error.Code.INVALID_AUTHORIZATION, HttpURLConnection.HTTP_UNAUTHORIZED)
        );
    }

    @ParameterizedTest(name = "givenError{0}ShouldReturnHttpStatusCode{1}")
    @MethodSource("getErrorMappings")
    public void testGetHttpStatus(Error.Code errorCode, int expectedHttpStatusCode) {
        BaseHttpErrorTranslator baseHttpErrorTranslator = new BaseHttpErrorTranslator();
        Error error = ErrorUtils.createError(errorCode, "");
        assertEquals(expectedHttpStatusCode, baseHttpErrorTranslator.getStatusCode(error));
    }
}