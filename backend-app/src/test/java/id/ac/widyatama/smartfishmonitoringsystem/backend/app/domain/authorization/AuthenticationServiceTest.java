package id.ac.widyatama.smartfishmonitoringsystem.backend.app.domain.authorization;

import id.ac.widyatama.smartfishmonitoringsystem.backend.api_response.APIResponse;
import id.ac.widyatama.smartfishmonitoringsystem.backend.app.domain.decoder.DecodeBasicAuthResponse;
import id.ac.widyatama.smartfishmonitoringsystem.backend.app.domain.decoder.DecoderService;
import id.ac.widyatama.smartfishmonitoringsystem.backend.error.Error;
import id.ac.widyatama.smartfishmonitoringsystem.backend.app.domain.psql.account.Account;
import id.ac.widyatama.smartfishmonitoringsystem.backend.app.domain.psql.account.AccountRepository;
import id.ac.widyatama.smartfishmonitoringsystem.backend.app.domain.psql.dto.GetAccountByUserCredential;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class AuthenticationServiceTest {

    @Nested
    class TestAuthorizeUsingBasic {

        private DecoderService decoderService;
        private AccountRepository accountRepository;
        private AuthenticationService authenticationService;

        @BeforeEach
        public void setUp() {
            decoderService = mock(DecoderService.class);
            accountRepository = mock(AccountRepository.class);
            authenticationService = new AuthenticationService(decoderService, accountRepository);
        }

        @Test
        public void givenTokenIsBlankThenReturnMalformedDataError() {
            APIResponse expectedResponse = APIResponse.createErrorResponse(Error.Code.MALFORMED_DATA, "token");
            assertEquals(expectedResponse, authenticationService.authorizeUsingBasic(""));
        }

        @Test
        public void givenDecodingFailedThenReturnTheError() {
            APIResponse genericErrorResponse = APIResponse.createErrorResponse(Error.Code.GENERIC_ERROR, "some-entity");
            when(decoderService.decodeBasicAuth(eq("foobar"))).thenReturn(genericErrorResponse);

            assertEquals(genericErrorResponse, authenticationService.authorizeUsingBasic("foobar"));
        }

        @Test
        public void givenGetDataFromRepositoryFailedThenReturnTheError() {
            APIResponse genericErrorResponse = APIResponse.createErrorResponse(Error.Code.GENERIC_ERROR, "some-entity");
            DecodeBasicAuthResponse basicAuthResponse = DecodeBasicAuthResponse.builder()
                    .setUsername("foo")
                    .setPassword("bar")
                    .build();
            when(decoderService.decodeBasicAuth(eq("foobar"))).thenReturn(APIResponse.createSuccessResponse(basicAuthResponse));
            GetAccountByUserCredential getAccountByUserCredential = GetAccountByUserCredential.builder()
                    .setUsername("foo")
                    .setPassword("bar")
                    .build();
            when(accountRepository.getAccountBy(getAccountByUserCredential)).thenReturn(genericErrorResponse);

            assertEquals(genericErrorResponse, authenticationService.authorizeUsingBasic("foobar"));
        }

        @Test
        public void givenAuthenticationProcessSucceed() {
            DecodeBasicAuthResponse basicAuthResponse = DecodeBasicAuthResponse.builder()
                    .setUsername("foo")
                    .setPassword("bar")
                    .build();
            when(decoderService.decodeBasicAuth(eq("foobar"))).thenReturn(APIResponse.createSuccessResponse(basicAuthResponse));
            GetAccountByUserCredential getAccountByUserCredential = GetAccountByUserCredential.builder()
                    .setUsername("foo")
                    .setPassword("bar")
                    .build();
            Account account = Account.builder()
                    .setGuid("lorem-ipsum")
                    .setUsername("foo")
                    .build();
            APIResponse<Account> repositoryResponse = APIResponse.createSuccessResponse(account);
            when(accountRepository.getAccountBy(getAccountByUserCredential)).thenReturn(repositoryResponse);
            AuthenticatedAccount authenticatedAccount = AuthenticatedAccount.builder()
                    .setGuid("lorem-ipsum")
                    .setUsername("foo")
                    .build();
            APIResponse<AuthenticatedAccount> successResponse = APIResponse.createSuccessResponse(authenticatedAccount);

            assertEquals(successResponse, authenticationService.authorizeUsingBasic("foobar"));
        }
    }
}