package id.ac.widyatama.smartfishmonitoringsystem.backend.app.common.json;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import lombok.Value;
import org.junit.jupiter.api.Test;

import java.time.OffsetDateTime;
import java.time.ZoneOffset;

import static org.junit.jupiter.api.Assertions.assertEquals;

class OffsetDateTimeSerializerTest {
    private static final Gson gson = new GsonBuilder()
            .registerTypeAdapter(OffsetDateTime.class, new OffsetDateTimeSerializer())
            .create();

    @Test
    void shouldSerializeLocalDate() {
        String expectedDate = "{\"dateTime\":\"2019-11-14T23:59:00Z\"}";
        OffsetDateTime dateTime = OffsetDateTime.of(2019, 11, 14, 23, 59, 0, 0, ZoneOffset.UTC);
        TestOffsetDateTime actualOffsetDateTime = new TestOffsetDateTime(dateTime);

        assertEquals(expectedDate, gson.toJson(actualOffsetDateTime));
    }

    @Test
    void shouldDeserializeLocalDate() {
        OffsetDateTime datetime = OffsetDateTime.of(2019, 11, 14, 23, 59, 0, 0, ZoneOffset.UTC);
        String expectedDate = "{\"dateTime\":\"2019-11-14T23:59:00Z\"}";

        TestOffsetDateTime expected = new TestOffsetDateTime(datetime);

        TestOffsetDateTime actual = gson.fromJson(expectedDate, TestOffsetDateTime.class);
        assertEquals(expected, actual);
    }
}

@Value
class TestOffsetDateTime {
    private OffsetDateTime dateTime;
}