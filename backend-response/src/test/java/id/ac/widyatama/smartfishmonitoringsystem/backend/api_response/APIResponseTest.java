package id.ac.widyatama.smartfishmonitoringsystem.backend.api_response;

import id.ac.widyatama.smartfishmonitoringsystem.backend.error.Error;
import id.ac.widyatama.smartfishmonitoringsystem.backend.error.ErrorUtils;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class APIResponseTest {

    @Nested
    class TestCreateSuccessResponse {

        @Test
        public void shouldBuildAPIResponse() {
            assertEquals(
                    new APIResponse<Object>(
                            true,
                            "foobar",
                            Collections.emptyList()
                    ),
                    APIResponse.createSuccessResponse("foobar")
            );
        }
    }

    @Nested
    class TestCreateErrorResponse {

        @Test
        public void givenListOfErrorsShouldBuildFailedResponse() {
            assertEquals(
                    new APIResponse<>(
                            false,
                            null,
                            Collections.singletonList(
                                    Error.builder()
                                            .setCode(Error.Code.GENERIC_ERROR)
                                            .setCause("some-cause")
                                            .setEntity("some-entity")
                                            .build()
                            )
                    ),
                    APIResponse.createErrorResponse(ErrorUtils.createErrors(Error.Code.GENERIC_ERROR, "some-entity", "some-cause"))
            );
        }

        @Test
        public void givenCodeAndEntityShouldBuildFailedResponse() {
            assertEquals(
                    new APIResponse<>(
                            false,
                            null,
                            Collections.singletonList(
                                    Error.builder()
                                            .setCode(Error.Code.GENERIC_ERROR)
                                            .setCause("")
                                            .setEntity("some-entity")
                                            .build()
                            )
                    ),
                    APIResponse.createErrorResponse(Error.Code.GENERIC_ERROR, "some-entity"));
        }

        @Test
        public void givenCodeEntityAndCauseShouldBuildFailedResponse() {
            assertEquals(
                    new APIResponse<>(
                            false,
                            null,
                            Collections.singletonList(
                                    Error.builder()
                                            .setCode(Error.Code.GENERIC_ERROR)
                                            .setCause("some-cause")
                                            .setEntity("some-entity")
                                            .build()
                            )
                    ),
                    APIResponse.createErrorResponse(Error.Code.GENERIC_ERROR, "some-entity", "some-cause"));
        }
    }

    @Nested
    class TestCreateResponse {

        @Test
        public void givenCallableNotThrowAnyExceptionShoulBuildSucceedResponse() {
            assertEquals(
                    new APIResponse<Object>(
                            true,
                            "foobar",
                            Collections.emptyList()
                    ),
                    APIResponse.createResponse(() -> "foobar")
            );
        }

        @Test
        public void givenCallableThrowAnExceptionShoulBuildFailedResponse() {
            assertEquals(
                    new APIResponse<>(
                            false,
                            null,
                            Collections.singletonList(
                                    Error.builder()
                                            .setCode(Error.Code.GENERIC_ERROR)
                                            .setCause("foobar exception")
                                            .setEntity("")
                                            .build()
                            )
                    ),
                    APIResponse.createResponse(() -> {
                        throw new Exception("foobar exception");
                    })
            );
        }

        @Test
        public void givenCallableThrowAnExceptionShoulBuildFailedResponseWithDefaultException() {
            assertEquals(
                    new APIResponse<>(
                            false,
                            null,
                            Collections.singletonList(
                                    Error.builder()
                                            .setCode(Error.Code.UNKNOWN)
                                            .setCause("foobar exception")
                                            .setEntity("")
                                            .build()
                            )
                    ),
                    APIResponse.createResponse(() -> {
                        throw new Exception("foobar exception");
                    }, Error.Code.UNKNOWN)
            );
        }
    }

    @Nested
    class TestMap {

        @Test
        public void givenSucceedShouldMapToAnotherObjectAndBuildSucceedResponse() {
            assertEquals(
                    new APIResponse<Object>(
                            true,
                            "foobar",
                            Collections.emptyList()
                    ),
                    new APIResponse<>(true, "lorem ipsum", Collections.emptyList())
                            .map(data -> "foobar")
            );
        }

        @Test
        public void givenFailedShouldNotMapToAnotherObject() {
            assertEquals(
                    new APIResponse<>(
                            false,
                            null,
                            Collections.singletonList(
                                    Error.builder()
                                            .setCode(Error.Code.UNKNOWN)
                                            .setCause("foobar exception")
                                            .setEntity("some-entity")
                                            .build()
                            )
                    ),
                    new APIResponse<>(false, null,
                            Collections.singletonList(
                                    Error.builder()
                                            .setCode(Error.Code.UNKNOWN)
                                            .setCause("foobar exception")
                                            .setEntity("some-entity")
                                            .build()
                            ))
                            .map(data -> "foobar")
            );
        }
    }

    @Nested
    class TestMapTry {

        @Test
        public void givenSucceedShouldMapToAnotherObjectAndBuildSucceedResponse() {
            assertEquals(
                    new APIResponse<Object>(
                            true,
                            "foobar",
                            Collections.emptyList()
                    ),
                    new APIResponse<>(true, "lorem ipsum", Collections.emptyList())
                            .mapTry(data -> "foobar")
            );
        }

        @Test
        public void givenFailedShouldNotMapToAnotherObject() {
            assertEquals(
                    new APIResponse<>(
                            false,
                            null,
                            Collections.singletonList(
                                    Error.builder()
                                            .setCode(Error.Code.UNKNOWN)
                                            .setCause("foobar exception")
                                            .setEntity("")
                                            .build()
                            )
                    ),
                    new APIResponse<>(false, null,
                            Collections.singletonList(
                                    Error.builder()
                                            .setCode(Error.Code.UNKNOWN)
                                            .setCause("foobar exception")
                                            .setEntity("")
                                            .build()
                            ))
                            .mapTry(data -> "foobar")
            );
        }

        @Test
        public void givenSucceedButThrowExceptionShouldMapToAnotherObjectAndBuildSucceedResponse() {
            assertEquals(
                    new APIResponse<>(
                            false,
                            null,
                            Collections.singletonList(
                                    Error.builder()
                                            .setCode(Error.Code.GENERIC_ERROR)
                                            .setCause("Index 1 out of bounds for length 0")
                                            .setEntity("")
                                            .build()
                            )
                    ),
                    new APIResponse<>(true, "lorem ipsum", Collections.emptyList())
                            .mapTry(data -> new String[]{}[1])
            );
        }
    }

    @Nested
    class TestValidate {

        @Test
        public void givenSucceedAndPredicateReturnTrueThenShouldReturnSucceedResponse() {
            assertEquals(
                    new APIResponse<Object>(
                            true,
                            "foobar",
                            Collections.emptyList()
                    ),
                    new APIResponse<>(true, "foobar", Collections.emptyList())
                            .validate(StringUtils::isNotBlank, Error.Code.GENERIC_ERROR, "some-entity")
            );
        }

        @Test
        public void givenSucceedAndPredicateReturnFalseThenShouldReturnFailedResponse() {
            assertEquals(
                    new APIResponse<>(
                            false,
                            null,
                            Collections.singletonList(
                                    Error.builder()
                                            .setCode(Error.Code.GENERIC_ERROR)
                                            .setCause("")
                                            .setEntity("some-entity")
                                            .build()
                            )
                    ),
                    new APIResponse<>(true, "foobar", Collections.emptyList())
                            .validate(StringUtils::isBlank, Error.Code.GENERIC_ERROR, "some-entity")
            );
        }

        @Test
        public void givenFailedShouldNotValidate() {
            assertEquals(
                    new APIResponse<>(
                            false,
                            null,
                            Collections.singletonList(
                                    Error.builder()
                                            .setCode(Error.Code.UNKNOWN)
                                            .setCause("foobar exception")
                                            .setEntity("")
                                            .build()
                            )
                    ),
                    new APIResponse<>(false, null,
                            Collections.singletonList(
                                    Error.builder()
                                            .setCode(Error.Code.UNKNOWN)
                                            .setCause("foobar exception")
                                            .setEntity("")
                                            .build()
                            ))
                            .validate(Objects::nonNull, Error.Code.GENERIC_ERROR, "some-entity")
            );
        }
    }

    @Nested
    class TestOrElse {
        @Test
        public void givenSucceedShouldReturnData() {
            assertEquals(
                    "foobar",
                    new APIResponse<Object>(
                            true,
                            "foobar",
                            Collections.emptyList()
                    ).orElse("lorem-ipsum")
            );
        }

        @Test
        public void givenFailedShouldReturnOther() {
            assertEquals(
                    "lorem-ipsum",
                    new APIResponse<>(
                            false,
                            null,
                            Collections.singletonList(
                                    Error.builder()
                                            .setCode(Error.Code.GENERIC_ERROR)
                                            .setCause("")
                                            .setEntity("some-entity")
                                            .build()
                            )
                    ).orElse("lorem-ipsum")
            );
        }
    }

    @Nested
    class TestRecoverWith {

        @Test
        public void givenSucceedShouldNotRecoverTheObject() {
            assertEquals(
                    new APIResponse<Object>(
                            true,
                            "foobar",
                            Collections.emptyList()
                    ),
                    new APIResponse<Object>(
                            true,
                            "foobar",
                            Collections.emptyList()
                    ).recoverWith((errors) -> APIResponse.createSuccessResponse("lorem-ipsum"))
            );
        }

        @Test
        public void givenFailedShouldRecoverTheObject() {
            assertEquals(
                    new APIResponse<Object>(
                            true,
                            "foobar",
                            Collections.emptyList()
                    ),
                    new APIResponse<>(
                            false,
                            null,
                            Collections.singletonList(
                                    Error.builder()
                                            .setCode(Error.Code.GENERIC_ERROR)
                                            .setCause("")
                                            .setEntity("some-entity")
                                            .build()
                            )
                    ).recoverWith(errors -> APIResponse.createSuccessResponse("foobar"))
            );
        }
    }

    @Nested
    class TestRecoverTry {

        @Test
        public void givenSucceedShouldNotRecoverTheObject() {
            assertEquals(
                    new APIResponse<Object>(
                            true,
                            "foobar",
                            Collections.emptyList()
                    ),
                    new APIResponse<Object>(
                            true,
                            "foobar",
                            Collections.emptyList()
                    ).recoverTry((errors) -> APIResponse.createSuccessResponse("lorem-ipsum"))
            );
        }

        @Test
        public void givenFailedShouldRecoverTheObject() {
            assertEquals(
                    new APIResponse<>(
                            false,
                            null,
                            Collections.singletonList(
                                    Error.builder()
                                            .setCode(Error.Code.GENERIC_ERROR)
                                            .setCause("Index 1 out of bounds for length 0")
                                            .setEntity("")
                                            .build()
                            )
                    ),
                    new APIResponse<>(
                            false,
                            null,
                            Collections.singletonList(
                                    Error.builder()
                                            .setCode(Error.Code.GENERIC_ERROR)
                                            .setCause("")
                                            .setEntity("some-entity")
                                            .build()
                            )
                    ).recoverTry(errors -> new String[]{}[1])
            );
        }
    }

    @Nested
    class TestPeek {

        @Test
        public void givenSucceedShouldExecuteOnSuccess() {
            Consumer<Object> consOnSuccess = mock(Consumer.class);
            Consumer<List<Error>> consOnFailure = mock(Consumer.class);
            new APIResponse<Object>(
                    true,
                    "foobar",
                    Collections.emptyList()
            ).peek(consOnSuccess, consOnFailure);
            verify(consOnSuccess, times(1)).accept("foobar");
            verifyNoInteractions(consOnFailure);
        }

        @Test
        public void givenFailedShouldExecuteOnError() {
            Consumer<Object> consOnSuccess = mock(Consumer.class);
            Consumer<List<Error>> consOnError = mock(Consumer.class);
            List<Error> errors = Collections.singletonList(
                    Error.builder()
                            .setCode(Error.Code.GENERIC_ERROR)
                            .setCause("")
                            .setEntity("some-entity")
                            .build()
            );
            new APIResponse<>(
                    false,
                    null,
                    errors
            ).peek(consOnSuccess, consOnError);
            verify(consOnError, times(1)).accept(errors);
            verifyNoInteractions(consOnSuccess);
        }
    }

    @Nested
    class TestOnError {

        @Test
        public void givenSucceedShouldNotExecuteOnError() {
            Consumer<List<Error>> consOnError = mock(Consumer.class);
            new APIResponse<Object>(
                    true,
                    "foobar",
                    Collections.emptyList()
            ).onError(consOnError);
            verifyNoInteractions(consOnError);
        }

        @Test
        public void givenFailedShouldExecuteOnError() {
            Consumer<List<Error>> consOnError = mock(Consumer.class);
            List<Error> errors = Collections.singletonList(
                    Error.builder()
                            .setCode(Error.Code.GENERIC_ERROR)
                            .setCause("")
                            .setEntity("some-entity")
                            .build()
            );
            new APIResponse<>(
                    false,
                    null,
                    errors
            ).onError(consOnError);
            verify(consOnError, times(1)).accept(errors);
        }
    }

    @Nested
    class TestOnSuccess {

        @Test
        public void givenSucceedShouldExecuteOnSuccess() {
            Consumer<Object> consOnSuccess = mock(Consumer.class);
            new APIResponse<Object>(
                    true,
                    "foobar",
                    Collections.emptyList()
            ).onSuccess(consOnSuccess);
            verify(consOnSuccess, times(1)).accept("foobar");
        }

        @Test
        public void givenFailedShouldNotExecuteOnSuccess() {
            Consumer<Object> consOnSuccess = mock(Consumer.class);
            List<Error> errors = Collections.singletonList(
                    Error.builder()
                            .setCode(Error.Code.GENERIC_ERROR)
                            .setCause("")
                            .setEntity("some-entity")
                            .build()
            );
            new APIResponse<>(
                    false,
                    null,
                    errors
            ).onSuccess(consOnSuccess);
            verifyNoInteractions(consOnSuccess);
        }
    }

    @Nested
    class TestChangeErrors {

        @Test
        public void givenSucceedShouldNotChangeIntoError() {
            assertEquals(
                    new APIResponse<Object>(
                            true,
                            "foobar",
                            Collections.emptyList()
                    ),
                    new APIResponse<Object>(
                            true,
                            "foobar",
                            Collections.emptyList()
                    ).changeErrors(Error.Code.GENERIC_ERROR, "entity")
            );
        }

        @Test
        public void givenFailedShouldChangeTheErrorsWithCodeEntityAndCause() {
            assertEquals(
                    new APIResponse<>(
                            false,
                            null,
                            Collections.singletonList(
                                    Error.builder()
                                            .setCode(Error.Code.UNKNOWN)
                                            .setCause("some-cause")
                                            .setEntity("some-entity")
                                            .build()
                            )
                    ),
                    new APIResponse<>(
                            false,
                            null,
                            Collections.singletonList(
                                    Error.builder()
                                            .setCode(Error.Code.GENERIC_ERROR)
                                            .setCause("")
                                            .setEntity("some-entity")
                                            .build()
                            )
                    ).changeErrors(Error.Code.UNKNOWN, "some-entity", "some-cause")
            );
        }

        @Test
        public void givenFailedShouldChangeTheErrorsWithCodeAndEntity() {
            assertEquals(
                    new APIResponse<>(
                            false,
                            null,
                            Collections.singletonList(
                                    Error.builder()
                                            .setCode(Error.Code.UNKNOWN)
                                            .setCause("")
                                            .setEntity("some-entity")
                                            .build()
                            )
                    ),
                    new APIResponse<>(
                            false,
                            null,
                            Collections.singletonList(
                                    Error.builder()
                                            .setCode(Error.Code.GENERIC_ERROR)
                                            .setCause("")
                                            .setEntity("some-entity")
                                            .build()
                            )
                    ).changeErrors(Error.Code.UNKNOWN, "some-entity")
            );
        }

        @Test
        public void givenFailedShouldChangeTheErrorsWithCustomMapper() {
            assertEquals(
                    new APIResponse<>(
                            false,
                            null,
                            Collections.singletonList(
                                    Error.builder()
                                            .setCode(Error.Code.UNKNOWN)
                                            .setCause("")
                                            .setEntity("some-entity")
                                            .build()
                            )
                    ),
                    new APIResponse<>(
                            false,
                            null,
                            Collections.singletonList(
                                    Error.builder()
                                            .setCode(Error.Code.GENERIC_ERROR)
                                            .setCause("")
                                            .setEntity("some-entity")
                                            .build()
                            )
                    ).changeErrors(
                            errors -> errors
                                    .stream()
                                    .map(error -> error.toBuilder().setCode(Error.Code.UNKNOWN)
                                            .build()
                                    )
                                    .collect(Collectors.toList()))
            );
        }
    }

    @Nested
    class TestIsGenericError {

        @Test
        public void givenNonGenericErrorShouldReturnFalse() {
            assertFalse(
                    new APIResponse<>(
                            false,
                            null,
                            Collections.singletonList(
                                    Error.builder()
                                            .setCode(Error.Code.UNKNOWN)
                                            .setCause("")
                                            .setEntity("some-entity")
                                            .build()
                            )
                    ).isGenericError()
            );
        }

        @Test
        public void givenGenericErrorShouldReturnTrue() {
            assertTrue(
                    new APIResponse<>(
                            false,
                            null,
                            Collections.singletonList(
                                    Error.builder()
                                            .setCode(Error.Code.GENERIC_ERROR)
                                            .setCause("")
                                            .setEntity("some-entity")
                                            .build()
                            )
                    ).isGenericError()
            );
        }
    }

    @Nested
    class TestIsContainError {

        @Test
        public void givenNotContainExpectedErrorShouldReturnFalse() {
            assertFalse(
                    new APIResponse<>(
                            false,
                            null,
                            Collections.singletonList(
                                    Error.builder()
                                            .setCode(Error.Code.UNKNOWN)
                                            .setCause("")
                                            .setEntity("some-entity")
                                            .build()
                            )
                    ).isContainError(error -> Error.Code.GENERIC_ERROR.equals(error.getCode()))
            );
        }

        @Test
        public void givenContainsExpectedErrorShouldReturnTrue() {
            assertTrue(
                    new APIResponse<>(
                            false,
                            null,
                            Collections.singletonList(
                                    Error.builder()
                                            .setCode(Error.Code.GENERIC_ERROR)
                                            .setCause("")
                                            .setEntity("some-entity")
                                            .build()
                            )
                    ).isContainError(error -> Error.Code.GENERIC_ERROR.equals(error.getCode()))
            );
        }
    }

}