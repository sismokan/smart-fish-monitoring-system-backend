package id.ac.widyatama.smartfishmonitoringsystem.backend.api_response;

import id.ac.widyatama.smartfishmonitoringsystem.backend.error.Error;
import id.ac.widyatama.smartfishmonitoringsystem.backend.error.ErrorUtils;
import lombok.Value;

import java.util.Collections;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static id.ac.widyatama.smartfishmonitoringsystem.backend.error.ErrorUtils.createError;
import static id.ac.widyatama.smartfishmonitoringsystem.backend.error.ErrorUtils.createErrors;

@Value
public class APIResponse<T> {

    boolean success;
    T data;
    List<Error> errors;

    public APIResponse(boolean success, T data, List<Error> errors) {
        this.success = success;
        this.data = data;
        this.errors = errors;
    }

    public static <U> APIResponse<U> createSuccessResponse(U data) {
        return new APIResponse(true, data, Collections.emptyList());
    }

    public static APIResponse createErrorResponse(List<Error> errors) {
        return new APIResponse(false, null, errors);
    }

    public static APIResponse createErrorResponse(Error.Code code, String entity) {
        return new APIResponse(false, null, createErrors(code, entity));
    }

    public static APIResponse createErrorResponse(Error.Code code, String entity, String cause) {
        return new APIResponse(false, null, createErrors(code, entity, cause));
    }

    public static <U> APIResponse<U> createResponse(Callable<U> callable) {
        try {
            return createSuccessResponse(callable.call());
        } catch (Exception e) {
            return createErrorResponse(createErrors(Error.Code.GENERIC_ERROR, "", e.getMessage()));
        }
    }

    public static <U> APIResponse<U> createResponse(Callable<U> callable, Error.Code defaultErrorCode) {
        try {
            return createSuccessResponse(callable.call());
        } catch (Exception e) {
            return createErrorResponse(createErrors(defaultErrorCode, "", e.getMessage()));
        }
    }

    public <U> APIResponse<U> map(Function<? super T, ? extends U> mapper) {
        if (isSuccess()) {
            return createSuccessResponse(mapper.apply(this.getData()));
        }
        return (APIResponse<U>) this;
    }

    public <U> APIResponse<U> flatMap(Function<? super T, APIResponse<U>> mapper) {
        if (isSuccess()) {
            return mapper.apply(this.getData());
        }
        return (APIResponse<U>) this;
    }

    public <U> APIResponse<U> mapTry(Function<? super T, ? extends U> mapper) {
        if (isSuccess()) {
            return createResponse(() -> mapper.apply(this.getData()));
        }
        return (APIResponse<U>) this;
    }

    public APIResponse<T> validate(Predicate<? super T> validator, List<Error> errors) {
        if (isSuccess()) {
            return validator.test(this.getData()) ? this : createErrorResponse(errors);
        }
        return this;
    }

    public APIResponse<T> validate(Predicate<? super T> validator, Error.Code code, String entity, String cause) {
        return validate(validator, createErrors(code, entity, cause));
    }

    public APIResponse<T> validate(Predicate<? super T> validator, Error.Code code, String entity) {
        return validate(validator, code, entity, "");
    }

    public T orElse(T other) {
        return isSuccess() ? data : other;
    }

    public APIResponse<T> recoverWith(Function<List<Error>, APIResponse<T>> mapper) {
        if (isSuccess()) {
            return this;
        }

        return mapper.apply(this.errors);
    }

    public APIResponse<T> recoverTry(Function<List<Error>, T> mapper) {
        return recoverWith(errors -> createResponse(() -> mapper.apply(errors)));
    }

    public APIResponse<T> peek(Consumer<T> onSuccess, Consumer<List<Error>> onError) {
        if (isSuccess()) {
            onSuccess.accept(this.data);
        } else {
            onError.accept(this.errors);
        }

        return this;
    }

    public APIResponse<T> onSuccess(Consumer<T> onSuccess) {
        if (isSuccess()) {
            onSuccess.accept(this.data);
        }

        return this;
    }

    public APIResponse<T> onError(Consumer<List<Error>> onError) {
        if (!isSuccess()) {
            onError.accept(this.errors);
        }

        return this;
    }

    public APIResponse<T> changeErrors(List<Error> errors) {
        if (isSuccess()) {
            return this;
        }

        return APIResponse.createErrorResponse(errors);
    }

    public APIResponse<T> changeErrors(Error.Code code, String entity, String cause) {
        return changeErrors(createErrors(code, entity, cause));
    }

    public APIResponse<T> changeErrors(Error.Code code, String entity) {
        return changeErrors(errors -> errors.stream()
                .map(error -> createError(code, entity, error.getCause()))
                .collect(Collectors.toList()));
    }

    public APIResponse<T> changeErrors(Function<List<Error>, List<Error>> mapper) {
        return changeErrors(mapper.apply(errors));
    }

    public boolean isGenericError() {
        return ErrorUtils.isContainError(errors, ErrorUtils::isGenericError);
    }

    public boolean isContainError(Predicate<Error> predicate) {
        return ErrorUtils.isContainError(errors, predicate);
    }

}
