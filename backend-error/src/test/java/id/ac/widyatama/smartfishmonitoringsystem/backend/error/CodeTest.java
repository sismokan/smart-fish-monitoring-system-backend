package id.ac.widyatama.smartfishmonitoringsystem.backend.error;

import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SuppressWarnings("PMD.UnusedPrivateMethod")
public class CodeTest {

    private static Stream<Arguments> getCodeNames() {
        return Stream.of(
                Arguments.of(Error.Code.GENERIC_ERROR, 900),
                Arguments.of(Error.Code.UNKNOWN, 0),
                Arguments.of(Error.Code.MALFORMED_JSON, 300),
                Arguments.of(Error.Code.MALFORMED_DATA, 301),
                Arguments.of(Error.Code.INVALID_AUTHORIZATION, 400),
                Arguments.of(Error.Code.RESOURCE_CANNOT_BE_FOUND, 500)
        );
    }

    @ParameterizedTest(name = "given code {0} should return {1}")
    @MethodSource("getCodeNames")
    public void testGetNumber(Error.Code inputCode, int expectedNumber) {
        assertEquals(expectedNumber, inputCode.getNumber());
    }

    @Nested
    class TestGetValue {

        @Test
        public void givenUnregisteredCodeShouldReturnUnknown() {
            assertEquals(Error.Code.UNKNOWN, Error.Code.getValue("foo"));
        }

        @Test
        public void givenGenericErrorShouldReturnGenericError() {
            assertEquals(Error.Code.GENERIC_ERROR, Error.Code.getValue("GENERIC_ERROR"));
        }
    }

}
