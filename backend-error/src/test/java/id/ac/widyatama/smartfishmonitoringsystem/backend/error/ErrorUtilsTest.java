package id.ac.widyatama.smartfishmonitoringsystem.backend.error;

import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.Collections;

import static org.junit.jupiter.api.Assertions.*;

class ErrorUtilsTest {

    @Nested
    class TestIsGenericError {

        @Test
        public void givenNonGenericErrorShouldReturnFalse() {
            assertFalse(ErrorUtils.isGenericError(Error.builder().setCode(Error.Code.UNKNOWN).build()));
        }

        @Test
        public void givenGenericErrorShouldReturnTrue() {
            assertTrue(ErrorUtils.isGenericError(Error.builder().setCode(Error.Code.GENERIC_ERROR).build()));
        }
    }

    @Nested
    class TestIsContainError {

        @Test
        public void givenErrorsNotContainedShouldReturnFalse() {
            assertFalse(
                    ErrorUtils.isContainError(
                            Collections.singletonList(Error.builder().setCode(Error.Code.GENERIC_ERROR).build()),
                            (error) -> Error.Code.UNKNOWN.equals(error.getCode())
                    )
            );
        }

        @Test
        public void givenErrorsContainedShouldReturnTrue() {
            assertTrue(
                    ErrorUtils.isContainError(
                            Collections.singletonList(Error.builder().setCode(Error.Code.GENERIC_ERROR).build()),
                            (error) -> Error.Code.GENERIC_ERROR.equals(error.getCode())
                    )
            );
        }
    }

    @Nested
    class TestCreateError {

        @Test
        public void shouldBuildError() {
            Error error = Error.builder()
                    .setCode(Error.Code.GENERIC_ERROR)
                    .setEntity("entity")
                    .setCause("cause")
                    .build();
            assertEquals(error, ErrorUtils.createError(Error.Code.GENERIC_ERROR, "entity", "cause"));
        }

        @Test
        public void withoutCauseShouldBuildError() {
            Error error = Error.builder()
                    .setCode(Error.Code.GENERIC_ERROR)
                    .setEntity("entity")
                    .setCause("")
                    .build();
            assertEquals(error, ErrorUtils.createError(Error.Code.GENERIC_ERROR, "entity"));
        }
    }

    @Nested
    class TestCreateErrors {

        @Test
        public void shouldBuildErrors() {
            Error error = Error.builder()
                    .setCode(Error.Code.GENERIC_ERROR)
                    .setEntity("entity")
                    .setCause("cause")
                    .build();
            assertEquals(Collections.singletonList(error), ErrorUtils.createErrors(Error.Code.GENERIC_ERROR, "entity", "cause"));
        }

        @Test
        public void withoutCauseShouldBuildErrors() {
            Error error = Error.builder()
                    .setCode(Error.Code.GENERIC_ERROR)
                    .setEntity("entity")
                    .setCause("")
                    .build();
            assertEquals(Collections.singletonList(error), ErrorUtils.createErrors(Error.Code.GENERIC_ERROR, "entity"));
        }
    }
}