package id.ac.widyatama.smartfishmonitoringsystem.backend.error;

import org.apache.commons.lang3.StringUtils;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;

public class ErrorUtils {

    public static boolean isGenericError(Error error) {
        return Error.Code.GENERIC_ERROR.equals(error.getCode());
    }

    public static boolean isContainError(List<Error> errors, Predicate<Error> predicate) {
        return Optional.ofNullable(errors)
                .orElse(Collections.emptyList())
                .stream()
                .anyMatch(predicate);
    }

    public static Error createError(Error.Code code, String entity, String cause) {
        return Error.builder()
                .setCode(code)
                .setEntity(StringUtils.defaultIfBlank(entity, ""))
                .setCause(StringUtils.defaultIfBlank(cause, ""))
                .build();
    }

    public static Error createError(Error.Code code, String entity) {
        return createError(code, entity, "");
    }

    public static List<Error> createErrors(Error.Code code, String entity, String cause) {
        Error error = createError(code, entity, cause);
        return Collections.singletonList(error);
    }

    public static List<Error> createErrors(Error.Code code, String entity) {
        return createErrors(code, entity, "");
    }

}
