package id.ac.widyatama.smartfishmonitoringsystem.backend.error;

import lombok.Builder;
import lombok.Value;

@Value
@Builder(setterPrefix = "set", toBuilder = true)
public class Error {

    String entity;
    Code code;
    String cause;

    public enum Code {

        UNKNOWN(0),
        GENERIC_ERROR(900),
        MALFORMED_JSON(300),
        MALFORMED_DATA(301),
        FIELD_CANNOT_BE_BLANK(302),
        INVALID_AUTHORIZATION(400),
        RESOURCE_CANNOT_BE_FOUND(500);

        private final int number;

        Code(int number) {
            this.number = number;
        }

        public static Code getValueOrDefault(String codeName, Code defaultCode) {
            try {
                return Code.valueOf(codeName);
            } catch (Exception e) {
                return defaultCode;
            }
        }

        public static Code getValue(String codeName) {
            return getValueOrDefault(codeName, UNKNOWN);
        }

        public int getNumber() {
            return number;
        }
    }
}
